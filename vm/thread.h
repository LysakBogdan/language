#ifndef THREAD_H_INCLUDED
#define THREAD_H_INCLUDED

#include <optional>
#include "runtime/function.h"

class VM;

struct Call
{
	Function *method = nullptr;
	Command *ip = nullptr;
	int start = 0;
	int locals_start = 0;
	bool root = false;
};

class Thread: public Object
{
	VM& vm;
	vector<Value> stack;
	vector<Value> vars;
	vector<Call> calls;
	Call call;
	int count = 0;

	void init_function_call(Function& f, int arg_n);

public:
	int top() const { return stack.size(); }
	void adjust_size(int size);

	optional<Value> yield;
	Value yield_key;

	explicit Thread(VM& aVM);

	Value& operator[](int i)
	{
		return stack[call.start + i];
	}

	Value& var(int i)
	{
		return vars[i];
	}

	VM& machine() const { return vm; }
	int result_number() const { return count; }
	void ret(int a);
	void move_values(int a, int b, int n);
	bool pop_call(int n);
	bool return_values(int n);

	Value execute(FuncProto&);
	Value execute(Function*);
	int run();

	Value constant(int id) const;
	void jump(int target);

	int apply(Context&);
	void apply_on_top(const vector<Value>& args, int result_number);
	Value apply(const vector<Value>& args);
	void apply(int n);
	tuple<Value, Value> apply2(const vector<Value>& args);
	void callAtom(Context& c, CFunction f);
	void callPattern(Context& c, CPattern f);
	void callFunction(Context& c, Function& f);

	tuple<Value, Value> pop2()
	{
		assert(stack.size() >= 2);
		auto a = stack[top() - 2];
		auto b = stack[top() - 1];
		stack.resize(top() - 2);
		return {a, b};
	}

	template<class F>
	void bin_op(F&& f)
	{
		assert(stack.size() >= 2);
		auto a = stack[top() - 2];
		auto b = stack[top() - 1];
		auto sum = f(*this, a, b);
		stack.pop_back();
		top_value() = sum;
	}

	void push(Value x)
	{
		stack.push_back(x);
	}

	Value pop()
	{
		assert(!stack.empty());
		auto r = stack.back();
		stack.pop_back();
		return r;
	}

	void pop_n(int n)
	{
		stack.resize(stack.size() - n);
	}

	Value& top_value()
	{
		assert(!stack.empty());
		return stack.back();
	}

	static constexpr auto type_name = "thread";
	void mark(GC&) const override;
	void inspect(Printer&) const override;
	Type& type() const override;
	static Type& get_type();

	friend ostream& operator<<(ostream& os, const Thread& thread);
	friend Context;
};

#endif
