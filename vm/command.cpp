#include "command.h"

#include "atoms.h"

using enum TCommand;

const Command::Info Command::info[] =
{
	{"push", 1, true},
	{"dip", 1},
	{"swap", 0},

	{"locals", 1},

	{"nil", 0, true},
	{"const", 1, true},
	{"get_global", 1, true},
	{"set_global", 1},
	{"get_upvalue", 1, true},
	{"get_local", 1, true},
	{"set_local", 1},
	{"self", 0, true},
	{"get", 0},
	{"set", 0},
	{"add_pair", 0},
	{"unpack", 0},

	{"apply", 1},
	{"decons", 1},

	{"array", 1},
	{"function", 1, true},
	{"string", 1, true},

	{"jump", 1},
	{"jump_true", 2},
	{"jump_false", 2},

	{"ret", 0},
	{"match_ret", 1},
	{"set_count", 1},
	{"pop", 0},

	{"fixed_values", 1},

	{"add", 0},
	{"sub", 0},
	{"mul", 0},
	{"div", 0},
	{"idiv", 0},
	{"mod ", 0},

	{"eq", 0},
	{"gr", 0},
	{"lt", 0},
	{"ge", 0},
	{"le", 0},
	{"ne", 0},

	{"neg", 0},
	{"not", 0},

	{"inc", 0},
	{"dec", 0},

	{"inspect", 1},
	{"check", 2},
	{"assert", 0}
};

optional<TCommand> PrimitiveCommand(Value a)
{
	static map<Value, TCommand> commands
	{
		{atom("negate"), Neg},
		{atom("not"), Not},
		{atom("plus"), Add},
		{atom("minus"), Subt},
		{atom("mult"), Mul},
		{atom("div"), Div},
		{atom("idiv"), iDiv},
		{atom("mod"), Mod},

		{atom("equal"), Eq},
		{atom("greater"), Gr},
		{atom("less"), Lt},
		{atom("greater_equal"), Ge},
		{atom("less_equal"), Le},
		{atom("not_equal"), Ne},

		{atom("next"), Inc},
		{atom("prev"), Dec},
		{atom("at"), Get}
	};
	auto c = commands.find(a);
	if(c == commands.end())
		return nullopt;
	return c->second;
}

ostream& print_command(ostream& os, Command c, const vector<Value>& constants)
{
	auto [name, n, _] = Command::info[int(c.command)];
	os << name;
	if(n > 0)
		os << " " << c.a;
	if(n > 1)
		os << " " << c.b;
	if(n > 2)
		os << " " << c.c;

	if(c.command == GetConst || c.command == NewString || c.command == GetGlobal || c.command == SetGlobal)
		return os << " (" << constants[c.a] << ")";

	return os;
}
