#include "thread.h"

#include "atom.h"
#include "formatting.h"
#include "inspection.h"
#include "config.h"
#include "icoroutine.h"
#include "atoms.h"
#include "arithmetic.h"

Thread::Thread(VM& aVM): vm(aVM)
{
}

void Thread::callAtom(Context& c, CFunction f)
{
	auto res = f(c);
	count = 1;
	stack.resize(c.shift);
	push(res);
}

void Thread::callPattern(Context& c, CPattern f)
{
	count = f(c, c[0]);
}

void Thread::init_function_call(Function& f, int arg_n)
{
	call = {&f, f.getProto().code.data(), top() - arg_n, int(vars.size())};
	calls.push_back(call);
	count = arg_n;
}

void Thread::callFunction(Context& c, Function& f)
{
	calls.back() = call;
	init_function_call(f, c.arg_num());
}

Value Thread::execute(FuncProto& proto)
{
	return execute(vm.create<Function>(proto, nullptr, stack.data() + call.start).get());
}

Value Thread::execute(Function* callee)
{
	init_function_call(*callee, 0);
	run();
	return stack.back();
}

int Thread::apply(Context& c)
{
	if(!init_apply(c))
		run();
	return count;
}

template<class I>
void nils(I first, I last)
{
	if(first < last)
		fill(first, last, Value::Nil);
}

void Thread::adjust_size(int size)
{
	auto r = top();
	stack.resize(size);
	nils(stack.begin() + r, stack.end());
}

void Thread::apply_on_top(const vector<Value>& args, int result_number)
{
	auto r = top();
	stack.insert(stack.end(), args.begin(), args.end());

	Context context(r, args.size(), vm, *this);
	apply(context);

	adjust_size(r + result_number);
}

Value Thread::apply(const vector<Value>& args)
{
	apply_on_top(args, 1);
	return pop();
}

tuple<Value, Value> Thread::apply2(const vector<Value>& args)
{
	apply_on_top(args, 2);
	return pop2();
}

void Thread::move_values(int a, int b, int n)
{
	stack.resize(call.start + a + n);
	auto r = stack.data() + call.start;
	copy_n(r + b, n, r + a); // TODO: maybe optimize it
	count = n;
}

Value Thread::constant(int id) const
{
	return call.method->getProto().constants[id];
}

void Thread::jump(int target)
{
	call.ip = call.method->getProto().code.data() + target;
}

void Thread::apply(int n)
{
	Context context(top() - n, n, vm, *this);
	init_apply(context);
}

bool Thread::pop_call(int n)
{
	calls.pop_back();
	if(calls.empty())
		return true;

	auto root = call.root;
	auto result = call.start;
	call = calls.back();
	count = n;
	stack.resize(result + n);
	return root;
}

bool Thread::return_values(int n)
{
	move_values(0, top() - call.start - n, n);
	return pop_call(n);
}

int Thread::run()
{
	using enum TCommand;
	call.root = true;
	for(;;)
	{
		if(yield)
			return 0;
		auto c = *call.ip++;
		auto R = stack.data() + call.start;
		auto locals = vars.data() + call.locals_start;

#if TRACE_EXECUTION
		log_indentation(calls.size() - 1);
		if(Log)
			print_command(*Log, c, call.method->getProto().constants);
#endif
		switch(c.command)
		{
			case Push:
				push(R[c.a]);
				break;
			case Dip:
				move_values(c.a, top() - 1 - call.start, 1);
				break;
			case SwapCmd:
				swap(stack[top() - 2], stack[top() - 1]);
				break;
			case LocalsCount:
				{
					auto n = vars.size();
					vars.resize(call.locals_start + c.a);
					nils(vars.begin() + n, vars.end());
				}
				break;
			case Nil:
				push(Value::Nil);
				break;
			case GetConst:
				push(constant(c.a));
				break;
			case GetLocal:
				push(locals[c.a]);
				break;
			case SetLocal:
				locals[c.a] = pop();
				break;
			case GetUpvalue:
				push(call.method->get_capture(c.a));
				break;
			case GetSelf:
				push(make_value(call.method));
				break;
			case CmdApply:
				apply(c.a);
				break;
			case CmdDecons:
				apply(c.a);
				break;
			case NewArray:
				{
					auto r = top() - call.start - c.a;
					auto a = vm.create<Array>(c.a, R + r);
					pop_n(c.a);
					push(a);
				}
				break;
			case NewString:
				push(make_value(vm.create<Blob>(Symbol(constant(c.a).id).str())));
				break;
			case NewFunction:
				push(make_value(vm.create<Function>(*constant(c.a).as<FuncProto>(), call.method, locals)));
				break;

			case Jump:
				jump(c.a);
				break;
			case JumpTrue:
				if(R[c.a])
					jump(c.b);
				break;
			case JumpFalse:
				if(!R[c.a])
					jump(c.b);
				break;
			case Pop:
				pop_n(1);
				break;
			case SetCount:
				adjust_size(top() - count + c.a);
				break;
			case FixedValues:
				{
					auto var_args = top() - count + c.a;
					count = max(count - c.a, 0);
					auto args = vm.create<Array>(count, stack.data() + var_args);
					adjust_size(var_args);
					push(args);
				}
				break;

			case Add:
				bin_op(op_add);
				break;
			case Subt:
				bin_op(op_sub);
				break;
			case Mul:
				bin_op(op_mul);
				break;
			case Div:
				bin_op(op_div);
				break;
			case iDiv:
				bin_op(op_idiv);
				break;
			case Mod:
				bin_op(op_mod);
				break;

			case Eq:
				{
					auto [a, b] = pop2();
					push(a == b ? a : Value::Nil);
				}
				break;
			case Ne:
				{
					auto [a, b] = pop2();
					push(b && a != b ? a : Value::Nil);
				}
				break;
			case Lt:
				{
					auto [a, b] = pop2();
					push(apply({atom("less"), a, b}));
				}
				break;
			case Gr:
				{
					auto [a, b] = pop2();
					push(apply({atom("greater"), a, b}));
				};
				break;
			case Le:
				{
					auto [a, b] = pop2();
					push(apply({atom("less_equal"), a, b}));
				}
				break;
			case Ge:
				{
					auto [a, b] = pop2();
					push(apply({atom("greater_equal"), a, b}));
				}
				break;

			case Neg:
				op_neg(*this);
				break;
			case Not:
				top_value() = Value::Bool(!top_value());
				break;

			case Inc:
				top_value() = apply({"next"_m, top_value()});
				break;
			case Dec:
				top_value() = apply({"prev"_m, top_value()});
				break;

			case Get:
				{
					auto [a, i] = pop2();
					push(apply({"at"_m, a, i}));
				}
				break;
			case Set:
				{
					auto [key, value] = pop2();
					apply({"set_at"_m, top_value(), value, key});
				}
				break;
			case AddPair:
				{
					auto [key, value] = pop2();
					top_value().as<Table>()->add_pair(key, value);
				}
				break;
			case SetGlobal:
				vm.module.put(Symbol(constant(c.a).id), pop());
				break;
			case GetGlobal:
				push(vm.module[Symbol(constant(c.a).id)]);
				break;

			case Unpack:
				push(top_value());
				stack[top() - 2] = "unpack"_m;
				apply(2);
				break;
			case Inspect:
				LOG_LINE("INSPECT #" << c.a);
				LOG_LINE(*this);
				break;
			case CheckCmd:
				if(!top_value())
				{
					jump(c.a);
					stack.resize(call.start + c.b);
					top_value() = Value::Nil;
				}
				break;
			case AssertCmd:
				if(!top_value())
				{
					if(calls.size() == 1)
						throw RuntimeError{MatchFail{}};

					R[0] = Value::Nil;
					if(pop_call(1))
						return 1;
				}
				break;
			case Ret:
				if(return_values(1))
					return 1;
				break;
			case MatchRet:
				if(return_values(c.a))
					return c.a;
				break;
			default:
				CAN_NOT_HAPPEN;
		}
#if TRACE_EXECUTION
		LOG_LINE("\t\t\t{" << vars << "}");
		log_indentation(calls.size() - 1);
		LOG_LINE(*this);
#endif
	}
	calls.pop_back();
	return 1;
}

void Thread::mark(GC& gc) const
{
	gc.mark(call.method);
	for(const auto& call : calls)
		gc.mark(call.method);

	for(auto value : stack)
		gc.mark(value);

	for(auto var : vars)
		gc.mark(var);
}

void Thread::inspect(Printer& p) const
{
	p << *this;
}

ostream& operator<<(ostream& os, const Thread& t)
{
	os << "| ";
#if TRACE_ONLY_CURRENT_FRAME
	for(int i = t.call.start; i < t.top(); i++)
		os << t.stack[i] << " | ";
#else
	for(auto value : t.stack)
		os << value << " | ";
#endif
	return os;
}

Type& Thread::get_type()
{
	static Type interface(value(ICoroutine::constructor), ICoroutine::methods);
	return interface;
}

Type& Thread::type() const
{
	return get_type();
}
