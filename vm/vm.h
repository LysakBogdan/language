#ifndef VM_H
#define VM_H

#include "runtime/table.h"
#include "runtime/array.h"
#include "runtime/memory.h"
#include "runtime/module.h"
#include "vm/thread.h"
#include "lexer/chars.h"

class VM
{
public:
	GC gc;
	Module module;
	vector<Type> base_types;
	Thread thread;

	VM();

	void collectGarbage();

	template<class T, class ...Args>
	GC::Ptr<T> create(Args&&... args)
	{
		if(gc.need_collect())
			collectGarbage();
		return gc.create<T>(args...);
	}
	Value get_global(const string& name);
	void set_global(const string& name, Value value);
	Value eval(CharIterator text);
	auto eval(const string &text)
	{
		return eval(text.c_str());
	}
	Value call(const vector<Value>& args);
};

void* operator new(size_t size, Thread& thread);
GC::Ptr<FuncProto> compile(VM& vm, const FunctionScope& exe);
int manual_garbage_collection(VM& vm);
bool supports(VM&, Value, Symbol method);

template<class T, class ...Args>
auto create(Context& c, Args&&... args)
{
	return c.vm.create<T>(args...);
}

#endif
