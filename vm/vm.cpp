#include "vm.h"

#include "compiler.h"
#include "parser.h"

#include "inumber.h"
#include "isymbol.h"
#include "ifunction.h"
#include "itype.h"

#include "inspection.h"
#include "ast_inspection.h"
#include "execution_inspection.h"
#include "atoms.h"

#include <sstream>

GC::Ptr<FuncProto> compile(VM& vm, const FunctionScope& exe)
{
	auto proto = vm.gc.create<FuncProto>(exe);
	Code code(*proto, vm.gc);
	compile(exe, code);
	return proto;
}

void mark(VM& vm)
{
	for(const auto& type : vm.base_types)
		type.mark(vm.gc);
	vm.module.mark(vm.gc);
	vm.thread.mark(vm.gc);
}

void VM::collectGarbage()
{
#if GC_ENABLED
	LOG_LINE("GARBAGE COLLECTION");
	mark(*this);
	gc.collect();
#endif
}

int manual_garbage_collection(VM& vm)
{
	LOG_LINE("MANUAL GARBAGE COLLECTION");
	mark(vm);
	return vm.gc.manual_collect();
}

VM::VM(): thread(*this)
{
	base_types =
	{
		Type(),
		Type(),
		Type(),
		Type(INumber::methods, RAFS::methods),
		Type(ISymbol::methods, RAFS::methods),
		Type(),
		Type(IFunction::methods),
		Type(),
		Type()
	};

	module.put(Symbol("Number"), make_value(&base_types[int(TValue::NUMBER)]));
	module.put(Symbol("Array"), make_value(&Array::get_type()));
	module.put(Symbol("Type"), make_value(&Type::get_type()));
	module.put(Symbol("Coroutine"), make_value(&Thread::get_type()));

	Atoms::Init(module, *this);
}

template<class T>
void log_section(const char* name, const T& x)
{
	LOG_LINE(name);
	LOG_LINE(x);
	LOG_LINE("");
}

ostream& operator<<(ostream& os, const vector<Lexeme>& tokens)
{
	return write_separated_by(os, tokens, " ");
}

Value VM::eval(CharIterator source)
{
	log_section("SOURCE FILE:", source);

	auto tokens = tokenize(source);
	log_section("LEXEMES:", tokens);

	auto ast = parse(tokens.data());
	log_section("SYNTAX TREE:", inspect(ast));

	auto exe = execution(ast, source);
	log_section("EXECUTION TREE:", inspect(exe));

	auto proto = compile(*this, exe);
	log_section("BYTE CODE:", *proto);

	auto result = thread.execute(*proto);
	log_section("RESULT STATE:", thread);

	return result;
}

Value VM::call(const vector<Value>& args)
{
	return thread.apply(args);
}

Value VM::get_global(const string& name)
{
	return module[Symbol(name)];
}

void VM::set_global(const string& name, Value value)
{
	module.put(Symbol(name), value);
}

bool supports(VM& vm, Value a, Symbol method)
{
	return a.getType(vm).getMethod(method).type != TValue::NIL;
}
