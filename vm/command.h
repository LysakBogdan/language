#ifndef COMMAND_H_INCLUDED
#define COMMAND_H_INCLUDED

#include <vector>
#include "runtime/value.h"
#include "error.h"

using namespace std;

enum class TCommand
{
	Push, Dip, SwapCmd,
	LocalsCount,
	Nil, GetConst,
	GetGlobal, SetGlobal, GetUpvalue, GetLocal, SetLocal, GetSelf, Get, Set, AddPair, Unpack,
	CmdApply, CmdDecons,
	NewArray, NewFunction, NewString,
	Jump, JumpTrue, JumpFalse,
	Ret, MatchRet, SetCount, Pop,
	FixedValues,
	Add, Subt, Mul, Div, iDiv, Mod,
	Eq, Gr, Lt, Ge, Le, Ne,
	Neg, Not,
	Inc, Dec,
	// TODO: remove Inspect
	Inspect, CheckCmd, AssertCmd
};

struct Command
{
	struct Info
	{
		const char* name;
		char arg_count;
		bool push = false;
	};
	static const Info info[];

	TCommand command;
	short a = 0, b = 0, c = 0;
};

optional<TCommand> PrimitiveCommand(Value);
ostream& print_command(ostream&, Command, const vector<Value>& constants);

#endif
