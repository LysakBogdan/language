import os, subprocess, time, sys, threading

RED = "\u001b[31m"
GREEN = "\u001b[32m"
ORANGE = "\u001b[38;5;214m"
ORANGERED = "\u001b[38;5;202m"
BOLD = "\u001b[1m"
RESET = "\u001b[0m"

def status_color(status):
	if status == 0:
		return GREEN
	elif status == 1:
		return ORANGE
	elif status == 2:
		return ORANGERED
	else:
		return RED

full = False
mode = 'debug'
post_build = False

if len(sys.argv) > 1:
	if sys.argv[1] == 'post_build':
		post_build = True
	if sys.argv[1] == 'release':
		mode = 'release'
	if len(sys.argv) > 2 and sys.argv[2] == 'full':
		full = True

print("mode = {}".format(mode))

failed = ""
total = passed = 0
folder = "specs"
executable = 'bin/{}/fabula'.format(mode)
if os.name == 'nt':
	executable += '.exe'

begin_time = time.time()

groups = {}

def test_group(group_path):
	global total, passed, failed
	tests = ""
	all_success = True
	for filename in os.listdir(group_path):
		[name, ext] = os.path.splitext(filename)
		if ext != ".fa":
			continue

		total += 1
		out = os.path.join(group_path, name + ".out")
		command_line = [executable, "--test", os.path.join(group_path, filename)]
		if os.path.exists(out):
			command_line.append(out)

		output = subprocess.Popen(command_line, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text = True)
		(_, stderr) = output.communicate()
		result_code = output.returncode
		success = result_code == 0

		if success:
			passed += 1
		else:
			failed += '{}\n{}: {}'.format(status_color(result_code), filename, stderr)
			all_success = False
		tests += status_color(result_code) + ' ' + ("_" if success else filename)
	return {'tests': tests, 'success': all_success}

def print_group(name, group):
	print(BOLD + (GREEN if group['success'] else RED) + name + RESET + group['tests'] + RESET)

def group_thread(group):
	groups[group] = test_group(os.path.join(folder, group))

ts = []

for group in os.listdir(folder):
	t = threading.Thread(target=group_thread, args = [group])
	t.start()
	ts.append(t)

for t in ts:
	t.join()

for name, group in sorted(groups.items()):
	print_group(name, group)

if full:
	print_group('doc examples', test_group('docs/examples'))
	print('HEAVY:')
	print_group('euler project', test_group('examples/project_euler'))
	print_group('stress tests', test_group('stress_tests'))

print("Time spent: {:.3f}s".format(time.time() - begin_time))
print("PASSED {}/{}".format(passed, total))

if failed:
	print(RED + "FAILED:" + failed + RESET)

if not post_build:
	input("Press Enter to continue...")