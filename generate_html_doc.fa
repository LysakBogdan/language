proc tag(name, text, params = {})
	res = '<' + name
	for name => param in params
		res += ' ' + name + ' = "' + param + '"'
	res + '>\n' + text + '\n</' + name + '>'

sections =
[
	['lexical', ['comments', 'identifiers', 'keywords', 'integer literals', 'float literals', 'strings', 'blobs', 'identation']],
	['values', ['basic types', 'truth', 'local variables']],
	['operators', ['arithmetic operators', 'comparison operators', 'logical operators', 'method call', 'precedence table']],
	['collections', ['arrays', 'blobs and strings', 'ranges', 'sequences', 'random access finite sequence', 'bags', 'infinite', 'lists',
		'associative arrays', 'dictionaries']],
	['control structures', ['if', 'while', 'for', 'do', 'short form', 'break', 'continue', 'pass', 'fail']],
	['functions', ['function definitions', 'varargs', 'lambdas', 'closures', 'map syntax', 'coroutines']],
	['pattern matching', ['constant check', 'name binding', 'structure matching', 'wildcard', 'pin', 'guards',
		'comparisons', 'standard patterns', 'combinations', 'function', 'other usage', 'operator in', 'multiple mappings', 'pseudo functions']],
	['assignment', ['assignment', 'swap', 'increment and decrement', 'compound assignments', 'methods']],
	['channels', ['sequences', 'standard']],
	['context variables', []],
	['junctions', []],
	['standard library', ['functions', 'number', 'list', 'dictionary', 'blob', 'string', 'file']],
	['interpreter usage', ['commands']]
]

proc process_line(line)
	res = ""
	+code? = +bold? = +italic? = nil

	i = 0
	while i < #line
		c = line[i]

		if c == ord('`')
			code? := !code?
			res.extend(code? ? '<code>' : '</code>')
		else if !code? && c == ord('*')
			if line[i + 1] == ord('*')
				i += 1
				bold? := !bold?
				res.extend(bold? ? '<b>' : '</b>')
			else
				italic? := !italic?
				res.extend(italic? ? '<em>' : '</em>')
		else if !code? && c == ord('\\')
			res << line[i += 1]
		else if !code? && c == ord('|')
			res.extend('</td><td>')
		else
			res << c
		i += 1
	return res

proc table_row('|' + line + '|')
	'<tr><td>' + process_line(line) + '</td></tr>'

proc table_header('|' + line + '|')
	res = "<tr>"
	for caption in line.split('|')
		res.extend('<th>' + caption + '</th>')
	res.extend('</tr>')
	res

proc code_block(code)
	'<pre><code>' + __highlight(code) + '</code></pre>'

proc md_to_html(text):
	html = ""
	+code? = +list? = +table? = nil
	code_buffer = ""

	for line in split(text, '\n')
		if '* ' + item = line
			if !list?
				list? := yes
				html.extend('<ul>')
			html.extend('<li>' + process_line(item) + '</li>')
			continue
		else if list?
			list? := nil
			html.extend('</ul>')

		if '|' + _ = line
			if !table?
				table? := yes
				html.extend('<table>' + table_header(line))
			else
				continue if '|:' + _ || '|-' + _ = line
				html.extend(table_row(line))
			continue
		else if table?
			table? := nil
			html.extend('</table>')

		if line == '```'
			code? := !code?
			if !code?
				html.extend(code_block(code_buffer))
				code_buffer := ""
		else if '#### ' + caption = line
			html.extend('<h4>' + caption + '</h4>')
		else if '@@ ' + path = line
			path := (('/' + _ = path) ? 'specs/' : 'docs/examples/') + path
			if example = file[path + '.fa']
				html.extend(code_block(example))
				if output = file[path + '.out']
					html.extend('Output:' + tag('pre', output))
			else
				print('Error, example ' + path + ' is not found')
		else if code?
			code_buffer.extend(line + '\n')
		else
			html.extend(line.empty? ? '<br>' : process_line(line) + ' ')

	html.extend('</ul>') if list?
	html.extend('</table>') if table?

	return html

start = time()
body = ""
index_table = ""
for i => [name, items] in sections
	body.extend(tag('h1', capitalize(name), {id: 'section_' + to_string(i)}))
	index_table.extend(tag('a', capitalize(name), {href: '#section_' + to_string(i)}) + '<br>')
	if description = file['docs/' + name + '/_.md']
		body.extend(md_to_html(description))

	sub_sections = ""
	for j => item in items
		sub_section = 'section_' + to_string(i) + '_' + to_string(j)
		sub_sections.extend(tag('li', tag('a', capitalize(item), {href: '#' + sub_section})))

		file_name = 'docs/' + name + '/' + item + '.md'
		doc = file[file_name]
		body.extend(tag('h2', capitalize(item), {id: sub_section}) + md_to_html(doc) + '\n')
	index_table.extend(tag('ul', sub_sections))

head = tag('title', 'Fabula') + tag('style', file['documentation.css'])
html = '<!DOCTYPE html>' + tag('html', tag('head', head) + tag('body', tag('aside', index_table, {class: 'sidebar'}) + tag('div', body, {class: 'page'})))
file['documentation.html'] := html

print('doc is generated\ntime spent: ', time() - start)