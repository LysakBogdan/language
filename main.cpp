#include "fabula.h"

#include <iostream>
#include <sstream>

using namespace Fabula;

int Test(string source)
{
	VM vm;
	return Evaluate(vm, source).code;
}

int TestOut(const string& source, const string& output)
{
	VM vm;

	stringstream output_stream;

	auto backup = cout.rdbuf();
	auto backup_err = cerr.rdbuf();

	cout.rdbuf(output_stream.rdbuf());
	cerr.rdbuf(output_stream.rdbuf());

	Evaluate(vm, source);

	cerr.rdbuf(backup_err);
	cout.rdbuf(backup);

	return output_stream.str() == output + "\n" ? 0 : 3;
}

void eval(VM& vm, const string& source)
{
	auto result = Evaluate(vm, source);
	show_result(result);
}

void REPL(VM& vm)
{
	string line;
	while(getline(cin, line))
	{
		if(line == "")
			break;
		eval(vm, line);
	}
}

int main(int argc, char *argv[])
{
	if(!argv[1])
	{
		cout << usage;
		return 0;
	}

	if(string("--version") == argv[1])
	{
		cout << Version << endl;
		return 0;
	}

	if(string("--help") == argv[1])
	{
		cout << usage;
		return 0;
	}

	if(string("--e") == argv[1])
	{
		if(argc < 3)
		{
			cout << usage;
			return 0;
		}

		VM vm;
		eval(vm, argv[2]);
		return 0;
	}

	if(argv[2] && string("--test") == argv[1])
	{
		if(argc > 3)
			return TestOut(load_file(argv[2]), load_file(argv[3]));
		return Test(load_file(argv[2]));
	}

	LOG_FILE("log.txt");
	VM vm;
	try
	{
		auto source = argv[1];
		eval(vm, load_file(source));
		REPL(vm);
	}
	catch(const exception& e)
	{
		log_error(e);
	}

	return 0;
}
