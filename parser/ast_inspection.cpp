#include "ast_inspection.h"

#include "inspection.h"
#include "formatting.h"

using namespace AST;

ostream& operator<<(ostream& os, const Expr& a)
{
	return os << inspect(a);
}

string inspect(Pass)
{
	return "pass";
}

string inspect(Void)
{
	return "void";
}

string inspect(Fail)
{
	return "fail";
}

string inspect(Self)
{
	return "self";
}

string inspect(Wildcard)
{
	return "_";
}

string inspect(const Contextual& o)
{
	return format("%{}", o.name.str());
}

string inspect(Symbol name)
{
	return name.str();
}

string inspect(const Apply& o)
{
	return LISP("apply", o.exp, o.args);
}

string inspect(const Action& o)
{
	return LISP("action", o.exp, o.args);
}

string inspect(const VariadicApply& o)
{
	return LISP("variadic_apply", o.exp, o.args);
}

string inspect(const Junction& o)
{
	return LISP("every", o.a);
}

string inspect(const Reduction& o)
{
	return LISP("reduction", o.op, o.init, o.seq);
}

string inspect(const Sequence& o)
{
	return format("{}; {}", o.a, o.b);
}

string inspect(const Cortege& o)
{
	ostringstream res;
	res << "[" << o << "]";
	return res.str();
}

string inspect(const Prefix& o)
{
	return LISP("prefix", inspect(o.op), o.exp);
}

string inspect(const Postfix& o)
{
	return LISP("postfix", inspect(o.op), o.exp);
}

string inspect(const Infix& o)
{
	return LISP("infix", inspect(o.op), o.a, o.b);
}

string inspect(const Definition& o)
{
	return LISP("def", o.name, o.exp);
}

string inspect(const ArrayConstructor& o)
{
	return LISP("array", o.exps);
}

string inspect(const TableConstructor& o)
{
	ostringstream ss;
	ss << "{ ";
	for(auto [key, value] : o.pairs)
		ss << inspect(key) << " => " << inspect(value) << ";";
	ss << " }";
	return ss.str();
}

string inspect(const MappingConstructor& o)
{
	ostringstream ss;
	ss << "[ ";
	for(auto [key, value] : o.pairs)
		ss << format("{} => {};", key, value);
	ss << " ]";
	return ss.str();
}

string inspect(const Do& o)
{
	return LISP("block", o.exp);
}

string inspect(const Generator& o)
{
	return LISP("__generator", o.exp);
}

string inspect(const If& o)
{
	return LISP("if", o.condition, o.yes, o.no);
}

string inspect(const While& o)
{
	return LISP("while", o.condition, o.body);
}

string inspect(const For& o)
{
	return LISP("for", o.var, o.collection, o.body);
}

string inspect(const ForPairs& o)
{
	return format("for {} => {} in {} : {}", o.key, o.value, o.collection, o.body);
}

string inspect(const Let& o)
{
	return LISP("let", o.pattern, o.value, o.body);
}

string inspect(const Break& b)
{
	return LISP("break", b.exp);
}

string inspect(const Continue& o)
{
	return LISP("continue", o.level);
}

string inspect(const Return& r)
{
	return LISP("return", r.exp);
}

string inspect(const Yield& o)
{
	return LISP("yield", o.exp);
}

string inspect(const Lambda& o)
{
	return LISP("lambda", o.params.args, o.params.is_variadic, o.exp);
}

string inspect(const Expr& e)
{
	return visit([](auto a) -> string { return inspect(a); }, *e);
}
