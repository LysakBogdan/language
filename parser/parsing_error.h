#ifndef PARSING_ERROR_H_INCLUDED
#define PARSING_ERROR_H_INCLUDED

#include "lexer/lexeme.h"
#include <string_view>

struct IdIsExpected {};
struct WrongBlock {};
template<class T>
struct Unexpected { T expected; Lexeme lx; };
struct UnexpectedLexeme { Lexeme lx; };
struct UnexpectedIndent {};

struct ParsingError
{
	std::variant<IdIsExpected, WrongBlock, Unexpected<TLexeme>,
		Unexpected<KeyToken>, UnexpectedLexeme, UnexpectedIndent> reason;
	std::string_view view;
};

#endif
