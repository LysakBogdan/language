#ifndef PARSER_H_INCLUDED
#define PARSER_H_INCLUDED

#include "lexer/lexer.h"
#include "grammar.h"

Expr parse(LexemeIterator ls);

#endif
