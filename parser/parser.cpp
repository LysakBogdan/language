#include "parser.h"

#include "tools.h"
#include "error.h"
#include "parsing_error.h"

using namespace AST;
using enum TLexeme;
using enum KeyToken;

bool startOfArg(Lexeme l)
{
	if(l == PROC)
		return true;

	switch(l.type)
	{
		case NUMBER:
		case STRING:
		case SYMBOL:
		case ID:
		case LH:
		case LQH:
		case LFH:
			return true;
		default:
			return false;
	}
}

Expr parse_expression(LexemeIterator& ls);
Expr field(LexemeIterator& ls);

Expr atom(LexemeIterator& ls);
Symbol id(LexemeIterator& ls);

Expr get_stat(LexemeIterator& ls);

Expr array_constructor(LexemeIterator&);
TableConstructor table(LexemeIterator&);

vector<Expr> cortege(LexemeIterator&);
Parameters patterns(LexemeIterator&);
Expr parse_exp(LexemeIterator& ls);

Expr block_node(LexemeIterator&);
Expr proc(LexemeIterator&);

template<class T>
void expect(LexemeIterator& ls, T x)
{
	if(*ls != x)
		throw ParsingError{Unexpected<T>{x, *ls}, ls->view};
	++ls;
}

auto parse_block(LexemeIterator& ls)
{
	if(*ls != COLON && *ls != NEW_LINE && *ls != BEGIN)
		throw ParsingError{WrongBlock{}, ls->view};

	if(*ls == COLON)
		++ls;
	if(*ls == NEW_LINE)
		++ls;
	return block_node(ls);
}

Expr optional_else(LexemeIterator& ls)
{
	if(*ls == ELSE)
		return block_node(++ls);

	if(ls[0] == NEW_LINE && ls[1] == ELSE)
		return block_node(ls += 2);

	return Pass{};
}

auto parse_if(LexemeIterator& ls)
{
	++ls; // if
	auto cond = parse_expression(ls);
	auto yes = parse_block(ls);
	auto no = optional_else(ls);
	return If{cond, yes, no};
}

auto parse_while(LexemeIterator& ls)
{
	auto cond = parse_expression(++ls);
	auto block = parse_block(ls);
	return While{cond, block};
}

tuple<optional<Expr>, Expr, Expr> generator(LexemeIterator& ls)
{
	auto value = parse_exp(++ls);
	if(*ls == PAIR)
	{
		auto key = value;
		value = parse_exp(++ls);
		expect(ls, IN);
		auto collection = parse_expression(ls);
		return {key, value, collection};
	}
	expect(ls, IN);
	auto collection = parse_expression(ls);
	return {nullopt, value, collection};
}

Expr parse_for(LexemeIterator& ls)
{
	auto [key, value, collection] = generator(ls);
	auto block = parse_block(ls);
	if(key)
		return ForPairs{*key, value, collection, block};
	return For{value, collection, block};
}

auto parse_with(LexemeIterator& ls)
{
	auto var = parse_exp(++ls);
	expect(ls, MATCH);
	auto value = parse_exp(ls);
	auto block = parse_block(ls);
	return Let{var, value, block};
}

Expr statement(LexemeIterator& ls)
{
	if(*ls == NEW_LINE || *ls == EOT)
		return Pass{};

	auto exp = get_stat(ls);

	while(*ls == IF || *ls == WHILE || *ls == FOR)
	{
		if(*ls == IF)
		{
			++ls;
			auto condition = get_stat(ls);
			auto no = optional_else(ls);
			exp = If{condition, exp, no};
		}
		else if(*ls == WHILE)
		{
			++ls;
			exp = While{get_stat(ls), exp};
		}
		else
		{
			auto [key, value, collection] = generator(ls);
			if(key)
				exp = ForPairs{*key, value, collection, exp};
			else
				exp = For{value, collection, exp};
		}
	}

	return exp;
}

Expr statements(LexemeIterator& ls)
{
	auto st = statement(ls);
	if(*ls == SEP || *ls == NEW_LINE)
		if(*(++ls) != END)
			return Sequence{st, statements(ls)};

	if(*ls == BEGIN)
		throw ParsingError{UnexpectedIndent{}, ls->view};

	return st;
}

bool stat_separator(Lexeme l)
{
	return l == END || l == SEP || l == IF || l == WHILE || l == FOR;
}

Pass parse_pass(LexemeIterator& ls)
{
	++ls;
	return {};
}

Fail parse_fail(LexemeIterator& ls)
{
	++ls;
	return {};
}

Return parse_return(LexemeIterator& ls)
{
	++ls;
	if(stat_separator(*ls))
		return {Void{}};
	return {parse_expression(ls)};
}

int parse_level(LexemeIterator& ls)
{
	if(*ls == LESS)
		NOT_IMPLEMENTED;
	return 1;
}

Break parse_break(LexemeIterator& ls)
{
	auto level = parse_level(++ls);
	if(stat_separator(*ls))
		return {Void{}, level};
	return {parse_expression(ls), level};
}

Continue parse_continue(LexemeIterator& ls)
{
	return {parse_level(++ls)};
}

Yield parse_yield(LexemeIterator& ls)
{
	return {parse_expression(++ls)};
}

Expr parse_action(LexemeIterator& ls)
{
	auto exp = atom(++ls);
	expect(ls, LH);
	Cortege args = {};
	if(*ls != RH)
		args = cortege(ls);
	expect(ls, RH);
	return Action{exp, args};
}

Expr get_stat(LexemeIterator& ls)
{
	if(*ls == PASS) return parse_pass(ls);
	if(*ls == FAIL) return parse_fail(ls);
	if(*ls == RETURN) return parse_return(ls);
	if(*ls == BREAK) return parse_break(ls);
	if(*ls == CONTINUE) return parse_continue(ls);
	if(*ls == YIELD) return parse_yield(ls);
	if(*ls == _DO) return parse_action(ls);

	return parse_expression(ls);
}

optional<KeyToken> parse_operator(const string& op);

auto oper(LexemeIterator& ls)
{
	return *parse_operator((ls++)->String);
}

Expr postfix_inc(LexemeIterator& ls)
{
	auto exp = field(ls);

	if(*ls == INC || *ls == DEC)
		return Postfix{oper(ls), exp};

	return exp;
}

using Parser = Expr (*)(LexemeIterator&);

template<Parser item, bool (*is_operator)(Lexeme l)>
Expr left_associative(LexemeIterator& ls)
{
	auto exp = item(ls);
	while(is_operator(*ls))
	{
		auto op = oper(ls);
		if(*ls == ELLIPSIS)
			exp = Reduction{op, exp, item(++ls)};
		else
			exp = Infix{op, exp, item(ls)};
	}
	return exp;
}

template<Parser item, bool (*is_operator)(Lexeme l)>
Expr right_associative(LexemeIterator& ls)
{
	auto exp = item(ls);

	if(is_operator(*ls))
	{
		auto op = oper(ls);
		if(*ls == ELLIPSIS)
			return Reduction{op, exp, right_associative<item, is_operator>(++ls)};
		return Infix{op, exp, right_associative<item, is_operator>(ls)};
	}

	return exp;
}

template<KeyToken ...ops>
auto one_of(Lexeme l)
{
	return ((l == ops) || ...);
}

template<Parser item, bool (*is_operator)(Lexeme l)>
Expr unary(LexemeIterator& ls)
{
	if(is_operator(*ls))
	{
		auto op = oper(ls);
		return Prefix{op, unary<item, is_operator>(ls)};
	}

	return item(ls);
}

constexpr auto exponent = right_associative<postfix_inc, one_of<POW>>;
constexpr auto parse_negate = unary<exponent, one_of<MINUS, PLUS, NOT, INC, DEC, LENGTH, PIN, MUL, INTERSECTION>>;
constexpr auto parse_mult = left_associative<parse_negate, one_of<MUL, DIV, IDIV, MOD>>;
constexpr auto parse_plus = left_associative<parse_mult, one_of<PLUS, MINUS>>;
constexpr auto set_operations = left_associative<parse_plus, one_of<INTERSECTION, UNION, PIN>>;
constexpr auto comparison = right_associative<set_operations, one_of<LESS, GREAT, LEQUAL, GEQUAL, EQUAL, NEQUAL, LIKE>>;
constexpr auto parse_and = left_associative<comparison, one_of<AND>>;
constexpr auto parse_or = left_associative<parse_and, one_of<OR>>;
constexpr auto parse_range = left_associative<parse_or, one_of<RANGE, PUSH>>;
constexpr auto item = right_associative<parse_exp, one_of<IN>>;

Expr parse_expression(LexemeIterator& ls)
{
	return right_associative<item, one_of<MATCH, ASSIGN, PLUSA, MINUSA, MULA, DIVA, IDIVA, MODA, POWA, SWAP>>(ls);
}

auto field_exp(Expr exp, Symbol field)
{
	return Apply{"field"_m, {exp, value(field)}};
}

string getString(LexemeIterator& ls)
{
	auto s = ls->String;
	++ls;
	return s;
}

Symbol id(LexemeIterator& ls)
{
	if(*ls != ID)
		throw ParsingError{IdIsExpected{}, ls->view};

	return Symbol(getString(ls));
}

Cortege call_args(LexemeIterator& ls)
{
	if(*ls == LH)
	{
		if(*++ls == RH)
		{
			++ls;
			return {};
		}
		auto args = cortege(ls);
		expect(ls, RH);
		return args;
	}
	else if(startOfArg(*ls))
		return cortege(ls);

	return {};
}

struct GetView
{
	LexemeIterator& ls;
	CharIterator begin;
	GetView(LexemeIterator& l): ls(l), begin(l->view.data())
	{}
	string_view get() const
	{
		return string_view(begin, ls->view.data() + ls->view.size() - begin);
	}
};

Wildcard wildcard(LexemeIterator& ls)
{
	++ls;
	return {};
}

Self self(LexemeIterator& ls)
{
	++ls;
	return {};
}

string string_literal(LexemeIterator& ls)
{
	return getString(ls);
}

Value number_literal(LexemeIterator& ls)
{
	return parse_number((ls++)->String);
}

Value symbol_literal(LexemeIterator& ls)
{
	return value(Symbol((ls++)->String));
}

Value literal(Value a, LexemeIterator& ls)
{
	++ls;
	return a;
}

Contextual contextual(LexemeIterator& ls)
{
	return {id(++ls)};
}

Expr group(LexemeIterator& ls)
{
	++ls; // (
	auto exp = *ls == RH ? Value::Nil : statement(ls);
	expect(ls, RH); // )
	return exp;
}

Expr atom(LexemeIterator& ls)
{
	switch(ls->type)
	{
		case NUMBER: return number_literal(ls);
		case STRING: return string_literal(ls);
		case SYMBOL: return symbol_literal(ls);
		case ID: return id(ls);
		case LH: return group(ls);
		case LFH: return table(ls);
		case LQH: return array_constructor(ls);
		case KEY:
		{
			// TODO: switch
			if(*ls == MOD) return contextual(ls);
			if(*ls == WILDCARD) return wildcard(ls);
			if(*ls == PROC) return proc(ls);
			if(*ls == SELF) return self(ls);
			if(*ls == NIL) return literal(Value::Nil, ls);
			if(*ls == YES) return literal(Value::YesNo(true), ls);
			if(*ls == NO) return literal(Value::YesNo(false), ls);

			if(*ls == EVERY) return Junction{TJunction::Every, atom(++ls)};
			if(*ls == SOME) return Junction{TJunction::Some, atom(++ls)};
		}
		default:
			throw ParsingError{UnexpectedLexeme{*ls}, ls->view};
	}
}

Expr field(LexemeIterator& ls)
{
	GetView view(ls);
	auto exp = atom(ls);

	while(*ls == FIELD || *ls == METHOD || *ls == LQH || startOfArg(*ls))
		if(*ls == FIELD)
			exp = field_exp(exp, id(++ls));
		else if(*ls == METHOD)
		{
			auto name = id(++ls);
			exp = Apply{Value::Method(name), cons(exp, call_args(ls))};
		}
		else if(*ls == LQH)
		{
			++ls; // [
			exp = Apply{"at"_m, cons(exp, cortege(ls))};
			++ls; // ]
		}
		else if(*ls == LH)
		{
			Cortege args = {};
			if(*++ls != RH)
				args = cortege(ls);
			if(*ls == ELLIPSIS)
			{
				ls++;
				exp = VariadicApply{exp, args, view.get()};
			}
			else
				exp = Apply{exp, args, view.get()};
			expect(ls, RH);
		}
		else
			exp = Apply{exp, cortege(ls)};

	return exp;
}

Expr getSubName(LexemeIterator& ls)
{
	auto exp = atom(ls);

	while(*ls == FIELD || *ls == LQH || *ls == METHOD)
		if(*ls == FIELD)
			exp = field_exp(exp, id(++ls));
		else if(*ls == LQH)
		{
			++ls; // [
			exp = Apply{"at"_m, cons(exp, cortege(ls))};
			++ls; // ]
		}
		else
			exp = Apply{Value::Method(id(++ls)), {exp}};

	return exp;
}

pair<Expr, Expr> getPair(LexemeIterator& ls)
{
	auto key_begin_view = ls->view;
	auto key = parse_exp(ls);
	if(*ls == COLON)
	{
		++ls; // :
		if(holds_alternative<Symbol>(*key))
			return {value(get<Symbol>(*key)), parse_exp(ls)};
		throw ParsingError{IdIsExpected{}, key_begin_view};
	}
	else
	{
		expect(ls, PAIR);
		return {key, parse_exp(ls)};
	}
}

Expr block_node(LexemeIterator& ls)
{
	if(*ls == BEGIN)
	{
		auto exp = statements(++ls);
		expect(ls, END);
		return exp;
	}
	return statement(ls);
}

Parameters sub_args(LexemeIterator& ls)
{
	if(*ls == LH)
	{
		++ls; // (
		if(*ls == RH)
		{
			++ls;
			return {};
		}
		auto exp = patterns(ls);
		expect(ls, RH); // )
		return exp;
	}
	return {};
}

Lambda lambda(LexemeIterator& ls)
{
	auto args = sub_args(ls);
	auto block = parse_block(ls);
	return {args, block};
}

Expr proc(LexemeIterator& ls)
{
	++ls; // sub
	if(*ls == LH)
		return lambda(ls);

	auto name = getSubName(ls);
	auto function = lambda(ls);

	return Definition{name, function};
}

Expr parse_ternary(LexemeIterator& ls)
{
	auto a = parse_range(ls);
	if(*ls != QUESTION_MARK)
		return a;

	auto b = parse_range(++ls);
	expect(ls, COLON);
	auto c = parse_range(ls);
	return If{a, b, c};
}

Expr parse_exp(LexemeIterator& ls)
{
	if(*ls == IF) return parse_if(ls);
	if(*ls == WHILE) return parse_while(ls);
	if(*ls == FOR) return parse_for(ls);
	if(*ls == WITH) return parse_with(ls);
	if(*ls == DO) return Do{parse_block(++ls)};
	if(*ls == GENERATOR) return Generator{parse_block(++ls)};

	return parse_ternary(ls);
}

Cortege cortege(LexemeIterator& ls)
{
	auto res = Cortege{statement(ls)};
	while(*ls == COMA)
		res.push_back(statement(++ls));
	return res;
}

Parameters patterns(LexemeIterator& ls)
{
	auto a = cortege(ls);
	if(*ls == ELLIPSIS)
	{
		++ls;
		return {a, true};
	}
	return {a};
}

TableConstructor table(LexemeIterator& ls)
{
	auto res = TableConstructor{};
	++ls; // {
	while(*ls != RFH)
	{
		res.pairs.push_back(getPair(ls));
		if(*ls != COMA && *ls != SEP)
			break;
		++ls; // ',' | ';'
	}
	expect(ls, RFH); // }
	return res;
}

Expr array_constructor(LexemeIterator& ls)
{
	++ls; // [
	if(*ls == RQH)
	{
		++ls;
		return ArrayConstructor{};
	}

	auto args = cortege(ls);

	if(*ls == PAIR)
	{
		if(args.size() != 1)
			throw ParsingError{UnexpectedLexeme{*ls}, ls->view};
		auto res = MappingConstructor{};
		auto key = args[0];
		auto value = statement(++ls);
		res.pairs.push_back({key, value});
		while(*ls == COMA)
		{
			auto key = statement(++ls);
			expect(ls, PAIR);
			auto value = statement(ls);
			res.pairs.push_back({key, value});
		}
		expect(ls, RQH);
		return res;
	}

	auto is_variadic = *ls == ELLIPSIS;
	if(is_variadic)
		ls++;

	expect(ls, RQH);
	return ArrayConstructor{args, is_variadic};
}

Expr parse(LexemeIterator ls)
{
	auto body = statements(ls);
	if(*ls != EOT)
		throw ParsingError{UnexpectedLexeme{*ls}, ls->view};
	return body;
}
