#ifndef AST_INSPECTION_H_INCLUDED
#define AST_INSPECTION_H_INCLUDED

#include "grammar.h"
#include <format>

string inspect(const Expr&);

template<>
struct std::formatter<Expr>: std::formatter<std::string>
{
	auto format(const Expr& e, format_context& c) const
	{
		return formatter<string>::format(inspect(e), c);
	}
};

#endif
