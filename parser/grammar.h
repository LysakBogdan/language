#ifndef GRAMMAR_H_INCLUDED
#define GRAMMAR_H_INCLUDED

#include <memory>
#include <variant>
#include <vector>

#include "runtime/value.h"

using namespace std;

struct ExprVariant;

struct Expr
{
	unique_ptr<ExprVariant> self;
	template<class T>
	Expr(T x): self(make_unique<ExprVariant>(ExprVariant{std::move(x)})) {}
	Expr(const Expr& x): self(make_unique<ExprVariant>(*x.self)) {}
	Expr(Expr&&) noexcept = default;

	Expr& operator=(const Expr& x)
	{
		return *this = Expr(x);
	}
	Expr& operator=(Expr&&) noexcept = default;
};

namespace AST
{
	struct Pass {};
	struct Void {};
	struct Fail {};
	struct Self {};
	struct Wildcard {};
	struct Contextual {Symbol name;};

	using Cortege = vector<Expr>;
	struct Apply {Expr exp; Cortege args; string_view view;};
	struct VariadicApply {Expr exp; Cortege args; string_view view;};
	enum class TJunction {Every, Some};
	struct Junction{TJunction type; Expr a;};

	struct Prefix {KeyToken op; Expr exp;};
	struct Postfix {KeyToken op; Expr exp;};
	struct Infix {KeyToken op; Expr a, b;};
	struct Sequence {Expr a, b;};
	struct Reduction{KeyToken op; Expr init, seq;};

	struct ArrayConstructor {Cortege exps; bool is_variadic = false;};
	struct TableConstructor {vector<pair<Expr, Expr>> pairs;};
	struct MappingConstructor {vector<pair<Expr, Expr>> pairs;};

	struct Definition {Expr name, exp;};

	struct If {Expr condition; Expr yes; Expr no;};
	struct While {Expr condition; Expr body;};
	struct For {Expr var; Expr collection; Expr body;};
	struct ForPairs {Expr key; Expr value; Expr collection; Expr body;};
	struct Do {Expr exp;};
	struct Let {Expr pattern, value, body;};

	struct Generator {Expr exp;};
	struct Action {Expr exp; Cortege args; string_view view;};

	struct Break {Expr exp; int level = 1;};
	struct Continue {int level = 1;};
	struct Return {Expr exp;};
	struct Yield {Expr exp;};

	struct Parameters {Cortege args; bool is_variadic = false;};
	struct Lambda {Parameters params; Expr exp;};

	using Variant = variant
	<
		Pass, Void, Fail, Symbol, string, Value, Self, Wildcard, Contextual,
		Cortege, Apply, VariadicApply, Junction,
		Prefix, Postfix, Infix, Sequence, Reduction,
		ArrayConstructor, TableConstructor, MappingConstructor,
		Definition,
		If, While, For, ForPairs, Do, Let, Generator, Action,
		Break, Continue, Return, Yield,
		Lambda
	>;
}

struct ExprVariant
{
	AST::Variant value;
};

inline AST::Variant& operator*(const Expr& o)
{
	return o.self->value;
}

#endif
