`fabula [option] [argument]`

| Options and arguments | |
|:-----|:-----|
|`file`| Execute the script file |
|`--version`| Print the version number |
|`--help`| Print the usage information |
|`--e string`| Execute the string |