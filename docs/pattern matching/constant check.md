The simplest pattern is a constant, just a check for equality.
```
1 = 1 // succes
4 = 2 + 2 // succes
7 = 8 // fail
```