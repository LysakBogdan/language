In case the pattern is an identifier, we have a regular value definition.
```
x = 42
```
Note that direct assignment of nil does not work because an expression like `a = nil` fails. But you can still do this by using the identity operator `+`.
```
+a = nil
```