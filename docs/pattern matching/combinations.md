Patterns can be combined using logical operators.
```
array && [first, second] = [2, 3]
5 || 6 = x
```

And nested also:
```
[42, [a, 3 + b], _] = [42, [0, 10], 5]
// a = 0, b = 7
```

You can set a default value when binding a variable. This value will be used when **nil** is gotten.
```
(x = 0) = foo()
```