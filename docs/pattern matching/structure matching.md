More interesting usage of pattern matching is deconstruction of structures.
```
[x, y, z] = [3, 4, 5]

{x: left, y: top} = {x: 33, y: 55}
```