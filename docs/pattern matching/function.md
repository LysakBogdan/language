If a pattern is used frequently, to avoid duplication of code, it can be extracted to an auxiliary function.
```
proc f(x): [x + 3]

f(a) = [10] // a = 7
```

A variable number of arguments works as well.
```
proc f(a, b, c):
	[a + 10, b + 10, c + 10]

f(p...) = [11, 15, 17] // p = [1, 5, 7]
```