You can impose conditions on the patterns using **if**.
```
([x, y] if x > y) = [20, 10]
```