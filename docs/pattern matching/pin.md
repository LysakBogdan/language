What if we want to compare with an existing variable rather than introduce a new one?
There is the pin operator `^` for this purpose.
```
y = 8
[x, ^y] = [3, 8]
```