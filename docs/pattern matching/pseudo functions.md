Some functions cannot even be evaluated directly. They are used only as a pattern.
```
// seq is for deconstructing a sequence
seq(first, rest) = [2, 3, 5, 7]
// first = 2, rest = [3, 5, 7]
```