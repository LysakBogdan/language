Patterns for some standard operations are supported.
```
-a = 10
b - 2 = 9
10 + c + 10 = 120
!d = yes
```
Even a range can be a pattern.
```
a..b = [17, 18, 19, 20] // a = 17, b = 20
```