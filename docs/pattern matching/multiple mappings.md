In the map syntax, there may be several mappings separated by comma. Such functions calculate and return the expression corresponding to the first successfully matched pattern.
```
f = [42 => 777, x => 10*x]

f(42) // 777
f(5) // 50

sign = [x > 0 => '+', x < 0 => '-', 0 => '0']

print(sign(5)) // +
print(sign(-3)) // -
print(sign(0)) // 0
```