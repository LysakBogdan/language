Comparisons can be used to filter values.
```
(a > 0) = 7 // a = 7
(b < 3) = 7 // fail
```