The `in` operator matches a pattern with the first suitable element of a sequence.
```
a in [7, 8] // a = 7
b > 10 in [3, 12, 8] // b = 12
```