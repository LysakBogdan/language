Beside the `=` operator, pattern matching works for function parameters and iteration variables of the **for** loop.
```
proc f([a, b]):
	a + b

f([9, 11]) // 20
```
```
p = [[a, b] => a*b]
p([5, 9]) // 45
```
```
// prints 2 5 7
for 100 + x in [102, 105, 107]:
	print(x)
```
If a pattern fails for any argument in a function, the entire function call fails immediately.

If the pattern fails on an element, the iteration is skipped.
```
// prints 9, 100, 18
for x > 5 in [9, 2, 100, 18, -33]
	print(x)
```