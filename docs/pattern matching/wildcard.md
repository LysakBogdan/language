The wildcard `_` matches everything.
```
a = [11, 22, 33]
[_, x, _] = a
```