The **while** loop repeats its body as long as its condition is true.

**while** *condition* : *block*
```
i = 1
while i < n:
	i *= 2
```
if after the condition the block starts with a new line, the colon can be omitted.