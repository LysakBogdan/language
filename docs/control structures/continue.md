The **continue** statement is used to instantly jump to the next iteration of a loop.
A **continue** may occur only inside a **while** or **for** cycle.
```
s = 0
for e in list
	if e % 2 <> 0
		continue
	s += e
```
*NYI: continue for an external of nested loops.*