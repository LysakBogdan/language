The **if** statement, the **while** and the **for** loop have the short forms:

*statement* **if** *condition* [**else** *statement*]

*statement* **while** *condition*

*statement* **for** *item* **in** *collection*

*statement* **for** *key* => *value* **in** *collection*

```
print(x) if x > 0
i *= 2 while i < n
```