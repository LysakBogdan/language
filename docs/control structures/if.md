The **if** statement is used for conditional execution.

**if** *condition* : *block* [**else** *block*]
```
if x == 5: print('five')
```
```
if a >= 0:
	print('+')
else
	print('-')
```
if after the condition the block starts with a new line, the colon can be omitted.

**if** is an expression, it returns a value. Thus we can write the above code like this:
```
sign = if a >= 0
	'+'
else
	'-'
print(sign)
```
or like this:
```
print(if a >= 0: '+' else '-')
```