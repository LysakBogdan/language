The **pass** statement does nothing. It can be useful as a placeholder or a replacement for an empty block.
```
while f()
	pass
```