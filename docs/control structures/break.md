There is the **break** statement for premature exit from a loop.
A **break** may occur only inside a **while** or **for** cycle, and it ends the innermost enclosing loop.
```
for e in list
	if e == value
		break
```
Loops are expressions and return a value, you can also use the break statement to return that value, by default **nil** is returned.
```
found = for e in list
	if e > limit
		break e
```
*NYI: Exit from several nested loops.*