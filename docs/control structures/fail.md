The **fail** statement just fails explicitly. For example, this code:
```
fail if x == 5
return 2*x
```
fails if x = 5, otherwise returns 2\*x.