The **for** loop is used to execute the body for all element in some collection.

**for** *item* **in** *collection* : *block*
```
p = 1
for e in [2, 3, 7, 12]
	p *= e
```

In addition, there is a syntax for traversing all pairs in the associative array.

**for** *key* => *value* **in** *collection* : *block*
@@ /cycles/for_pairs