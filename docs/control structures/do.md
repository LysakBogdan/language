**if**, **while** and **for** introduce new scope, so variables defined in their bodies or conditions are local to the construct.
@@ /control/if_scope

The **do** statement just creates a nested scope.
@@ /control/do