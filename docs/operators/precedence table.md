The following table describes the operators from highest to lowest precedence.

| Operator | Form | Associativity |
|:--------:|:----:|:-------------:|
| `[] () . :` | binary | left |
| `++ --` | unary postfix | - |
| `**` | binary | right |
| `- + ! ++ -- # ^ *` | unary prefix | - |
| `* / \\ %` | binary | left |
| `+ -` | binary | left |
| `& | ^` | binary | left |
| `== <> < > <= >= ~` | binary | right |
| `&&` | binary | left |
| `||` | binary | left |
| `.. <<` | binary | left |
| `? :` | ternary | - |
| `in` | binary | right |
| `= := op= <->` | binary | right |
| `;` | binary | left |