Fabula supports the standard arithmetic operators: addition `+`, subtraction `-`, multiplication `*` and division `/`.
Also, the modulo `%`, the floor division `\\` and the exponentiation `**`.

All operators have the standard precedence as in mathematics.
The exponent operator `**` has the right associativity, so for example `2 ** 2 ** 3 = 2 ** (2 ** 3) = 2 ** 8 = 256`.

You can change the sign of the number with a unary minus `-`.