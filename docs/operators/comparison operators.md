There are next comparison operators `<`, `>`, `<=`, `>=`, `==`, `<>`.

Unlike other languages, comparison operators do not return simple boolean values. If the comparison is successful, the result will be the first operand. If not then it's fail.
```
42 > 5 // 42
42 > 100 // fail
```
Another feature is that they have the right associativity. Which together allows us to make chain comparisons.
```
1 < 6 < 10
// 1 < (6 < 10)
// 1 < 6
// 1, success
```
It works, because if any sub-comparison fails, the fail will propagate to the outermost. As any operation with the fail returns the fail.
```
100 > 33 > 52
// 100 > (33 > 52)
// 100 > fail
// fail
```