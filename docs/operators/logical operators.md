There are common logical operators `&&`, `||`, and `!`.

The `&&` operator returns **nil** if the first argument is **nil**, and the second argument otherwise.
```
yes && nil // nil
nil && yes // nil
5 && 7 // 7
```
The `||` operator returns the first argument if this is a true value; otherwise, it returns its second argument.
```
yes || nil // yes
nil || nil || 8 // 8
5 > 10 || 7 || nil // 7
```
Both `&&` and `||` operators don't evaluate the second argument unless necessary.

The `!` operator negates its argument and always returns _ or **nil**.
```
!nil // _
!yes // nil
!100 // nil
```

Because only a **nil** value is considered false. In the conditions to check if the boolean value is **yes**, there is the `*` operator.
```
foo = yes
*foo // success
*no // fail
```

Conversely, to convert a condition to a boolean value, there is the `bool` function.
```
bool(5 > 2) // yes
bool(2 == 3) // no
```