Almost every operation in Fabula can be done with a method call.

object.method(*arguments*)

```
5.plus(6) // 11
8.negate() // -8
```

If there are no arguments, the parentheses can be omitted:
```
23.sin
```

Method calls can be chained:
```
9.cos.pow(3).negate
```