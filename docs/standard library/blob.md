Blobs have similar methods as in lists, and have a few extras.

| Method | Description |
|:-------|:------------|
|`blob.str`| converts the blob to the corresponding string |
|`blob.extend(s)`| adds string s to the end of the blob |