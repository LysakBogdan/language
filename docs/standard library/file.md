For working with the file system, there is a `file` pseudo-dictionary. It provides access to the contents of a file by its path.
```
// read
text = file['book.txt']
// write
file['copy.txt'] := text
```