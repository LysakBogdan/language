| Method | Description |
|:-------|:------------|
|`s.blob`| returns the corresponding blob |
|`s.split(separator = ' ')`| splits a string into a list of parts by separator |
|`s.ord`| takes a single character string and returns its code as an integer |
|`chr(code)`| takes an ASCII code and converts it to a single character string |
|`s.to_number`| converts the string to a number |
|`s.parse_escaped`| replaces all escape sequences with corresponding character |