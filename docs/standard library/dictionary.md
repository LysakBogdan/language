| Method | Description |
|:-------|:------------|
|`a.at(i)`| `a[i]` |
|`a.size`| returns the number of key => value pairs |
|`a.empty?`| checks whether a is empty |
|`a.clear`| removes all elements of the dictionary |
|`a.keys`| returns the list of keys of a |
|`a.values`| returns the list of values of a |
|`a.key_of(x)`| finds a key that `a[key] == x` |
|`a.keys_of(x)`| finds all keys that `a[key] == x` and returns them as an array |
|`a.each(f)`| applies the function f to all values of the dictionary |
|`a.each_pair(f)`| applies the function f to all key => value pairs of the dictionary `f(key, value)` |