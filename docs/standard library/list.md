Lists support the RAFS interface plus the following mutation methods.

| Method | Description |
|:-------|:------------|
|`a[i] := e`| sets the element at the index |
|`a.size := n`| change the size of the list, missing elements = **nil** |
|`a.first := e`| sets the first element in the list |
|`a.last := e`| set the last element in the list |
|`a.push(e)`| adds e to the end of the list |
|`a.pop`| removes and returns the last element of the list |
|`a.insert(index, e)`| inserts e before the element at index |
|`a.remove(index)`| removes the element at index |
|`a.clear`| removes all elements of the list |
|`a.reverse!`| reverse order of elements in the list |
|`a.sort(pred = less)`| sorts elements in the list. pred is a two-argument function that succeeds if the first argument must come before the second in the sorted list |
|`a.extend(b)`| appends all elements of b to the end of the list, b must be a RAFS |