| Method | Description |
|:-------|:------------|
|`x.negate`| -x |
|`x.plus(y)`| x + y |
|`x.minus(y)`| x - y |
|`x.mult(y)`| x \* y |
|`x.div(y)`| x / y |
|`x.idiv(y)`| x \\\\ y |
|`x.mod(y)`| x % y |
|`x.pow(y)`| x \*\* y |
|`x.abs`| returns absolute value of x |
|`x.min(y)`| returns the minimum of x and y |
|`x.max(y)`| returns the maximum of x and y |
|`x.cos`| cosine of x |
|`x.sin`| sine of x |
|`x.sqrt`| square root of x |
|`x.exp`| the value of e raised to the xth power |
|`x.log`| natural logarithm of x |
|`x.log10`| logarithm of x to the base of 10 |
|`x.ceil`| returns the smallest integer greater than or equal to x |
|`x.floor`| returns the largest integer less than or equal to x |