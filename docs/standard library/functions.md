Almost every operator has a corresponding standard function.

| Operator | Function |
|:--------:|:--------:|
|`!a`| `not(a)`|
|`-a`| `negate(a)`|
|`++i`| `i := next(i)`|
|`--i`| `i := prev(i)`|
|`a + b`| `plus(a, b)`|
|`a - b`| `minus(a, b)`|
|`a * b`| `mult(a, b)`|
|`a / b`| `div(a, b)`|
|`a \\ b`| `idiv(a, b)`|
|`a % b`| `mod(a, b)`|
|`a == b`| `equal(a, b)` |
|`a <> b`| `not_equal(a, b)` |
|`a < b`| `less(a, b)` |
|`a > b`| `greater(a, b)` |
|`a <= b`| `less_equal(a, b)` |
|`a >= b`| `greater_equal(a, b)` |
|`#a`| `size(a)` |
|`a[i]`| `at(a, i)` |
|`[a, b, c]`| `array(a, b, c)` |
|`a..b`| `range(a, b)` |

| Function | Description |
|:---------|:------------|
| `identity` | returns its argument unchanged |
| `bool` | returns **yes** if its argument is true, **no** otherwise |
| `to_string` | returns a string representation of the value |
| `print(args...)` | prints the arguments to standard output |
| `inspect` | returns a debug representation of the value |
| `iota()` | returns an infinite sequence 0, 1, 2, ... |
| `seq(first, rest) = sq` | the pseudo function for sequence deconstruction |
| `finite_list(count, at) = sq` | the pseudo function for finite sequence matching, count = #collection, at is a function to access a random element of the collection by index |
| `reduce(op, sequence, init)` | performs reduction of the sequence. op is an arbitrary binary function |
| `assert(condition)` | checks whether the condition is true, causes an error, if not |
| `get(channel)` | gets value from the channel |
| `send(channel, value)` | sends value to the channel |
| `seq_channel(sequence)` | makes a channel based on the sequence |
| `accumulator(init)` | makes a channel that summarizes all received numbers and returns the current sum |
| `some_in(sequence, pred)` | checks whether pred is true for at least one sequence element |
| `every_in(sequence, pred)` | checks whether pred is true for all elements of the sequence |
| `random(collection)` | collection is a random access finite sequence, returns a random element |
| `int(str, base = 10)` | converts the string str into an integer. By default, base = 10. Also supported base = 2, 8 or 16 |
| `capitalize(str)` | returns a copy of the string with the first character in uppercase |
| `yield(value)` | suspends its coroutine execution and returns the value |
| `yield_pair(key, value)` | same as `yield`, but returns the key-value pair |
| `read_from_file(path)` | reads the file as a blob by the path, returns nil if the file does not exist |
| `save_to_file(blob, path)` | saves the blob as a file by the path |
| `system(command)` | executes a system command |
| `time()` | returns current time in seconds since the epoch |
| `pair(key, value)` | creates a function {key => value} |
| `pair(key, value) = f` | matches function f as a single pair of values {key => value} |
| `contains(sequence, subsequence)` | checks if a sequence contains a subsequence. |
| `to_array(sequence)` | converts a sequence into an array |