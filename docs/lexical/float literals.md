A real number starts with an optional integer part that written in decimal, followed by an fractional part and an optional exponential part
```
30.0 // same as 30
1e6 // same as 1_000_000
3e-5 // approximately 0.00003
.1 // 0.1
```