Natural numbers can be written as a sequence of digits.
```
42
```
Literals may be also given in binary, octal, and hexadecimal notation.
```
0B10101110 // binary
0o777 // octal
0xFE // hexadecimal
```
Underscores can be used to make long numbers more readable.
```
1_000_000
```