A string is an arbitrary sequence of characters enclosed in single quotation marks.
```
'two words'
'!$Q&>a:...'
```
The backslash `\` character is used to escape characters.
```
'\n' // new line character
```
Escape character can be one of standard:
* new line `\n`
* tab `\t`
* carriage return `\r`
* vertical tab `\v`
* form feed `\f`
* backspace `\b`
* bell `\a`

Decimal digits specify an arbitrary character by its code, like `\32` for space, or `\0` for NUL.
Any other character means itself (e.g., `\\` or `\'`).


If the string represents a name of a unique identifier, it can be written using a dollar sign followed by the identifier.
```
$name
$empty?
```

Fabula also supports block string literals.
They start with `$$$` immediately followed by indented block of text:
```
$$$
	Love the life you live.
	Live the life you love.
```
The indentation level is defined by the first line of the block and is not part of the literal itself.

If you need a whitespace at the beginning of the first line, just escape it with `\`:
```
$$$
	\ 123
```