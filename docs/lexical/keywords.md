The following keywords are reserved and cannot be used as names:
```
nil yes no
if else while for in with do
pass fail return break continue
proc self _ every some
```
Also, identifiers that begin with an underscore followed by a capital letter or with two underscore are considered reserved for internal use.