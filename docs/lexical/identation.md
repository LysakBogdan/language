Fabula is an indentation sensitive language. Spaces and tabs at the beginning of a line is used to compute the indentation level of the line.
This information is used to determine the block structure of statements. Statements inside a block are separated by a line end or semicolon.
Empty lines are ignored and do not affect the block structure.