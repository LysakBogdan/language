A comment starts with two slash signs and runs until the end of the line. Comments are ignored by the syntax.
```
// A comment ...
```