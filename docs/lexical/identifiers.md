Identifiers are any non-empty sequences of letters, digits and underscores that do not begin with a digit.
Also, they can end with one of the characters **!** **?** **#**.
Identifiers are case-sensitive, so **foo**, **Foo** and **FOO** are distinct names.

Examples of valid identifiers:
```
i name H24 _310_to_Y open? max_element#
```