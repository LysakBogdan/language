In Fabula, each expression succeeds or fails. Successful expressions produce values.
Fails, on the other hand, are passed to external expressions and propagated further by default.
**nil** stands for failure "value".

```
2 + 2 // 4, success
2 + nil // fail
4 + (nil - 3) // fail
```

Only **nil** is false. Any other value is true. Including **yes** and **no**, both are considered true.