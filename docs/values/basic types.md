Fabula is a dynamically typed language with garbage collection. Basic types in Fabula:

* Nil `nil`
* Unit `_`
* Boolean `yes`, `no`
* Number `42`, `0.2`, `1e10`
* Blob (Mutable string) `"hello"`, `"world"`
* String `$foo`, `'bar'`
* Array `[5, 25]`, `[$a, $b, $c]`
* Associative array `{key => value}`
* Function `proc(x): x*x`
* Thread (Coroutine)

Nil has a single value **nil**. It indicates the absence of a value.

Unit has only one value **_** which represents an entity without information.

Blobs represent mutable byte sequences. Strings instead are immutable.
There may be many different blobs with the same content, but the string is always unique.