Local variables are used to store values for intermediate calculations.
The most common way to define a local variable is by using operator `=`.
```
two = 2
```
It binds the name to the value. And the name can be used in any expression instead of the value:
```
greeting = 'Hello'
print(greeting)
```
A variable exists until the end of the block it was defined. A local variable inside a nested block shadows an outer with the same name.