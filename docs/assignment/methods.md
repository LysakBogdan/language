We can use a method call as a reference for assignment.
```
object.method(arg1, arg2) := value
```
Which is actually syntactic sugar for another method call:
```
object.set_method(value, arg1, arg2)
```
For example:
```
a = list()
a.size := 5 // a.set_size(5)
a.first := 1 // a.set_first(1)
a.at(index) := 27 // a.set_at(27, index)
```