So, for initial assignment the `=` operator is used. But if we need to reassign an existing var, there is the `:=` operator for that:
```
x = 0
x := 2
```

It works in the same way as pattern matching, except that it does not create new binding variables, but modifies existing ones.
Assignments can be chained:
```
a := b := 10
```

For example, this is how the calculation of Fibonacci numbers might look like:
```
proc fib(n)
	a = b = 1
	while a < n
		[a, b] := [b, a + b]
	return a
```

It is worth noting that constants can be used in the same way as for pattern matching:
```
4 := 2 + 2 // success
3 := 7 // fail
```

However, if there is no local var bound to the name, the value will be assigned to the corresponding global variable. If no such variable exists, it will be created.
```
// defines a global variable
Resolution :=
{
	width: 1280,
	height: 1024
}
```