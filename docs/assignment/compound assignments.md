Compound assignment operators set the value of the variable a to the result of a binary operation with the right operand.
```
a = 5
a += 7 // a = 12
a /= 3 // a = 4
```