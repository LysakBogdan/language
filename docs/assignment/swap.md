Suppose we have two variables a and b. You can use an array assignment to swap their values.
```
[a, b] := [b, a]
```
But there is the swap operator for this purpose.
```
a <-> b
```
It also works for more complex cases.
```
pair = [1, 2]
x = 5; y = 8

pair <-> [x, y] // makes pair = [5, 8], x = 1, y = 2
```