Prefix increment and decrement operators increments or decrements the value of a variable and returns the variable as a result.
```
i = 4; j = 10
print(++i) // 5; i = 5
print(--j) // 9; i = 9
```

There are also postfix versions that increment or decrement the value of a variable and return the value from before the operation.
```
i = 4; j = 10
print(i++) // 4; i = 5
print(j--) // 10; i = 9
```