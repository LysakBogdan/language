The `seq_channel` function makes a source channel from a sequence.
```
a = seq_channel([2, 3, 5])

*a // 2
*a // 3
*a // 5
*a // nil
```

Since coroutines are sequences, we can also turn them into channels.
```
proc generator:
	yield(1)
	yield(8)
	yield(9)
	yield(5)

c = seq_channel(Coroutine(generator))

a = [*c, *c, *c] // [1, 8, 9]
```