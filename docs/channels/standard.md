The `discard` channel ignores everything that is sent to him.
```
*discard := $no_effect
```

The `accumulator` channel summarizes all received numbers and sends the current value.
```
a = accumulator(0)
*a := 14
*a := 22

sum = *a // 36
```