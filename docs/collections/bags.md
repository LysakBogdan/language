A bag is a collection containing an unordered list of elements.
Bags can be created using the `bag` function, which takes a list of items.
```
a = bag(1, 2, 17, 4)
```
The `pick` function looks for an element of the bag and if it is there - returns a copy of the bag without it. Fails, otherwise.
```
rest = pick(a, 17) // rest = bag(1, 2, 4)
pick(a, 5) // fail
```
Bag is a sequence, but the order of the items is undefined. So, for example, operator `#` gets the size of a bag.
```
length = #a // 4
```
#### Set operations
Bags support basic set operations like union (`|` operator), intersection `&`, symmetric difference `^`.
```
bag(2, 5) | bag(5, 5, 3) // bag(2, 3, 5, 5)
bag(2, 5, 7) & bag(5, 5, 3, 2) // bag(2, 5)
bag(2, 5, 7) ^ bag(5, 5, 3, 2) // bag(5, 7, 3)
```
`bag_difference` returns the difference between two bags.

Also, we can test whether a bag contains another bag using `>=` or `<=`. Operators `<` and `>` work the same but do not allow the bags to be equal.
```
bag(2, 3) <= bag(2, 5, 3) // success
bag(2, 5, 3) >= bag(2, 4) // fail
bag() < bag(0, 10) // success
bag(1) > bag(1) // fail
```
#### Numbers
A natural number can be thought of as a bag of units. That is, `&`, `|` and `^` work for numbers.
The union of numbers is maximum, the intersection is minimum. Whereas the symmetric difference is just absolute difference.
```
5 | 7 == 7
5 & 7 == 5
5 ^ 7 == 2
```