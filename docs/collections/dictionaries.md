The `dict` function takes a collection or a function, and creates a dictionary with key => value pairs from that.
```
a = dict({})
a.empty? // yes
b = dict({5 => 6, 7 => 8})
b.size // 2
```

Dictionaries are mutable associative arrays. Using the `[]` operator you can get or set value for an argument (key).
```
point = dict({x: 0, y: 0, z: 0})
point[$x] := 7 // {x: 7, y: 0, z: 0}
point[$x] // 7
```

A shorthand syntax is available for identifier string keys.
```
print(point:x)
point:z := point:z + 10
```