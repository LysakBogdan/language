An array is a collection containing an ordered list of elements.
Arrays can be created with an array literal denoted by square brackets `[]` and individual elements separated by a comma `,`:
```
a = [1, 2, 17, 4]
```
Individual array elements can be accessed by indexing with square brackets. Indexing starts at 0.
```
third_element = a[2] // 17
```

Operator `#` gets the size of an array.
```
length = #a // 4
```