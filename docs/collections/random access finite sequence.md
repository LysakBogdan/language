Random Access Finite Sequence (RAFS) is subclass of sequence, where arbitrary element can be accessed in constant time by its index. Indexes start with 0.

Operator `#` gets the length of RAFS. Operator `[]` can be used to access an element.

Arrays, blobs, strings, ranges are RAFS.

```
a = [2, 3, 5, 7, 11]

#a // 5
a[0] // 2
a[4] // 11
```

All RAFSs share the common interface.

| Method | Description |
|:-------|:------------|
|`a.at(i) or a[i]`| access specified element |
|`a.size or #a`| returns the number of elements |
|`a.empty?`| checks whether the sequence is empty |
|`a.first`| returns the first element in the sequence |
|`a.last`| returns the last element in the sequence |
|`a.equals(b) or a ~ b`| checks whether a equals b (`==` compares only references) |
|`a.random` | returns a random element |
|`a.each(f)`| applies the function f to all elements of the sequence |
|`a.each_pair(f)`| applies the function f to all index => value pairs of the sequence `f(index, value)` |
|`a.slice(start = 0, stop = #a)`| returns a subsequence delimited by a pair of indices |

Fabula aims to provide uniform interfaces, even for base types. For this, natural numbers support the RAFS interface, they behave like arrays of units.
```
0.empty? // yes
5.last // _
#57 // 57
4 ~ [_, _, _, _] // yes
```