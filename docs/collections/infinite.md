Sequences do not have to be finite, the simplest example is `infinity` just an infinite sequence of units.

```
for _ in infinity // endless loop
	// ...

7 <= infinity // success, 7
7 >= infinity // fail
```

Product works for `infinity` too.
```
cycle = infinity*[1, 2, 3] // sequence: 1, 2, 3, 1, 2, 3, ...
[2, 3, 1] <= cycle // success
```