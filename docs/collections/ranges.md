A range represents a list of values from a beginning to an end.
In general, ranges behave in the same way as similar arrays.
```
a = 20..30
a[0] // 20
a[5] // 25
a[12] // nil
#a // 11
```