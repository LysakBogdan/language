An associative array literal is a list of key => value pairs enclosed in curly braces. Keys and values can be of any type except nil.
```
a = {$x => 100, $y => 200, $z => 300}
a[$y] // 200
```

If the key is a string that can be used as an identifier, then instead of writing `$id =>` you can just write `id:`.
```
a = {x: 100, y: 200, z: 300}
```

These keys are also accessible with `:`.
```
print(a:x) // 100
print(a:y) // 200
print(a:z) // 300
```