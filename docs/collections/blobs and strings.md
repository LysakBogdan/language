Blobs and strings just represent a sequence of bytes. They have array interface.
```
a = $Hello

#a // 5
a[0] // 72, 'H'
a[4] // 111, 'o'
```
The blobs can also be changed.
```
s = "deer"
s.reverse! // s = "reed"
s << 115 // s = "reeds"
```