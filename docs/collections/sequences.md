In Fabula, all consecutive data structures have common sequence interface. So you can work with the same way.
Arrays, ranges, blobs and strings are sequences. There are several basic operations on sequences.

#### Length
Operator `#` gets the size of a finite sequence.
```
#[3, 4, 5] // 3
#(4..9) // 6
#"Title" // 5
#$foo // 3
```
#### Sum
```
[1, 2] + [7, 8, 9] // [1, 2, 7, 8, 9]
'Hello, ' + 'world!' // 'Hello, world!'
$re + $action // $reaction
```
#### Product
```
2*[2, 3, 5] // equivalent to [2, 3, 5, 2, 3, 5]
3*'abc' // equivalent to 'abcabcabc'
```
#### Contains
The `contains` function can be used to test if a sequence contains a subsequence.
```
//success:
contains('carpet', 'car')
contains('carpet', 'pet')
contains([2, 0, 2, 3], [0, 2])
// fail:
contains(1..10, [2, 5])
```
For arrays or ranges, you can use comparison operators for the same purpose.
```
(3..5) <= [10, 3, 4, 5, 0] // success
[2, 0, 2] >= [2, 2] // fail
```
But not for blobs and strings, they are compared in lexicographic order.
#### Reduction
A reduction expression is a way to combine all elements of a sequence by applying a certain binary operation. They have the following form:

*base* **op** ... *sequence*

it works the same way as:

*base* **op** *e<sub>1</sub>* **op** *e<sub>2</sub>* **op** ... **op** *e<sub>n</sub>*

Where *e<sub>1</sub>*, *e<sub>2</sub>*, ..., *e<sub>n</sub>* are the sequence elements.

```
a = [2, 3, 5, 7]
sum = 0 + ...a
product = 1 * ...a
first_100_numbers_sum = 0 + ...(1..100)
```