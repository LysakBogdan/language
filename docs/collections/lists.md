Lists are mutable arrays. They are created by the `list` function from a sequence.

```
a = list([2, 3]) // a = [2, 3]
a.set_first(9) // a = [9, 3]
a.clear // a = []
```

The `<<` operator adds an element to the end of the list.
```
a = list([2, 3])
a << 5 // a = [2, 3, 5]
```