The proc constructor syntax is used to create an anonymous function:

**proc** (*parameters*): *block*

```
[1, 2, 3].each(proc(x): print(x*x)) // prints 1 4 9
```

Any function can reference itself from within with the **self** keyword.
This allows you to create recursive anonymous functions:
```
factorial = proc(n)
	n > 1 ? n*self(n - 1) : 1
```