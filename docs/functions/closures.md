Fabula supports closures, a function can refer to local variables of an external function.
When the external function completes, the internal function can capture the last values of the related variables.
```
proc add(a)
	proc(b)
		a + b

add10 = add(10)
add10(30) // 40
```