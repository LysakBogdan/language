It is also possible to define functions with a variable number of arguments. To do this, simply use the syntax of the last parameter.
```
arguments...
```
then all the last arguments of the function will be "packed" in the array.
```
proc avg(args...)
	(0 + ...args) / #args

avg(7, 7, 10) // 8
```