The `dict` function takes a function, and creates a dictionary with const => value pairs from that function.
```
a = dict({})
a.empty? // yes
b = dict({5 => 6, 7 => 8})
b.size // 2
```

Dictionaries are mutable. Using the `[]` operator you can get or set value for an argument (key)
```
point = dict({x: 0, y: 0, z: 0})
7 -> point[$x] // {x: 7, y: 0, z: 0}
point[$x] // 7
```

A shorthand syntax is available for identifier symbol keys
```
print(point:x)
point:z + 10 -> point:z
```