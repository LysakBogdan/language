The syntax for function definition is:

**proc** *name* (*parameters*): *block*
```
proc cube(a)
	return a*a*a

proc hypotenuse(a, b)
	return (a*a + b*b).sqrt

cube(4) // 64
hypotenuse(3, 5) // 5
```

A function's value is the value of its last expression, in such cases **return** can be skipped.
```
proc cube(a)
	a*a*a
```

If the list of actual arguments passed to the function is greater than declared, the additional arguments are discarded (but calculated).
If this list is less than declared, the **nil** values are passed as missing arguments.