Fabula supports coroutines. A coroutine represents an independent suspendable thread of execution.
The `yield` function is used to suspend its coroutine execution.
```
proc f123()
	yield(1)
	yield(2)
	yield(3)
```
Use the `Coroutine` function to create a coroutine from the main function, and the `resume` method to execute the thread.
`resume` will execute until a `yield` call, and returns the value passed to `yield`.

```
c = Coroutine(f123)
c.resume // 1
c.resume // 2
c.resume // 3
```
Coroutines support a sequence interface.
So, for example, you can traverse the values yielded by the coroutine using the **for** cycle.
```
for e in Coroutine(f123)
	print(e)
```
The **for** loop with pairs traversal also works with coroutines. Keys will be `_` by default. To have meaningful keys, the `yield_pair` function should be used.
@@ yield_pair