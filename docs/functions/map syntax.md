There is a short mapping syntax for anonymous functions:
```
double = [x => 2*x]
double(10) // 20
```