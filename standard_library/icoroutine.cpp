#include "icoroutine.h"

#include "vm.h"

namespace ICoroutine
{

Value resume(Context& c)
{
	auto t = c[0].as<Thread>();
	if(!t->yield)
		return c.fail();

	c[0] = *t->yield;
	t->yield = nullopt;
	t->run();
	return c[0];
}

int decons(Context& c, Value a)
{
	auto t = as_object<Thread>(a);
	if(!t->yield)
		return c.match_fail();

	auto copy = create<Thread>(c, *t);
	auto value = *copy->yield;
	copy->yield = nullopt;
	copy->run();

	return c.results(value, copy.get());
}

int decons_pair(Context& c, Value a)
{
	auto t = as_object<Thread>(a);
	if(!t->yield)
		return c.match_fail();

	auto copy = create<Thread>(c, *t);
	auto key = copy->yield_key;
	auto value = *copy->yield;
	copy->yield = nullopt;
	copy->run();

	return c.results(key, value, copy.get());
}

Value constructor(Context& c)
{
	auto cor = create<Thread>(c, c.vm);
	cor->execute(as_function(c[0]));
	return cor;
}

Library methods =
{
	{"resume", value(resume)},
	{"decons", value(decons)},
	{"decons_pair", value(decons_pair)}
};
}
