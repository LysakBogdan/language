#include "inumber.h"

#include "vm.h"
#include <math.h>

namespace INumber
{

double PI = 3.14159265358979323846;

double neg(double x)
{
	return -x;
}

double deg(double x)
{
	return 180/PI*x;
}

double rad(double x)
{
	return PI/180*x;
}

template<double (*f)(double)>
Value primitive(Context& c)
{
	if(c[0].type == TValue::NIL)
		return c.fail();

	return Value::Double(f(as_number(c[0])));
}

template<double (*f)(double, double)>
Value primitive2(Context& c)
{
	if(c[0].type == TValue::NIL || c[1].type == TValue::NIL)
		return c.fail();

	return Value::Double(f(as_number(c[0]), as_number(c[1])));
}

tuple<double, double> n2(Context& c)
{
	return {as_number(c[0]), as_number(c[1])};
}

tuple<int, int> i2(Context& c)
{
	return {as_int(c[0]), as_int(c[1])};
}

double _plus(double a, double b)
{
	return a + b;
}

double _minus(double a, double b)
{
	return a - b;
}

double _mult(double a, double b)
{
	return a * b;
}

double _div(double a, double b)
{
	return a / b;
}

double _idiv(double a, double b)
{
	return floor(a / b);
}

double next(double a)
{
	return a + 1;
}

double prev(double a)
{
	return a - 1;
}

int get_sincos(Context& c, Value a)
{
	auto x = as_number(a);
	return c.results(sin(x), cos(x));
}

Value do_times(Context& c)
{
	auto n = as_int(c[0]);
	for(int i = 0; i < n; i++)
		c.thread.apply({c[1], Value::Int(i)});
	return Value::Unit;
}

Value do_to(Context& c)
{
	auto [n1, n2] = i2(c);
	for(int i = n1; i <= n2; i++)
		c.thread.apply({c[1], Value::Int(i)});
	return Value::Unit;
}

int decons(Context& c, Value a)
{
	auto n = as_int(a);
	if(n <= 0)
		return c.match_fail();
	return c.results(Value::Unit, n - 1);
}

Value pick(Context& c)
{
	auto n = as_int(c[0]);

	if(n > 0 && c[1] == Value::Unit)
		return Value::Int(n - 1);
	return c.fail();
}

Value at(Context& c)
{
	auto [n, i] = i2(c);
	if(i >= 0 && i < n)
		return Value::Unit;
	return c.fail();
}

Library methods =
{
	{"abs", value(primitive<fabs>)},

	{"min", value(primitive2<fmin>)},
	{"max", value(primitive2<fmax>)},

	{"cos", value(primitive<cos>)},
	{"acos", value(primitive<acos>)},
	{"cosh", value(primitive<cosh>)},

	{"sin", value(primitive<sin>)},
	{"asin", value(primitive<asin>)},
	{"sinh", value(primitive<sinh>)},

	{"tan", value(primitive<tan>)},
	{"atan", value(primitive<atan>)},
	{"tanh", value(primitive<tanh>)},

	{"deg", value(primitive<deg>)},
	{"rad", value(primitive<rad>)},

	{"ceil", value(primitive<ceil>)},
	{"floor", value(primitive<floor>)},

	{"sqrt", value(primitive<sqrt>)},

	{"exp", value(primitive<exp>)},
	{"log", value(primitive<log>)},
	{"log10", value(primitive<log10>)},

	{"negate", value(primitive<neg>)},

	{"plus", value(primitive2<_plus>)},
	{"minus", value(primitive2<_minus>)},
	{"mult", value(primitive2<_mult>)},
	{"div", value(primitive2<_div>)},
	{"idiv", value(primitive2<_idiv>)},
	{"mod", value(primitive2<fmod>)},
	{"pow", value(primitive2<pow>)},

	{"minus_left", value(primitive2<_minus>)},
	{"minus_right", value(primitive2<_minus>)},

	{"next", value(primitive<next>)},
	{"prev", value(primitive<prev>)},

	{"sincos", value(get_sincos)},
	{"atan2", value(primitive2<atan2>)},
	{"do_times", value(do_times)},
	{"do_to", value(do_to)},

	{"decons", value(decons)},
	{"pick", value(pick)},
	{"at", value(at)}
};
}
