#include "isymbol.h"

#include "vm.h"
#include "lexer.h"

namespace ISymbol
{

Value setter(Context& c)
{
	return make_value(::setter(as_string(c[0])));
}

Value blob(Context& c)
{
	return c.createBlob(as_symbol(c[0]).str());
}

int decons(Context& c, Value a)
{
	auto s = as_symbol(a).str();
	if(s.empty())
		return c.match_fail();

	auto first = Value::Int(s[0]);
	auto rest = c.createBlob(string(s.begin() + 1, s.end()));
	return c.results(first, rest);
}

Value at(Context& c)
{
	auto s = as_symbol(c[0]).str();
	auto i = as_int(c[1]);

	if(i < 0 || i >= int(s.size()))
		return c.fail();
	return Value::Int(s[i]);
}

Value concat(Context& c)
{
	auto a = as_string(c[0]);
	auto b = as_string(c[1]);

	return make_value(a + b);
}

Value minus_left(Context& c, const string& a, const string& b)
{
	if(a.starts_with(b))
		return make_value(a.substr(b.size()));
	return c.fail();
}

Value minus_right(Context& c, const string& a, const string& b)
{
	if(a.ends_with(b))
		return make_value(a.substr(0, a.size() - b.size()));
	return c.fail();
}

Value minus_left(Context& c)
{
	auto& a = as_symbol(c[0]).str();
	auto& b = as_symbol(c[1]).str();

	return minus_left(c, a, b);
}

Value minus_right(Context& c)
{
	auto& a = as_symbol(c[0]).str();
	auto& b = as_symbol(c[1]).str();

	return minus_right(c, a, b);
}

Value split(Context& c)
{
	auto s = as_string(c[0]);
	auto sep = c.arg_num() > 1 ? as_string(c[1]) : " ";

	if(sep.empty())
		throw runtime_error("split separator can not be empty");

	auto parts = create<Array>(c);
	size_t pos;
	while((pos = s.find(sep)) != string::npos)
	{
		auto part = s.substr(0, pos);
		parts->values.push_back(make_value(part));
		s = s.substr(pos + sep.size());
	}
	parts->values.push_back(make_value(s));
	return parts;
}

Value ord(Context& c)
{
	auto [s] = c.arguments<string>();
	if(s.size() != 1)
		return Value::Nil;
	return Value::Int(s[0]);
}

Value chr(Context& c)
{
	auto [ch] = c.arguments<int>();
	if(ch < 0 || ch > 0xFF)
		return Value::Nil;
	return value(Symbol(string(1, ch)));
}

Value to_number(Context& c)
{
	auto [s] = c.arguments<string>();
	istringstream is(s);
	double x;
	is >> x;
	return Value::Double(x);
}

Value parse_escaped(Context& c)
{
	auto s = as_string(c[0]);
	return make_value(parse_escaped_chars(s.c_str()));
}

Library methods =
{
	{"setter", value(setter)},
	{"blob", value(blob)},
	{"at", value(at)},
	{"call", value(at)},
	{"decons", value(decons)},
	{"plus", value(concat)},
	{"minus_left", value(minus_left)},
	{"minus_right", value(minus_right)},
	{"split", value(split)},
	{"ord", value(ord)},
	{"chr", value(chr)},
	{"to_number", value(to_number)},
	{"parse_escaped", value(parse_escaped)}
};
}
