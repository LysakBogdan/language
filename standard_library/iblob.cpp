#include "iblob.h"

#include "vm.h"
#include "isymbol.h"

const string& as_string(Value a)
{
	if(!is_string(a))
		throw TypeError{"string", a};

	return a.type == TValue::SYMBOL ? as_symbol(a).str() : as_object<Blob>(a)->str();
}

bool is_string(Value a)
{
	return a.type == TValue::SYMBOL || a.is_object<Blob>();
}

string to_string(Value a)
{
	if(is_string(a))
		return as_string(a);
	return inspect(a);
}

namespace IString
{

template<int i = 0>
Blob& blob(Context& c)
{
	return *as_object<Blob>(c[i]);
}

Value set_at(Context& c)
{
	auto& s = blob(c);
	auto i = as_int(c[2]);

	if(i < 0 || i >= int(s.size()))
		throw RuntimeError{OutOfRange{i, s.size()}};

	return Value::Int(s[i] = as_int(c[1]));
}

Value set_first(Context& c)
{
	auto& s = blob(c);
	auto byte = char(as_int(c[1]));

	if(s.empty())
		s.push_back(byte);
	else
		s[0] = byte;
	return c[1];
}

Value set_last(Context& c)
{
	auto& s = blob(c);
	auto byte = char(as_int(c[1]));

	if(s.empty())
		s.push_back(byte);
	else
		s.back() = byte;
	return c[1];
}

Value set_size(Context& c)
{
	auto& s = blob(c);
	auto size = as_int(c[1]);

	s.resize(size);
	return c[1];
}

Value set_last_num(Context& c)
{
	auto& s = blob(c);
	auto size = as_int(c[1]);

	s.resize(size + 1);
	return c[1];
}

Value push(Context& c)
{
	blob(c).push_back(as_int(c[1]));
	return c[0];
}

Value pop(Context& c)
{
	auto& s = blob(c);
	if(s.empty())
		return c.fail();

	int ch = s.back();
	s.pop_back();
	return Value::Int(ch);
}

Value reverse_self(Context& c)
{
	auto& s = blob(c);
	ranges::reverse(s);
	return c[0];
}

Value clear(Context& c)
{
	blob(c).clear();
	return Value::Unit;
}

Value str(Context& c)
{
	return make_value(blob(c).str());
}

int decons(Context& c, Value a)
{
	auto& s = *as_object<Blob>(a);
	if(s.empty())
		return c.match_fail();

	auto first = Value::Int(s[0]);
	auto rest = c.createBlob(string(s.begin() + 1, s.end()));
	return c.results(first, rest);
}

Value blob_equals(Context& c)
{
	const auto& a = blob<0>(c).str();
	const auto& b = blob<1>(c).str();

	return Value::Bool(a == b);
}

Value minus_left(Context& c)
{
	const auto a = as_string(c[0]);
	const auto b = as_string(c[1]);

	return ISymbol::minus_left(c, a, b);
}

Value minus_right(Context& c)
{
	const auto a = as_string(c[0]);
	const auto b = as_string(c[1]);

	return ISymbol::minus_right(c, a, b);
}

Value extend(Context& c)
{
	auto& a = *as_object<Blob>(c[0]);
	const auto b = as_string(c[1]);

	a.extend(b);

	return Value::Unit;
}

Library methods =
{
	{"set_at", value(set_at)},
	{"decons", value(decons)},
	{"blob_equals", value(blob_equals)},
	{"set_first", value(set_first)},
	{"set_last", value(set_last)},
	{"set_size", value(set_size)},
	{"set_last_num", value(set_last_num)},
	{"push", value(push)},
	{"pop", value(pop)},
	{"clear", value(clear)},
	{"reverse!", value(reverse_self)},
	{"plus", value(ISymbol::concat)},
	{"str", value(str)},
	{"minus_left", value(minus_left)},
	{"minus_right", value(minus_right)},

	{"extend", value(extend)}
};
}

Type& Blob::type() const
{
	static Type interface(IString::methods, RAFS::methods);
	return interface;
}
