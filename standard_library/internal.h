#ifndef INTERNAL_H_INCLUDED
#define INTERNAL_H_INCLUDED

#include "module.h"
#include "type.h"

namespace Atoms
{
	Library Internal();
}

#endif
