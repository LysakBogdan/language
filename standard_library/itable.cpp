#include "itable.h"

#include "vm.h"

namespace ITable
{

template<int i = 0>
map<Value, Value>& table(Context& c)
{
	return as_object<Table>(c[i])->table;
}

Value size(Context& c)
{
	return Value::Int(table(c).size());
}

Value is_empty(Context& c)
{
	return Value::Bool(table(c).empty());
}

Value keys(Context& c)
{
	auto arr = create<Array>(c, table(c).size());
	int i = 0;
	for(auto [key, value] : table(c))
		arr->values[i++] = key;
	return arr;
}

Value values(Context& c)
{
	auto arr = create<Array>(c, table(c).size());
	int i = 0;
	for(auto [key, value] : table(c))
		arr->values[i++] = value;
	return arr;
}

Value key_of(Context& c)
{
	for(auto [key, value] : table(c))
		if(value == c[1])
			return key;
	return c.fail();
}

Value keys_of(Context& c)
{
	const auto& map = table(c);
	auto arr = create<Array>(c);
	for(auto [key, value] : map)
		if(value == c[1])
			arr->values.push_back(key);
	return arr;
}

Value each(Context& c)
{
	for(const auto& [key, value] : table(c))
		c.thread.apply({c[1], value});
	return Value::Unit;
}

Value each_pair(Context& c)
{
	const auto& map = table(c);
	for(const auto& [key, value] : map)
		c.thread.apply({c[1], key, value});
	return Value::Unit;
}

// TODO: optimize
int decons_pair(Context& c, Value a)
{
	const auto& map = as_object<Table>(a)->table;
	if(map.empty())
		return c.match_fail();

	auto i = map.begin();
	auto key = i->first;
	auto value = i->second;
	auto rest = create<Table>(c, ++i, map.end());
	return c.results(key, value, rest.get());
}

// TODO: optimize
int decons(Context& c, Value a)
{
	const auto& map = as_object<Table>(a)->table;
	if(map.empty())
		return c.match_fail();

	auto i = map.begin();
	auto value = i->second;
	auto rest = create<Table>(c, ++i, map.end());
	return c.results(value, rest.get());
}

Value at(Context& c)
{
	const auto& dict = table(c);
	auto i = dict.find(c[1]);
	if(i == dict.end())
		return c.fail();
	return i->second;
}

Library methods =
{
	{"at", value(at)},
	{"field", value(at)},
	{"call", value(at)},
	{"size", value(size)},
	{"empty?", value(is_empty)},
	{"keys", value(keys)},
	{"values", value(values)},
	{"key_of", value(key_of)},
	{"keys_of", value(keys_of)},
	{"each", value(each)},
	{"each_pair", value(each_pair)},
	{"decons_pair", value(decons_pair)},
	{"decons", value(decons)}
};
}

Type& Table::type() const
{
	static Type interface(ITable::methods);
	return interface;
}

namespace IDictionary
{

template<int i = 0>
map<Value, Value>& table(Context& c)
{
	return as_object<MutableTable>(c[i])->table;
}

Value set_at(Context& c)
{
	c[0].as<MutableTable>()->set(c[2], c[1]);
	return c[1];
}

Value clear(Context& c)
{
	table(c).clear();
	return Value::Unit;
}

Library methods =
{
	{"set_at", value(set_at)},
	{"set_field", value(set_at)},
	{"clear", value(clear)}
};
}

Type& MutableTable::type() const
{
	static Type interface(ITable::methods, IDictionary::methods);
	return interface;
}
