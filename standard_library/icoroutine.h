#ifndef ICOROUTINE_H_INCLUDED
#define ICOROUTINE_H_INCLUDED

#include "type.h"

namespace ICoroutine
{
extern Value constructor(Context&);
extern Library methods;
};

#endif
