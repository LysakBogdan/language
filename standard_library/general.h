#ifndef GENERAL_H_INCLUDED
#define GENERAL_H_INCLUDED

#include "object.h"

struct Fallback: Object
{
	Value a, b;
	Fallback(Value aA, Value aB): a(aA), b(aB) {}

	static constexpr auto type_name = "fallback";
	void mark(GC& gc) const override;
	void inspect(Printer& p) const override;
	Type& type() const override;
};

#endif
