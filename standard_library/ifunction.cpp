#include "ifunction.h"

#include "vm.h"
#include "formatting.h"

namespace IFunction
{

Value at(Context& c)
{
	return c.thread.apply({c[0], c[1]});
}

Library methods =
{
	{"at", value(at)}
};
}
