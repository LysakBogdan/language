#include "sequence.h"

#include "range.h"
#include "vm.h"
#include "array.h"
#include "table.h"
#include "atoms.h"

optional<pair<Value, Value>> single_pair(Context& c, Value a);

namespace Atoms
{
Value seq(Context&)
{
	NOT_IMPLEMENTED;
}

int as_seq(Context& c, Value a)
{
	if(c[0] == atom("fail"))
		c.match_fail();
	if(auto pr = single_pair(c, a); pr)
		return c.results(pr->second, atom("fail"));
	return c("decons"_m);
}

int decons_pair(Context& c, Value a)
{
	if(a == atom("fail"))
		return c.match_fail();

	if(a.type == TValue::FUNCTION)
	{
		if(auto pr = single_pair(c, a); pr)
		{
			auto [key, value] = *pr;
			return c.results(key, value, atom("fail"));
		}
		return c.match_fail();
	}

	return c("decons_pair"_m);
}

tuple<Value, Value> decons(Context& c, Value a)
{
	return c.thread.apply2({value(as_seq), a});
}

tuple<GC::Handle, GC::Handle> decons_h(Context& c, Value a)
{
	auto [first, rest] = decons(c, a);
	return {GC::Handle(c.vm.gc, first), GC::Handle(c.vm.gc, rest)};
}

bool empty_sequence(Context& c, Value a)
{
	auto [e, rest] = Atoms::decons(c, a);
	return e == Value::Nil;
}

Value size(Context& c)
{
	auto s = c[0];

	if(s.is_object<Array>())
		return Value::Int(s.as<Array>()->values.size());
	if(s.is_object<Table>())
		return Value::Int(as_object<Table>(s)->table.size());

	int n = 0;
	for(Iterator i(c, s); i; i.next())
		n++;
	return Value::Int(n);
}

Value some_in(Context& c)
{
	for(Iterator e(c, c[0]); e; e.next())
		if(c.thread.apply({c[1], *e}))
			return Value::Bool(true);
	return c.fail();
}

Value every_in(Context& c)
{
	for(Iterator e(c, c[0]); e; e.next())
		if(!c.thread.apply({c[1], *e}))
			return c.fail();
	return Value::Bool(true);
}

Value reduce(Context& c)
{
	auto acc = c[2];
	for(Iterator e(c, c[1]); e; e.next())
		acc = c.thread.apply({c[0], acc, *e});
	return acc;
}

Library Sequence()
{
	return
	{
		{"seq", value(seq)},
		{"as_seq", value(as_seq)},
		{"decons_pair", value(decons_pair)},
		{"size", value(size)},
		{"set_size", "set_size"_m},
		{"some_in", value(some_in)},
		{"every_in", value(every_in)},
		{"reduce", value(reduce)},
	};
}
}

namespace IInfinity
{
int decons(Context& c, Value a)
{
	as_object<Infinity>(a);
	return c.results(Value::Unit, a);
}

Library methods =
{
	{"decons", value(decons)}
};
}

void Infinity::inspect(Printer& p) const
{
	p << "Infinity";
}

Type& Infinity::type() const
{
	static Type interface(IInfinity::methods);
	return interface;
}

Iterator::Iterator(Context& context, Value sequence):
	c(context), current(context.vm.gc, Value::Nil), remainder(context.vm.gc, sequence)
{
	next();
}

void Iterator::next()
{
	tie(current, remainder) = Atoms::decons(c, remainder);
}
