#ifndef ISYMBOL_H_INCLUDED
#define ISYMBOL_H_INCLUDED

#include "type.h"

namespace ISymbol
{
extern Library methods;

Value concat(Context& c);
Value minus_left(Context& c, const string& a, const string& b);
Value minus_right(Context& c, const string& a, const string& b);
};

#endif
