#include "itype.h"

#include "vm.h"
#include "lang_object.h"
#include "atom.h"

namespace IType
{
Value at(Context& c)
{
	return as_object<Type>(c[0])->getMethod(as_symbol(c[1]));
}

Value set_at(Context& c)
{
	as_object<Type>(c[0])->setMethod(as_symbol(c[2]), c[1]);
	return Value::Unit;
}

Value new_(Context& c)
{
	auto type = as_object<Type>(c[0]);
	if(dynamic_cast<LangType*>(type))
	{
		auto created = make_value(create<LangObject>(c, type));
		c.thread.apply({type->constructor, created});
		return created;
	}

	c[0] = type->constructor;
	init_apply(c);

	return c[0];
}

Value get_constructor(Context& c)
{
	return as_object<Type>(c[0])->constructor;
}

Value set_constructor(Context& c)
{
	as_object<Type>(c[0])->constructor = c[1];
	return Value::Unit;
}

Value constructor(Context& c)
{
	return create<LangType>(c);
}

Library methods =
{
	{"at", value(at)},
	{"set_at", value(set_at)},
	{"field", value(at)},
	{"set_field", value(set_at)},
	{"constructor", value(get_constructor)},
	{"set_constructor", value(set_constructor)},
	{"new", value(new_)},
	{"call", value(new_)}
};
}
