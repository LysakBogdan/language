#include "os.h"

#include "blob.h"
#include "vm.h"
#include <sstream>
#include <chrono>

Value save_to_file(Context& c)
{
	const auto& blob = as_string(c[0]);
	const auto& path = as_string(c[1]);

	ofstream file(path, ios::out | ios::binary);
	file.write(blob.data(), blob.size());

	return Value::Unit;
}

Value read_from_file(Context& c)
{
	const auto& path = as_string(c[0]);

	ifstream file(path);
	if(!file.is_open())
		return c.fail();

	stringstream s;
	s << file.rdbuf();
	return c.createBlob(s.str());
}

Value os_system(Context& c)
{
	auto [cmd] = c.arguments<string>();
	return Value::Int(system(cmd.c_str()));
}

Value os_time(Context& c)
{
	using namespace std::chrono;

	auto now = system_clock::now().time_since_epoch();
	auto seconds = duration<double, seconds::period>(now);
	return Value::Double(seconds.count());
}

namespace IFileSystem
{
Value at(Context& c)
{
	c.pop_front();
	return c.delegate(value(read_from_file));
}

Value set_at(Context& c)
{
	c.pop_front();
	return c.delegate(value(save_to_file));
}

Library methods =
{
	{"at", value(at)},
	{"set_at", value(set_at)}
};
};

Type& Files::type() const
{
	static Type interface(IFileSystem::methods);
	return interface;
}

namespace Atoms
{
Library OS(VM& vm)
{
	return
	{
		{"read_from_file", value(read_from_file)},
		{"save_to_file", value(save_to_file)},
		{"file", vm.create<Files>()},
		{"system", value(os_system)},
		{"time", value(os_time)}
	};
}
}
