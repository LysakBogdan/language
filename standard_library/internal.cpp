#include "internal.h"

#include "lexer.h"

void write_span(ostream& os, const string_view& c, const string_view& s)
{
	os << "<span class='" << c << "'>" << s << "</span>";
}

const char* span_class(Lexeme l)
{
	using enum TLexeme;
	if(l.type == KEY)
		return isalnum(l.view[0]) || l.view[0] == '_' ? "k" : "o";
	if(l.type == NUMBER)
		return "n";
	if(l.type == STRING || l.type == SYMBOL)
		return "s";
	return "c";
}

Value highlight(Context& c)
{
	auto [code] = c.arguments<string>();

	stringstream ss;
	auto tokens = tokenize(code.c_str());

	const char* i = code.c_str();
	for(auto token : tokens)
	{
		ss << string(i, token.view.data());
		write_span(ss, span_class(token), token.view);
		i = token.view.data() + token.view.size();
	}

	return make_value(ss.str());
}

Library Atoms::Internal()
{
	return
	{
		{"__highlight", value(highlight)}
	};
}
