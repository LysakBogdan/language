#ifndef SEQUENCE_H_INCLUDED
#define SEQUENCE_H_INCLUDED

#include "module.h"
#include "object.h"

namespace Atoms
{
	std::tuple<Value, Value> decons(Context& c, Value a);
	std::tuple<GC::Handle, GC::Handle> decons_h(Context& c, Value a);
	bool empty_sequence(Context& c, Value a);
	Library Sequence();
}

struct Infinity: Object
{
	static constexpr auto type_name = "infinity";
	void inspect(Printer&) const override;
	Type& type() const override;
};

class Iterator
{
	Context& c;
	GC::Handle current;
	GC::Handle remainder;

public:
	Iterator(Context&, Value);

	void next();

	operator bool() const
	{
		return *current != Value::Nil;
	}

	Value operator*() const
	{
		return current;
	}

	Value rest() const
	{
		return remainder;
	}
};

#endif
