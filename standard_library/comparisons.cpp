#include "comparisons.h"

#include "array.h"
#include "sequence.h"
#include "blob.h"
#include "bag.h"

namespace Atoms
{
bool begin_with(Context& c, Value A, Value B)
{
	for(Iterator a(c, A), b(c, B); b; a.next(), b.next())
		if(*a != *b)
			return false;
	return true;
}

Value pick_all(Context& c, Value a, Value b)
{
	auto rest_a = c.keep(a);
	for(Iterator e(c, b); e; e.next())
	{
		rest_a = as_object<Pickable>(rest_a)->pick(c, *e);
		if(*rest_a == Value::Nil)
			return Value::Nil;
	}
	return rest_a;
}

bool bag_contains(Context& c, Value a, Value b)
{
	return pick_all(c, a, b) != Value::Nil;
}

bool bag_greater(Context& c, Value a, Value b)
{
	const auto rest = pick_all(c, a, b);
	return rest != Value::Nil && !empty_sequence(c, rest);
}

bool contains(Context& c, Value a, Value b)
{
	if(a.type == TValue::NUMBER)
		return !(a < b);

	if(a.is_object<Pickable>())
		return bag_contains(c, a, b);

	if(begin_with(c, a, b))
		return true;

	auto [first, rest] = decons_h(c, a);
	if(*first == Value::Nil)
		return false;

	return contains(c, rest, b);
}

bool less(Context& c, Value a, Value b)
{
	// TODO: complete
	if(a.is_object<Array>() && b.is_object<Array>())
		return as_array(a).size() < as_array(b).size() && contains(c, b, a);

	if(a.is_object<Pickable>())
		return bag_greater(c, b, a);

	if(is_string(a))
		return as_string(a) < as_string(b);

	return a < b;
}

Value equal(Context& c)
{
	return Value::Bool(c[0] == c[1]);
}

Value not_equal(Context& c)
{
	return Value::Bool(!(c[0] == c[1]));
}

Value less(Context& c)
{
	auto a = c[0];
	auto b = c[1];

	return less(c, a, b) ? a : Value::Nil;
}

Value greater(Context& c)
{
	auto a = c[0];
	auto b = c[1];

	return less(c, b, a) ? a : Value::Nil;
}

Value less_equal(Context& c)
{
	auto a = c[0];
	auto b = c[1];

	return contains(c, b, a) ? a : Value::Nil;
}

Value greater_equal(Context& c)
{
	auto a = c[0];
	auto b = c[1];

	return contains(c, a, b) ? a : Value::Nil;
}

Value contains(Context& c)
{
	return Value::Bool(contains(c, c[0], c[1]));
}

Library Comparisons()
{
	return
	{
		{"equal", value(equal)},
		{"not_equal", value(not_equal)},
		{"less", value(less)},
		{"greater", value(greater)},
		{"less_equal", value(less_equal)},
		{"greater_equal", value(greater_equal)},

		{"contains", value(contains)}
	};
}
}
