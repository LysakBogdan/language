#include "general.h"

#include "type.h"
#include "thread.h"
#include "atoms.h"

namespace IFallback
{
Value call(Context& c)
{
	auto fb = as_object<Fallback>(c[0]);
	auto arg = c[1];

	auto res = c.thread.apply({fb->a, arg});
	if(res != Value::Nil)
		return res;
	return c.thread.apply({fb->b, arg});
}

int decons_pair(Context& c, Value a)
{
	auto fb = as_object<Fallback>(a);
	if(auto pr = single_pair(c, fb->a); pr)
	{
		auto [key, value] = *pr;
		return c.results(key, value, fb->b);
	}
	return c.match_fail();
}

int decons(Context& c, Value a)
{
	auto fb = as_object<Fallback>(a);
	if(auto pr = single_pair(c, fb->a); pr)
		return c.results(pr->second, fb->b);
	return c.match_fail();
}

Library methods =
{
	{"call", value(call)},
	{"at", value(call)},
	{"decons_pair", value(decons_pair)},
	{"decons", value(decons)}
};
};

Type& Fallback::type() const
{
	static Type interface(IFallback::methods);
	return interface;
}

void Fallback::mark(GC& gc) const
{
	gc.mark(a);
	gc.mark(b);
}

void Fallback::inspect(Printer& p) const
{
	p << "FALLBACK";
}
