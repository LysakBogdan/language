#ifndef OS_H_INCLUDED
#define OS_H_INCLUDED

#include "module.h"
#include "type.h"

struct Files: public Object
{
	void inspect(Printer& p) const override
	{
		p << "FileSystem";
	}
	Type& type() const override;
};

namespace Atoms
{
	Library OS(VM&);
}

#endif
