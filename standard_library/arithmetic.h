#ifndef ARITHMETIC_H_INCLUDED
#define ARITHMETIC_H_INCLUDED

#include "module.h"
#include "type.h"
#include "bag.h"

namespace Atoms
{
	Library Arithmetic();
}

Value op_add(Thread& th, Value, Value);
Value op_sub(Thread& th, Value, Value);
Value op_mul(Thread& th, Value, Value);
Value op_div(Thread& th, Value a, Value b);
Value op_idiv(Thread& th, Value a, Value b);
Value op_mod(Thread& th, Value a, Value b);

void op_neg(Thread& th);

struct Sum: Object
{
	Value a, b;
	Sum(Value aA, Value aB): a(aA), b(aB) {}

	static constexpr auto type_name = "sum";
	void mark(GC &gc) const override;
	void inspect(Printer&) const override;
	Type& type() const override;

	static Type Interface;
};

struct Product: Object
{
	Value n, a, rest;
	Product(Value aN, Value aA, Value aRest = Value::Int(0)): n(aN), a(aA), rest(aRest) {}

	static constexpr auto type_name = "product";
	void mark(GC &gc) const override;
	void inspect(Printer&) const override;
	Type& type() const override;

	static Type Interface;
};

struct Intersection: Object, Pickable
{
	Value a, b;
	Intersection(Value aA, Value aB): a(aA), b(aB) {}

	void mark(GC &gc) const override;
	void inspect(Printer&) const override;
	Type& type() const override;

	Value pick(Context& c, Value a) const override;

	static Type Interface;
};

struct Union: Object, Pickable
{
	Value a, b;
	Union(Value aA, Value aB): a(aA), b(aB) {}

	void mark(GC &gc) const override;
	void inspect(Printer&) const override;
	Type& type() const override;

	Value pick(Context& c, Value a) const override;

	static Type Interface;
};

struct BagDifference: Object, Pickable
{
	Value a, b;
	BagDifference(Value aA, Value aB): a(aA), b(aB) {}

	void mark(GC &gc) const override;
	void inspect(Printer&) const override;
	Type& type() const override;

	Value pick(Context& c, Value a) const override;

	static Type Interface;
};

struct SymmetricDifference: Object, Pickable
{
	Value a, b;
	SymmetricDifference(Value aA, Value aB): a(aA), b(aB) {}

	void mark(GC &gc) const override;
	void inspect(Printer&) const override;
	Type& type() const override;

	Value pick(Context& c, Value a) const override;

	static Type Interface;
};

#endif
