#ifndef ITYPE_H_INCLUDED
#define ITYPE_H_INCLUDED

#include "type.h"

namespace IType
{
extern Value constructor(Context& c);
extern Library methods;
};

#endif
