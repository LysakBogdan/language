#include "iarray.h"

#include "vm.h"
#include "range.h"
#include "atoms.h"

namespace IArray
{
vector<Value>& as_list(Value a)
{
	return as_object<MutableArray>(a)->values;
}

Value set_at(Context& c)
{
	auto& a = as_list(c[0]);
	auto e = c[1];
	auto index = as_int(c[2]);

	if(index < 0 || index >= int(a.size()))
		return c.fail();

	return a[index] = e;
}

Value set_first(Context& c)
{
	auto& a = as_list(c[0]);
	if(a.empty())
		a.push_back(c[1]);
	else
		a[0] = c[1];
	return Value::Unit;
}

Value set_last(Context& c)
{
	auto& a = as_list(c[0]);
	if(a.empty())
		a.push_back(c[1]);
	else
		a.back() = c[1];
	return Value::Unit;
}

Value set_size(Context& c)
{
	auto& a = as_list(c[0]);
	a.resize(as_int(c[1]));
	return Value::Unit;
}

Value push(Context& c)
{
	as_list(c[0]).push_back(c[1]);
	return c[0];
}

Value extend(Context& c)
{
	auto& a = as_list(c[0]);
	auto fs = RAFS::RAFS(c[1]);

	a.reserve(a.size() + fs.size());
	for(int i = 0; i < fs.size(); i++)
		a.push_back(fs[i]);

	return Value::Unit;
}

Value insert(Context& c)
{
	auto& a = as_list(c[0]);
	auto index = as_int(c[1]);
	auto e = c[2];

	if(index < 0 || index > int(a.size()))
		throw runtime_error("insert position must be in range 0..#list");

	a.insert(a.begin() + index, e);
	return c[0];
}

Value remove(Context& c)
{
	auto& a = as_list(c[0]);
	auto index = as_int(c[1]);

	if(index < 0 || index >= int(a.size()))
		throw runtime_error("remove position must be in range 0 ..< #list");
	a.erase(a.begin() + index);
	return c[0];
}

Value pop(Context& c)
{
	auto& a = as_list(c[0]);
	if(a.empty())
		return c.fail();

	auto item = a.back();
	a.pop_back();
	return item;
}

Value reverse_self(Context& c)
{
	auto& a = as_list(c[0]);
	ranges::reverse(a);
	return c[0];
}

Value clear(Context& c)
{
	as_list(c[0]).clear();
	return Value::Unit;
}

Value sort(Context& c)
{
	auto& a = as_list(c[0]);
	auto pred = c.arg_num() > 1 ? c[1] : atom("less");

	ranges::sort(a, [&c, pred](Value a, Value b) -> bool
	{
		return bool(c.thread.apply({pred, a, b}));
	});
	return c[0];
}

Value plus(Context& c)
{
	auto& a = as_array(c[0]);
	auto& b = as_array(c[1]);
	auto sum = create<Array>(c);

	sum->values.reserve(a.size() + b.size());
	sum->values.insert(sum->values.end(), a.begin(), a.end());
	sum->values.insert(sum->values.end(), b.begin(), b.end());

	return sum;
}

Value minus_left(Context& c)
{
	auto& a = as_array(c[0]);
	auto& b = as_array(c[1]);

	if(a.size() >= b.size() && equal(a.begin(), a.begin() + b.size(), b.begin()))
		return create<Array>(c, a.size() - b.size(), a.data() + b.size());
	return c.fail();
}

Value minus_right(Context& c)
{
	auto& a = as_array(c[0]);
	auto& b = as_array(c[1]);

	if(a.size() >= b.size() && equal(a.end() - b.size(), a.end(), b.begin()))
		return create<Array>(c, a.size() - b.size(), a.data());
	return c.fail();
}

Library methods =
{
	{"plus", value(plus)},
	{"minus_left", value(minus_left)},
	{"minus_right", value(minus_right)}
};

Library mutable_methods =
{
	{"set_at", value(set_at)},
	{"set_first", value(set_first)},
	{"set_last", value(set_last)},
	{"set_size", value(set_size)},
	{"push", value(push)},
	{"extend", value(extend)},
	{"insert", value(insert)},
	{"remove", value(remove)},
	{"pop", value(pop)},
	{"clear", value(clear)},
	{"reverse!", value(reverse_self)},
	{"sort", value(sort)}
};
}

Type& Array::get_type()
{
	static Type interface(IArray::methods, RAFS::methods);
	return interface;
}

Type& Array::type() const
{
	return get_type();
}

Type& MutableArray::type() const
{
	static Type interface(IArray::methods, RAFS::methods, IArray::mutable_methods);
	return interface;
}
