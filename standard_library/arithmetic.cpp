#include "arithmetic.h"

#include "vm.h"
#include "sequence.h"

using enum TValue;

Value try_method(Context& c, Symbol name);

Value op_add(Thread& th, Value a, Value b)
{
	if(a.type == NIL || b.type == NIL)
		return Value::Nil;

	if(a.type == NUMBER && b.type == NUMBER)
		return Value::Double(a.number + b.number);

	if(a.is_object<Array>() && b.is_object<Array>())
		return th.apply({"plus"_m, a, b});

	if(is_string(a))
		return th.apply({"plus"_m, a, b});

	return th.machine().create<Sum>(a, b).value();
}

Value op_sub(Thread& th, Value a, Value b)
{
	if(a.type == NUMBER)
		return Value::Double(as_number(a) - as_number(b));
	return th.apply({"minus"_m, a, b});
}

Value op_mul(Thread& th, Value a, Value b)
{
	if(a.type == NIL || b.type == NIL)
		return Value::Nil;

	if(a.type == NUMBER && b.type == NUMBER)
		return Value::Double(as_number(a) * as_number(b));

	return th.machine().create<Product>(a, b).value();
}

Value op_div(Thread& th, Value a, Value b)
{
    if(a.type == NUMBER)
	{
		auto divisor = as_number(b);
		if(divisor == 0.0)
			return Value::Nil;
		return Value::Double(as_number(a) / divisor);
	}
	return th.apply({"div"_m, a, b});
}

Value op_idiv(Thread& th, Value a, Value b)
{
	switch(a.type)
	{
		case FUNCTION:
		case OBJECT:
			return th.apply({"idiv"_m, a, b});
		default:
			return Value::Int(as_number(a) / as_number(b));
	}
}

Value op_mod(Thread& th, Value a, Value b)
{
	switch(a.type)
	{
		case FUNCTION:
		case OBJECT:
			return th.apply({"mod"_m, a, b});
		default:
		{
			auto x = as_number(a);
			auto y = as_number(b);
			return Value::Double(x - y*int(x/y));
		}
	}
}

void op_neg(Thread& th)
{
	auto& a = th.top_value();
	switch(a.type)
	{
		case NIL:
			return;
		case NUMBER:
			a = Value::Double(-as_number(a));
			return;
		default:
			a = th.apply({"negate"_m, a});
	}
}

namespace Atoms
{
Value seq_sum(Context& c)
{
	if(c[0].type == NIL || c[1].type == NIL)
		return c.fail();

	return create<Sum>(c, c[0], c[1]);
}

Value minus_left(Context& c)
{
	return try_method(c, Symbol("minus_left"));
}

Value minus_right(Context& c)
{
	return try_method(c, Symbol("minus_right"));
}

template<auto (*f)(Thread&, Value, Value)>
CFunction to_atom()
{
	return [](Context& c)
	{
		return make_value(f(c.thread, c[0], c[1]));
	};
}

Library Arithmetic()
{
	return
	{
		{"negate", "negate"_m},
		{"plus", value(to_atom<op_add>())},
		{"minus", value(to_atom<op_sub>())},
		{"mult", value(to_atom<op_mul>())},
		{"div", value(to_atom<op_div>())},
		{"idiv", value(to_atom<op_idiv>())},
		{"mod", value(to_atom<op_mod>())},
		{"pow", "pow"_m},
		{"minus_left", value(minus_left)},
		{"minus_right", value(minus_right)},
		{"seq_sum", value(seq_sum)},
		{"prev", "prev"_m},
		{"next", "next"_m}
	};
}
}

namespace ISum
{
int decons(Context& c, Value a)
{
	const auto& sum = *as_object<Sum>(a);

	auto [first_a, rest_a] = Atoms::decons_h(c, sum.a);

	if(*rest_a != Value::Nil)
		return c.results(*first_a, create<Sum>(c, *rest_a, sum.b));

	auto [first, rest] = Atoms::decons(c, sum.b);
	return c.results(first, rest);
}

Library methods =
{
	{"decons", value(decons)}
};
};

void Sum::mark(GC& gc) const
{
	gc.mark(a);
	gc.mark(b);
}

void Sum::inspect(Printer& p) const
{
	p << format("Sum({}, {})", a, b);
}

Type& Sum::type() const
{
	static Type interface(ISum::methods);
	return interface;
}

namespace IProduct
{
int decons(Context& c, Value a)
{
	auto product = *as_object<Product>(a);
	auto [first, rest] = Atoms::decons_h(c, product.rest);
	if(*first == Value::Nil)
	{
		auto [n_first, n_rest] = Atoms::decons(c, product.n);
		if(n_rest == Value::Nil)
			return c.match_fail();
		product = Product(n_rest, product.a, product.a);
		tie(first, rest) = Atoms::decons(c, product.rest);
	}

	if(*first != Value::Nil)
	{
		auto rest_product = create<Product>(c, product.n, product.a, rest);
		return c.results(*first, rest_product.value());
	}

	return c.match_fail();
}

Library methods =
{
	{"decons", value(decons)}
};
};

void Product::mark(GC& gc) const
{
	gc.mark(n);
	gc.mark(a);
	gc.mark(rest);
}

void Product::inspect(Printer& p) const
{
	p << format("Product({}, {})", n , a);
}

Type& Product::type() const
{
	static Type interface(IProduct::methods);
	return interface;
}

namespace IIntersection
{
int decons(Context& c, Value a)
{
	const auto& o = *as_object<Intersection>(a);
	for(Iterator e(c, o.a); e; e.next())
	{
		auto rest_b = c.keep(as_object<Pickable>(o.b)->pick(c, *e));
		if(*rest_b != Value::Nil)
			return c.results(*e, create<Intersection>(c, e.rest(), rest_b));
	}
	return c.match_fail();
}

Library methods =
{
	{"decons", value(decons)},
	{"equals", value(bag_equals)}
};
};

Value Intersection::pick(Context& c, Value the_value) const
{
	auto a_rest = GC::Handle(c.vm.gc, as_object<Pickable>(a)->pick(c, the_value));
	auto b_rest = GC::Handle(c.vm.gc, as_object<Pickable>(b)->pick(c, the_value));

	if(a_rest->type == NIL || b_rest->type == NIL)
		return Value::Nil;

	return create<Intersection>(c, a_rest, b_rest).value();
}

void Intersection::mark(GC& gc) const
{
	gc.mark(a);
	gc.mark(b);
}

void Intersection::inspect(Printer& p) const
{
	p << format("Intersection({}, {})", a, b);
}

Type& Intersection::type() const
{
	static Type interface(IIntersection::methods);
	return interface;
}

namespace IUnion
{
int decons(Context& c, Value a)
{
	const auto& o = *as_object<Union>(a);

	auto [first, rest] = Atoms::decons_h(c, o.a);
	if(*first != Value::Nil)
	{
		auto rest_b = c.keep(as_object<Pickable>(o.b)->pick(c, first));
		if(*rest_b != Value::Nil)
			return c.results(*first, create<Union>(c, rest, rest_b));
		return c.results(*first, create<Union>(c, rest, o.b));
	}

	c[0] = o.b;
	return c("decons"_m);
}

Library methods =
{
	{"decons", value(decons)},
	{"equals", value(bag_equals)}
};
};

Value Union::pick(Context& c, Value the_value) const
{
	auto a_rest = GC::Handle(c.vm.gc, as_object<Pickable>(a)->pick(c, the_value));
	auto b_rest = GC::Handle(c.vm.gc, as_object<Pickable>(b)->pick(c, the_value));

	if(a_rest->type == NIL && b_rest->type == NIL)
		return Value::Nil;

	if(a_rest->type == NIL)
		return create<Union>(c, a, b_rest).value();
	if(b_rest->type == NIL)
		return create<Union>(c, a_rest, b).value();

	return create<Union>(c, a_rest, b_rest).value();
}

void Union::mark(GC& gc) const
{
	gc.mark(a);
	gc.mark(b);
}

void Union::inspect(Printer& p) const
{
	p << format("Union({}, {})", a, b);
}

Type& Union::type() const
{
	static Type interface(IUnion::methods);
	return interface;
}

namespace IBagDifference
{
int decons(Context& c, Value bag)
{
	const auto& o = *as_object<BagDifference>(bag);

	auto b = c.keep(o.b);
	for(Iterator a(c, o.a); a; a.next())
	{
		auto rest_b = c.keep(as_object<Pickable>(b)->pick(c, *a));
		if(*rest_b == Value::Nil)
			return c.results(*a, create<BagDifference>(c, a.rest(), b));
		b = *rest_b;
	}
	return c.match_fail();
}

Library methods =
{
	{"decons", value(decons)},
	{"equals", value(bag_equals)}
};
};

Value BagDifference::pick(Context& c, Value the_value) const
{
	auto a_rest = GC::Handle(c.vm.gc, as_object<Pickable>(a)->pick(c, the_value));
	auto b_rest = GC::Handle(c.vm.gc, as_object<Pickable>(b)->pick(c, the_value));

	if(a_rest->type == NIL && b_rest->type == NIL)
		return Value::Nil;

	if(a_rest->type == NIL)
		return create<BagDifference>(c, a, b_rest).value();
	if(b_rest->type == NIL)
		return create<BagDifference>(c, a_rest, b).value();

	return create<BagDifference>(c, a_rest, b_rest).value();
}

void BagDifference::mark(GC& gc) const
{
	gc.mark(a);
	gc.mark(b);
}

void BagDifference::inspect(Printer& p) const
{
	p << format("BagDifference({}, {})", a, b);
}

Type& BagDifference::type() const
{
	static Type interface(IBagDifference::methods);
	return interface;
}

namespace ISymmetricDifference
{
int decons(Context& c, Value bag)
{
	const auto& o = *as_object<SymmetricDifference>(bag);

	auto b = c.keep(o.b);
	for(Iterator a(c, o.a); a; a.next())
	{
		auto rest_b = c.keep(as_object<Pickable>(b)->pick(c, *a));
		if(*rest_b == Value::Nil)
			return c.results(*a, create<SymmetricDifference>(c, a.rest(), b));
		b = *rest_b;
	}
	c[0] = b;
	return c("decons"_m);
}

Library methods =
{
	{"decons", value(decons)},
	{"equals", value(bag_equals)}
};
};

Value SymmetricDifference::pick(Context& c, Value the_value) const
{
	auto a_rest = GC::Handle(c.vm.gc, as_object<Pickable>(a)->pick(c, the_value));
	auto b_rest = GC::Handle(c.vm.gc, as_object<Pickable>(b)->pick(c, the_value));

	if(a_rest->type == NIL && b_rest->type != NIL)
		return create<SymmetricDifference>(c, a, b_rest).value();
	if(a_rest->type != NIL && b_rest->type == NIL)
		return create<SymmetricDifference>(c, a_rest, b).value();
	return Value::Nil;
}

void SymmetricDifference::mark(GC& gc) const
{
	gc.mark(a);
	gc.mark(b);
}

void SymmetricDifference::inspect(Printer& p) const
{
	p << format("SymmetricDifference({}, {})", a, b);
}

Type& SymmetricDifference::type() const
{
	static Type interface(ISymmetricDifference::methods);
	return interface;
}
