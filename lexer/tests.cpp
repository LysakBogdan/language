#include <iostream>
#include "lexer.h"

using namespace std;
using enum TLexeme;

bool check_list(const char* source, const vector<Lexeme>& ls)
{
	try
	{
		const auto ts = tokenize(source);
		if(ts.size() != ls.size() + 1)
			return false;
		if(ts.back() != EOT)
			return false;
		return equal(ls.begin(), ls.end(), ts.begin());
	}
	catch(const runtime_error& e)
	{
		cout << e.what() << endl;
		return false;
	}
	catch(const LexerError& e)
	{
		cout << inspect(source, e) << endl;
		return false;
	}
}

template<class ... Ts>
bool check(const char* source, Ts... ts)
{
	return check_list(source, {Lexeme(ts)...});
}

int passed = 0;
int n = 0;

void test(const string& name, bool result)
{
	using namespace std;

	++n;
	if(result)
	{
		++passed;
		cout << " _";
	}
	else
		cout << name << " " << "FAIL" << endl;
}

template<class Err>
bool error(const char* s, const Err& err, Location loc)
{
	try
	{
		tokenize(s);
		return false;
	}
	catch(const LexerError& e)
	{
		return holds_alternative<Err>(e.reason) && get<Err>(e.reason) == err && get_location(s, e.view) == loc;
	}
}

struct Section
{
	Section(const char* name)
	{
		cout << name;
	}
	~Section()
	{
		cout << endl;
	};
};

void number_literals()
{
	Section name = "numbers";
	auto valid_numbers = initializer_list<pair<const char*, const char*>>
	{
		{"zero", "0"}, {"42", "42"}, {"long #", "12345987601029234632"}, {"leading zeros", "00035"}, {"decimal with _", "1_000_000"}, {"digits by _", "1_2_3_4"},
		{"0 in hex", "0x0"}, {"hex", "0xFF"}, {"long hex", "0xffffffff"}, {"upper case hex with _", "0xFFFF_FFFF"},
		{"octal", "0o54"}, {"octal with _", "0o720_234_275"},
		{"binary", "0b101"}, {"binary with _", "0b1010_0111_0100"},
		{"float 0", "0.0"}, {"float", "5677.249003"}, {"only fraction", ".12"}
	};

	for(auto [name, token] : valid_numbers)
		test(name, check(token, Lexeme(NUMBER, token)));

	test("trailing _ are not allowed", error("345_", InvalidNumberLiteral{}, {1, 4}));
	test("many _ are not allowed", error("1__000", InvalidNumberLiteral{}, {1, 2}));
	test("many _ in hex", error("0x10__00", InvalidNumberLiteral{}, {1, 5}));
	test("many _ in octal", error("0o1__23", InvalidNumberLiteral{}, {1, 4}));
	test("many _ in binary", error("0b1__001", InvalidNumberLiteral{}, {1, 4}));

	test("not binary digit", error("0b13", NotBinaryDigit{'3'}, {1, 4}));
	test("not octal digit", error("0o398", NotOctalDigit{'9'}, {1, 4}));
}

void simple()
{
	Section name = "simple";
	test("empty", check(""));
	test("comma", check(",", COMA));
	test("semicolon", check(";", SEP));
}

void operators()
{
	using enum KeyToken;

	Section name = "operators";
	for(int i = int(PLUS); i != int(KEYS_COUNT); i++)
	{
		auto key = KeyToken(i);
		auto name = inspect(key);
		test(name, check(name.c_str(), key));
	}

	test("unexpected char after dot", error(".+", CharIsUnexpected{'+'}, {1, 2}));
	test("! doesn't stick with others", check("!*", NOT, MUL));
	test("# doesn't stick with others", check("#+", LENGTH, PLUS));
	test("^ doesn't stick with others", check("-^", MINUS, PIN));
}

void block_strings()
{
	Section name = "block strings";
	test("basic", check("$$$\n\tHello!", Lexeme(SYMBOL, "Hello!")));
	test("indented", check("\t$$$\n\t\t1:#Q", Lexeme(SYMBOL, "1:#Q")));
	test("no line end", error("$$$", LineEndIsExpected{}, {1, 4}));
	test("empty", check("$$$\nfoo", Lexeme(SYMBOL, ""), NEW_LINE, Lexeme(ID, "foo")));
	test("with some code after", check("$$$\n\tfoo\nbar", Lexeme(SYMBOL, "foo"), NEW_LINE, Lexeme(ID, "bar")));
	test("empty in block", check("do\n\t$$$\nfoo", KeyToken::DO, BEGIN, Lexeme(SYMBOL, ""), END, NEW_LINE, Lexeme(ID, "foo")));
	test("multiple lines", check("$$$\n\t1 foo!\n\t2 @bar", Lexeme(SYMBOL, "1 foo!\n2 @bar")));
	test("multiple lines, spaces", check("$$$\n\t***\n\t  #", Lexeme(SYMBOL, "***\n  #")));
	test("doesn't affect other indentations",
		check("do\n\t$$$\n\t\tfoo\n\tbar", KeyToken::DO, BEGIN, Lexeme(SYMBOL, "foo"), NEW_LINE, Lexeme(ID, "bar"), END));
	test("escape characters", check("$$$\n\t\\n\\$\\t", Lexeme(SYMBOL, "\n$\t")));
	test("escape leading whitespace", check("$$$\n\t\\  .\n\tabc", Lexeme(SYMBOL, "  .\nabc")));
}

void symbols()
{
	Section name = "symbols";
	test("symbol", check("$foo", Lexeme(SYMBOL, "foo")));
	test("symbol with ?", check("$open?", Lexeme(SYMBOL, "open?")));
	test("symbol in ''", check("'foo'", Lexeme(SYMBOL, "foo")));
	test("symbol in '' with space", check("'two words'", Lexeme(SYMBOL, "two words")));
	test("symbol with random chars", check("'!$Q&>a:...'", Lexeme(SYMBOL, "!$Q&>a:...")));
}

void identifiers()
{
	Section name = "identifiers";
	const char* valid_ids[][2] =
	{
		{"id 1 char", "a"}, {"id", "thing"}, {"id with #", "var123"}, {"id with _", "foo_bar"},
		{"id starts with _", "_count"}, {"only _ and #", "_12_345"},
		{"long id", "The_quick_brown_fox_jumps_over_the_lazy_dog"}
	};

	for(auto [name, id] : valid_ids)
		test(name, check(id, Lexeme(ID, id)));
}

void new_line()
{
	Section name = "new_line";
	test("only \\n", check("\n"));
	test("id + \\n", check("a\n", Lexeme(ID, "a")));
	test("\\n as separator", check("a\nb", Lexeme(ID, "a"), NEW_LINE, Lexeme(ID, "b")));
	test("backslash escapes \\n", check("a\\\nb", Lexeme(ID, "a"), Lexeme(ID, "b")));
	test("first new-line and comments are skipped", check("// comment\n\n// ...\nfoo", Lexeme(ID, "foo")));
	test("new-line and comments are ignored after +", check("2 +\n// comment\n2", Lexeme(NUMBER, "2"), KeyToken::PLUS, Lexeme(NUMBER, "2")));
	test("new-line and comments are ignored after (", check("(\n// comment\n5)", LH, Lexeme(NUMBER, "5"), RH));
	test("new-line and comments are ignored after comma", check("a,// comment\nb", Lexeme(ID, "a"), COMA, Lexeme(ID, "b")));

	test("unexpected backslash", error("1\\ 2", CharIsUnexpected{'\\'}, {1, 2}));
}

void indentation()
{
	Section name = "indentation";
	test("1 line", check("a\n\tb", Lexeme(ID, "a"), BEGIN, Lexeme(ID, "b"), END));
	test("2 lines", check("a\n\tb\n\tc", Lexeme(ID, "a"), BEGIN, Lexeme(ID, "b"), NEW_LINE, Lexeme(ID, "c"), END));
	test("nested", check("a\n\tb\n\t\tc", Lexeme(ID, "a"), BEGIN, Lexeme(ID, "b"), BEGIN, Lexeme(ID, "c"), END, END));
	test("with empty line", check("a\n\tb\n\n\tc", Lexeme(ID, "a"), BEGIN, Lexeme(ID, "b"), NEW_LINE, Lexeme(ID, "c"), END));

	test("unclosed bracket", error("(", UnclosedBracket{}, {1, 1}));
	test("unclosed bracket [", error("[", UnclosedBracket{}, {1, 1}));
	test("unclosed bracket {", error("{", UnclosedBracket{}, {1, 1}));
	test("last unclosed bracket", error("((", UnclosedBracket{}, {1, 2}));
	test("unclosed bracket in block", error(
		"do\n"
		"	a = (if yes\n"
		"5)\n",
		UnclosedBracket{},
		{2, 6}));

	test("unmatched bracket", error(")", UnmatchedBracket{}, {1, 1}));
	test("unmatched bracket ]", error("]", UnmatchedBracket{}, {1, 1}));
	test("unmatched bracket }", error("}", UnmatchedBracket{}, {1, 1}));
	test("unmatched bracket 2", error(
		"do\n"
		"	pass)",
		UnmatchedBracket{},
		{2, 6}));
	test("unmatched brackets", error("{]", UnmatchedBracket{}, {1, 2}));
}

int main()
{
	simple();
	operators();
	number_literals();
	symbols();
	block_strings();
	identifiers();
	new_line();
	indentation();

	cout << "Passed: " << passed << "/" << n << endl;
	return 0;
}
