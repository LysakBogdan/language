#ifndef LEXER_ERROR_H_INCLUDED
#define LEXER_ERROR_H_INCLUDED

#include <variant>
#include <string_view>

struct InvalidOperator
{
	bool operator==(const InvalidOperator&) const = default;
};

struct TooBigCharCode {};
struct LineEndIsExpected
{
	bool operator==(const LineEndIsExpected&) const = default;
};

struct CharIsExpected
{
	char ch;
	bool operator==(const CharIsExpected&) const = default;
};

struct CharIsUnexpected
{
	char ch;
	bool operator==(const CharIsUnexpected&) const = default;
};

struct NotBinaryDigit
{
	char digit;
	bool operator==(const NotBinaryDigit&) const = default;
};

struct NotOctalDigit
{
	char digit;
	bool operator==(const NotOctalDigit&) const = default;
};

struct InvalidNumberLiteral
{
	bool operator==(const InvalidNumberLiteral&) const = default;
};

struct UnclosedBracket
{
	bool operator==(const UnclosedBracket&) const = default;
};

struct UnmatchedBracket
{
	bool operator==(const UnmatchedBracket&) const = default;
};

struct LexerError
{
	using Reason = std::variant<InvalidOperator, TooBigCharCode, LineEndIsExpected, CharIsExpected, CharIsUnexpected,
		NotBinaryDigit, NotOctalDigit, InvalidNumberLiteral,
		UnclosedBracket, UnmatchedBracket>;

	Reason reason;
	std::string_view view;
};

std::string inspect(const char*, const LexerError&);

#endif
