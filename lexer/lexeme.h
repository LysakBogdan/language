#ifndef LEXEME_H_INCLUDED
#define LEXEME_H_INCLUDED

#include "base/base.h"

enum class TLexeme
{
	NUMBER, STRING, SYMBOL, ID, KEY,
	COMA, SEP, NEW_LINE,
	LH, RH, LFH, RFH, LQH, RQH, BEGIN, END,
	FIELD, METHOD, EOT
};

enum class KeyToken
{
	PLUS, MINUS, MUL, DIV, IDIV, MOD, POW,
	EQUAL, GREAT, LESS, GEQUAL, LEQUAL, NEQUAL,
	PUSH,
	NOT, AND, OR,
	LENGTH,
	INC, DEC,
	RANGE, ELLIPSIS, COLON,
	QUESTION_MARK,
	ASSIGN, ARROW, PLUSA, MINUSA, MULA, DIVA, IDIVA, MODA, POWA,
	PAIR, SWAP,
	MATCH, PIN, LIKE, INTERSECTION, UNION,
	NIL, YES, NO,
	IF, ELSE, WHILE, FOR, IN, WITH, DO, GENERATOR,
	PASS, FAIL, RETURN, BREAK, CONTINUE, YIELD,
	PROC, SELF, WILDCARD,
	EVERY, SOME,
	_DO,
	KEYS_COUNT
};

std::string inspect(KeyToken);

struct Lexeme
{
	TLexeme type;
	std::string String;
	std::string_view view;

	Lexeme(TLexeme aType, const std::string& s): type(aType), String(s) {}
	explicit Lexeme(KeyToken key): type(TLexeme::KEY), String(inspect(key)) {};
	explicit Lexeme(TLexeme Type): type(Type) {};

	bool operator==(TLexeme tl) const
	{
		return type == tl;
	}

	bool operator==(KeyToken key) const
	{
		return type == TLexeme::KEY && String == inspect(key);
	}

	bool operator==(const char *s) const
	{
		return type == TLexeme::STRING && String == s;
	}

	bool operator==(const Lexeme& l) const
	{
		using enum TLexeme;
		if(type != l.type)
			return false;
		if(type == NUMBER || type == STRING || type == SYMBOL || type == ID || type == KEY)
			return String == l.String;
		return true;
	}

	friend std::ostream &operator<<(std::ostream& os, Lexeme l);

	template<class T>
	bool operator!=(T x) const
	{
		return !operator==(x);
	};
};

struct Location
{
	int line, b;
	size_t count = 1;
	bool operator==(const Location&) const = default;
};

std::string inspect(TLexeme);

#endif
