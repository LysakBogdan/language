#include "lexer_error.h"

#include "lexer.h"
#include <sstream>
#include <format>

using namespace std;

ostream& operator<<(ostream& os, const Location& loc)
{
	return os << "[" << loc.line << ":" << loc.b << "-" << loc.b + loc.count << "]";
}

string inspect(const InvalidOperator&)
{
	return "invalid operator";
}

string inspect(const TooBigCharCode&)
{
	return "code of char can't be > 255";
}

string inspect(const LineEndIsExpected&)
{
	return "expected end of line";
}

string inspect(const CharIsExpected& e)
{
	return format("expected '{}' character", string{e.ch});
}

string inspect(const CharIsUnexpected& e)
{
	return format("unexpected character '{}'", string{e.ch});
}

string inspect(const InvalidNumberLiteral& e)
{
	return "invalid number literal";
}

string inspect(const NotBinaryDigit& e)
{
	return format("'{}' is not a binary digit", e.digit);
}

string inspect(const NotOctalDigit& e)
{
	return format("'{}' is not an octal digit", e.digit);
}

string inspect(const UnclosedBracket& e)
{
	return "unclosed bracket";
}

string inspect(const UnmatchedBracket& e)
{
	return "unmatched bracket";
}

string inspect(const char* source, const LexerError& e)
{
	auto location = get_location(source, e.view);
	return format("{}: lexer error, {}", location.line, visit([](auto a) { return inspect(a); }, e.reason));
}
