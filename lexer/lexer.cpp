#include "lexer.h"

#include <algorithm>
#include <sstream>
#include <optional>
#include <tuple>

#include "tools.h"
#include "error.h"

using namespace std;
using enum CharClass;
using enum TLexeme;
using enum KeyToken;

string LexemeTypes[] =
{
	"number", "string", "symbol", "id", "token",
	",", ";", "NL",
	"(", ")", "{", "}", "[", "]", "<BEGIN>", "<END>",
	":_", "._", "EOF", "NONE"
};

string Keywords[] =
{
	"+", "-", "*", "/", "\\\\", "%", "**", "==", ">", "<", ">=", "<=", "<>",
	"<<", "!", "&&", "||", "#", "++", "--", "..", "...", ":",
	"?", ":=", "->", "+=", "-=", "*=", "/=", "\\\\=", "%=", "**=",
	"=>", "<->", "=", "^", "~", "&", "|",
	"nil", "yes", "no",
	"if", "else", "while", "for", "in", "with", "do", "__generator",
	"pass", "fail", "return", "break", "continue", "__yield",
	"proc", "self", "_",
	"every", "some",
	"__do"
};

optional<KeyToken> parse_operator(const string& op)
{
	auto index = find_index(Keywords, op);
	if(index)
		return KeyToken(*index);
	return nullopt;
}

string inspect(TLexeme l)
{
	return LexemeTypes[int(l)];
}

string inspect(KeyToken key)
{
	return Keywords[int(key)];
}

ostream& operator<<(ostream& os, Lexeme l)
{
	switch(l.type)
	{
		case NUMBER:
			os << l.String;
			break;
		case STRING:
			os << '"' << l.String << '"';
			break;
		case SYMBOL:
			os << "'" << l.String << "'";
			break;
		case ID:
			os << l.String;
			break;
		case KEY:
			os << l.String;
			break;
		case LH:
			os << "(";
			break;
		case RH:
			os << ")";
			break;
		case LFH:
			os << "{";
			break;
		case RFH:
			os << "}";
			break;
		case LQH:
			os << "[";
			break;
		case RQH:
			os << "]";
			break;
		case COMA:
			os << ",";
			break;
		case SEP:
			os << ";";
			break;
		case NEW_LINE:
			os << "[NEW LINE]";
			break;
		case EOT:
			os << "[END OF FILE]";
			break;
		case BEGIN:
			os << "[BLOCK BEGIN]";
			break;
		case END:
			os << "[BLOCK END]";
			break;
		case FIELD:
			os << ":";
			break;
		case METHOD:
			os << ".";
			break;
		default:
			CAN_NOT_HAPPEN;
	};

	return os;
}

tuple<int, int> get_char_location(CharIterator text, CharIterator c)
{
	auto line = 1;
	auto pos = 1;
	for(auto i = text; i != c; i++)
	{
		pos++;
		if(*i == '\n')
		{
			line++;
			pos = 1;
		}
	}
	return {line, pos};
}

Location get_location(CharIterator text, string_view v)
{
	auto [line, pos] = get_char_location(text, v.data());
	return Location{line, pos, v.size()};
}

Location get_location(const string& text, string_view v)
{
	return get_location(text.c_str(), v);
}

void skip_space(CharIterator& s)
{
	while(char_class(*s) == SPACE || char_class(*s) == TAB)
		++s;
}

CharIterator line_end(CharIterator s)
{
	while(*s && char_class(*s) != NEW_LINE_CHAR)
		s++;
	return s;
}

void skip_comment_line(CharIterator& s)
{
	s = line_end(s);
}

void skip_comments(CharIterator& s)
{
	skip_space(s);
	if(s[0] == '/' && s[1] == '/')
	{
		skip_comment_line(s);
		skip_space(s);
	}
}

void newLine(CharIterator& s)
{
	if(s[0] == '\r' && s[1] == '\n') // in case CR LF
		++s;
	++s;
}

void ignore_NL(CharIterator& s)
{
	skip_comments(s);
	while(char_class(*s) == NEW_LINE_CHAR)
	{
		newLine(s);
		skip_comments(s);
	}
}

char get(CharIterator& s)
{
	return *s++;
}

int digit(CharIterator& s)
{
	return get(s) - '0';
}

int read_escape_char(CharIterator& s)
{
	if(char_class(*s) == NULL_CHAR || char_class(*s) == NEW_LINE_CHAR)
		throw LexerError{CharIsUnexpected{*s}, {s, 1}};

	if(char_class(*s) == DIGIT)
	{
		auto begin = s;
		int code = 0;
		code = *s++ - '0';
		if(char_class(*s) == DIGIT)
		{
			code = 10*code + digit(s);
			if(char_class(*s) == DIGIT)
				code = 10*code + digit(s);
		}

		if(code > 255)
			throw LexerError{TooBigCharCode{}, {begin, size_t(s - begin)}};

		return code;
	}

	string escape = "bnrtva";
	string res = "\b\n\r\t\v\a";

	char c = get(s);
	auto i = escape.find_first_of(c);
	return i != string::npos ? res[i] : c;
}

int read_string_char(CharIterator& s)
{
	return *s == '\\' ? read_escape_char(++s) : get(s);
}

string read_line(CharIterator& s)
{
	auto end = line_end(s);
	string line;
	while(s != end)
		line += read_string_char(s);
	if(*s)
		newLine(s);
	return line;
}

string parse_escaped_chars(const char* s)
{
	string res;
	while(*s)
		res += read_string_char(s);
	return res;
}

template<int (*is_digit)(int c)>
void skip_digits(CharIterator &s)
{
	while(is_digit(*s))
	{
		++s;
		if(*s == '_')
		{
			if(!is_digit(s[1]))
				throw LexerError{InvalidNumberLiteral{}, {s, 1}};
			++s;
		}
	}
}

int is_binary(int c)
{
	return c == '0' || c == '1';
}

int is_octal(int c)
{
	return isdigit(c) && c < '8';
}

constexpr auto skip_decimal = skip_digits<isdigit>;
constexpr auto skip_hex = skip_digits<isxdigit>;
constexpr auto skip_binary = skip_digits<is_binary>;
constexpr auto skip_octal = skip_digits<is_octal>;

void skip_real(CharIterator& s)
{
	skip_decimal(s);
	if(*s == '.')
	{
		if(char_class(s[1]) != DIGIT)
			return;
		++s;
		skip_decimal(s);
	}

	if(*s == 'e' || *s == 'E')
	{
		++s;
		if(*s == '+' || *s == '-')
			++s;
		skip_decimal(s);
	}
}

Lexeme read_number(CharIterator& s)
{
	auto b = s;

	if(s[0] != '0' || s[1] == '.')
		skip_real(s);
	else switch(*++s)
	{
		case 'x':
		case 'X':
			skip_hex(++s);
			break;
		case 'b':
		case 'B':
			skip_binary(++s);
			if(isdigit(*s))
				throw LexerError{NotBinaryDigit{*s}, {s, 1}};
			break;
		case 'o':
		case 'O':
			skip_octal(++s);
			if(isdigit(*s))
				throw LexerError{NotOctalDigit{*s}, {s, 1}};
			break;
		default:
			skip_decimal(s);
	}

	return Lexeme(NUMBER, string(b, s));
}

string read_symbol(CharIterator& s)
{
	string id;
	while(char_class(*s) == LETTER || char_class(*s) == DIGIT)
		id += *s++;
	if(*s == '?' || *s == '!' || *s == '#')
		id += *s++;
	return id;
}

Lexeme read_id_or_keyword(CharIterator& s)
{
	auto id = read_symbol(s);
	if(find_index(Keywords, id))
		return {KEY, id};
	return {ID, id};
}

bool single_char_op(char c)
{
	return c == '!' || c == '#' || c == '^';
}

string read_operator_body(CharIterator& s)
{
	if(single_char_op(*s))
		return {*s++};

	string op;
	while(char_class(*s) == CHARACTER && !single_char_op(*s))
		op += *s++;
	return op;
}

KeyToken read_operator(CharIterator& s)
{
	auto begin = s;
	auto o = parse_operator(read_operator_body(s));
	if(!o)
		throw LexerError{InvalidOperator{}, {begin, size_t(s - begin)}};
	return *o;
}

string read_string(CharIterator& s, char delimiter = '"')
{
	string text;

	++s; // delimiter
	while(*s != delimiter)
	{
		if(char_class(*s) == NULL_CHAR || char_class(*s) == NEW_LINE_CHAR)
			throw LexerError{CharIsExpected{delimiter}, {s, 1}};
		text += read_string_char(s);
	}
	++s; // delimiter

	return text;
}

int get_indentation(CharIterator& ci)
{
	int n = 0;
	while(true)
	{
		if(char_class(*ci) == SPACE)
			n++;
		else if(char_class(*ci) == TAB)
			n = 4*n/4 + 4;
		else if(*ci == '\n')
			n = 0;
		else
			break;
		++ci;
	}
	return n;
}

bool try_pass_indentation(CharIterator& ci, int n)
{
	int m = 0;
	auto s = ci;

	for(; m < n; ++s)
		if(char_class(*s) == SPACE)
			m++;
		else if(char_class(*s) == TAB)
			m = 4*m/4 + 4;
		else
			break;

	if(m != n)
		return false;

	ci = s;
	return true;
}

void LexemeStream::set_new_indentation_level(int n, TokenInserter out)
{
	if(n > indentation)
	{
		opens.push_back({ci, n});
		out(Lexeme{BEGIN}, ci);
	}
	else
	{
		while(!opens.empty() && opens.back().indent > n)
			close_block(out);

		if(*ci != '}' && *ci != ']' && *ci != ')')
			out(Lexeme{NEW_LINE}, ci);
	}
	indentation = n;
}

void LexemeStream::indentations(TokenInserter out)
{
	int n = get_indentation(ci);
	skip_comments(ci);
	while(char_class(*ci) == NEW_LINE_CHAR)
	{
		newLine(ci);
		n = get_indentation(ci);
		skip_comments(ci);
	}

	if(!*ci)
		return;

	set_new_indentation_level(n, out);
}

void LexemeStream::close_block(TokenInserter out)
{
	if(opens.back().bracket)
		throw LexerError{UnclosedBracket{}, {opens.back().pos, 1}};
	opens.pop_back();
	out(Lexeme{END}, ci);
}

void LexemeStream::close_bracket(char bracket, CharIterator pos, TokenInserter out)
{
	while(!opens.empty() && !opens.back().bracket)
	{
		opens.pop_back();
		out(Lexeme(END), pos);
	}
	if(opens.empty() || opens.back().bracket != bracket)
		throw LexerError{UnmatchedBracket{}, {pos, 1}};
	opens.pop_back();
}

void LexemeStream::read_block_string(TokenInserter out)
{
	auto begin = ci;
	ci += 3;

	if(char_class(*ci) != NEW_LINE_CHAR)
		throw LexerError{LineEndIsExpected{}, {ci, 1}};
	newLine(ci);

	auto text_indentation = get_indentation(ci);

	if(text_indentation <= indentation)
	{
		out(Lexeme{SYMBOL, ""}, begin);
		set_new_indentation_level(text_indentation, out);
		return;
	}

	auto text = read_line(ci);
	while(*ci && try_pass_indentation(ci, text_indentation))
	{
		text.push_back('\n');
		text += read_line(ci);
	}

	out(Lexeme{SYMBOL, text}, begin);
	if(*ci)
		indentations(out);
}

Lexeme read_dot(CharIterator& s)
{
	if(isdigit(s[1]))
		return read_number(s);

	++s;

	if(char_class(*s) == LETTER)
		return Lexeme(METHOD);

	if(*s == '.')
	{
		if(s[1] == '.')
		{
			s += 2;
			return Lexeme(ELLIPSIS);
		}
		s += 1;
		return Lexeme(RANGE);
	}

	throw LexerError{CharIsUnexpected{*s}, {s, 1}};
}

Lexeme LexemeStream::separator(TokenInserter out)
{
	if(*ci == '.')
		return read_dot(ci);

	auto sep = ci++;
	switch(*sep)
	{
		case '(':
			ignore_NL(ci);
			opens.push_back({sep, indentation, '('});
			return Lexeme(LH);
		case '{':
			ignore_NL(ci);
			opens.push_back({sep, indentation, '{'});
			return Lexeme(LFH);
		case '[':
			ignore_NL(ci);
			opens.push_back({sep, indentation, '['});
			return Lexeme(LQH);

		case ')':
			close_bracket('(', sep, out);
			return Lexeme(RH);
		case '}':
			close_bracket('{', sep, out);
			return Lexeme(RFH);
		case ']':
			close_bracket('[', sep, out);
			return Lexeme(RQH);
		case ',':
			ignore_NL(ci);
			return Lexeme(COMA);
		case ';':
			return Lexeme(SEP);
		default:
			CAN_NOT_HAPPEN;
	}
}

void LexemeStream::getLexeme(TokenInserter ts)
{
	ignore_NL(ci);
	for(;;)
	{
		skip_comments(ci);
		auto begin = ci;
		switch(char_class(*ci))
		{
			case DIGIT:
				ts(read_number(ci), begin);
				break;
			case LETTER:
				ts(read_id_or_keyword(ci), begin);
				break;
			case CHARACTER:
			{
				if(ci[0] == ':' && char_class(ci[1]) == LETTER)
				{
					++ci;
					ts(Lexeme(FIELD), begin);
					break;
				}
				auto key = read_operator(ci);
				ts(Lexeme(key), begin);
				if(key != INC && key != DEC && key != COLON)
					ignore_NL(ci);
				break;
			}
			case QUOTE_MARK:
				ts(Lexeme{STRING, read_string(ci)}, begin);
				break;
			case SINGLE_QUOTE:
				ts(Lexeme{SYMBOL, read_string(ci, '\'')}, begin);
				break;
			case SEPARATOR:
				ts(Lexeme{separator(ts)}, begin);
				break;
			case DOLLAR:
				if(ci[1] == '$' && ci[2] == '$')
					read_block_string(ts);
				else
					ts(Lexeme{SYMBOL, read_symbol(++ci)}, begin);
				break;

			case NEW_LINE_CHAR:
				newLine(ci);
				indentations(ts);
				continue;
			case BACKSLASH:
			{
				auto start = ci++;
				if(*ci == '\\')
				{
					ci++;
					if(*ci == '=')
					{
						ci++;
						ts(Lexeme(IDIVA), begin);
					}
					else
						ts(Lexeme(IDIV), begin);
					continue;
				}
				if(char_class(*ci) != NEW_LINE_CHAR)
					throw LexerError{CharIsUnexpected{'\\'}, {start, 1}};
				newLine(ci);
				continue;
			}
			case NULL_CHAR:
				while(!opens.empty())
					close_block(ts);
				ts(Lexeme(EOT), begin);
				return;
			default:
				CAN_NOT_HAPPEN;
		}
	}
}

vector<Lexeme> LexemeStream::tokenize()
{
	vector<Lexeme> tokens;
	getLexeme({tokens, *this});
	return tokens;
}

vector<Lexeme> tokenize(CharIterator source)
{
	LexemeStream st(source);
	return st.tokenize();
}
