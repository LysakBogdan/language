#ifndef LEXER_H_INCLUDED
#define LEXER_H_INCLUDED

#include "lexeme.h"
#include "chars.h"
#include "lexer_error.h"
#include <vector>

class TokenInserter;

struct OpenToken
{
	CharIterator pos;
	int indent;
	char bracket = 0;
};

class LexemeStream
{
	CharIterator ci;
	std::vector<OpenToken> opens;

	int indentation = 0;

	void getLexeme(TokenInserter);
	void set_new_indentation_level(int n, TokenInserter);
	void indentations(TokenInserter);
	void close_block(TokenInserter out);
	void close_bracket(char bracket, CharIterator pos, TokenInserter out);
	void read_block_string(TokenInserter out);
	Lexeme separator(TokenInserter out);
public:
	explicit LexemeStream(const char *text): ci{text} {};
	std::vector<Lexeme> tokenize();
	friend TokenInserter;
};

class TokenInserter
{
public:
	std::vector<Lexeme>& tokens;
	LexemeStream& ls;
	void operator()(Lexeme l, CharIterator begin)
	{
		l.view = {begin, size_t(ls.ci - begin)};
		tokens.push_back(l);
	}
};

Location get_location(CharIterator text, std::string_view);
Location get_location(const std::string& text, std::string_view view);
std::vector<Lexeme> tokenize(CharIterator);

std::string parse_escaped_chars(const char*);

using LexemeIterator = const Lexeme*;

#endif
