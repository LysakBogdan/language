#include "chars.h"

#include <math.h>
#include "error.h"

using namespace std;
using enum CharClass;

CharClass char_class(char c)
{
	static string signs = "=+-*/|~!?#%^&<>:";
	static string separators = "(){}[];,.";

	if(isalpha(c) || c == '_')
		return LETTER;
	if(isdigit(c))
		return DIGIT;
	if(signs.find_first_of(c) != string::npos)
		return CHARACTER;
	if(separators.find_first_of(c) != string::npos)
		return SEPARATOR;

	switch(c)
	{
		case '$':
			return DOLLAR;
		case '"':
			return QUOTE_MARK;
		case '\'':
			return SINGLE_QUOTE;
		case ' ':
			return SPACE;
		case '\\':
			return BACKSLASH;
		case '\t':
			return TAB;
		case '\n':
		case '\r':
			return NEW_LINE_CHAR;
		case '\0':
			return NULL_CHAR;
		default:
			return WRONG_CHAR;
	}
};

int64_t parse_decimal(CharIterator &s)
{
	int64_t i = 0;
	while(char_class(*s) == DIGIT)
	{
		i *= 10;
		i += *s++ - '0';
		if(*s == '_')
			++s;
	}
	return i;
}

int64_t parse_binary(CharIterator& s)
{
	int64_t i = 0;
	while(*s == '0' || *s == '1')
	{
		i *= 2;
		i += *s++ - '0';
		if(*s == '_')
			++s;
	}
	return i;
}

int64_t parse_octal(CharIterator& s)
{
	int64_t i = 0;
	while(*s >= '0' && *s <= '7')
	{
		i *= 8;
		i += *s++ - '0';
		if(*s == '_')
			++s;
	}
	return i;
}

int64_t parse_hex(CharIterator& s)
{
	string digits = "0123456789abcdef";
	int64_t i = 0;
	while(isxdigit(*s))
	{
		i *= 16;
		i += digits.find_first_of(tolower(*s++));
		if(*s == '_')
			++s;
	}
	return i;
}

double parse_real(CharIterator& s)
{
	double num = parse_decimal(s);

	if(*s == '.')
	{
		if(char_class(s[1]) != DIGIT)
			return num;
		++s;

		int p = 10;
		while(char_class(*s) == DIGIT)
		{
			num += double(*s++ - '0')/p;
			p *= 10;
			if(*s == '_')
				++s;
		}
	}

	if(*s == 'e' || *s == 'E')
	{
		bool sign = true;

		++s;
		if(*s == '+')
			++s;
		if(*s == '-')
		{
			++s;
			sign = false;
		};

		int p = parse_decimal(s);
		if(!sign)
			p = -p;
		return num*pow(10, p);
	}

	return num;
}
