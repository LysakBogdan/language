#ifndef CHARS_H_INCLUDED
#define CHARS_H_INCLUDED

#include <cstdint>

enum class CharClass
{
	LETTER,
	DIGIT,
	CHARACTER,
	SEPARATOR,
	QUOTE_MARK,
	SINGLE_QUOTE,
	BACKSLASH,
	DOLLAR,
	SPACE,
	TAB,
	NEW_LINE_CHAR,
	WRONG_CHAR,
	NULL_CHAR
};

CharClass char_class(char);

using CharIterator = const char*;

int64_t parse_binary(CharIterator &s);
int64_t parse_octal(CharIterator &s);
int64_t parse_hex(CharIterator &s);
double parse_real(CharIterator &s);

#endif
