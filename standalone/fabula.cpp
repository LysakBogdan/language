#include "fabula.h"
#include "inspection.h"
#include "semantics.h"
#include "compiler.h"

#include "formatting.h"

#include <iostream>

namespace Fabula
{
const char* usage = "Usage:\n\n"
"  fabula [option] [argument]\n\n"
"Options and arguments:\n"
"  file\n    execute the script file\n"
"  --version\n    print the version number\n"
"  --help\n    print the usage information\n"
"  --e string\n    execute the string\n";

void show_result(const Result& res)
{
	cout << ">>> " << res.value << endl;
}

void log_error(const exception& e)
{
	LOG_LINE("error: " << e.what());
	cerr << "error: " << e.what() << endl;
}

Result Evaluate(VM& vm, const string& source)
{
	try
	{
		return {0, vm.eval(source)};
	}
	catch(const LexerError& e)
	{
		cerr << inspect(source.c_str(), e) << endl;
		return {1};
	}
	catch(const ParsingError& e)
	{
		cerr << inspect(source, e) << endl;
		return {1};
	}
	catch(const CompileError& e)
	{
		cerr << "Compile error: " << inspect(e) << endl;
		return {1};
	}
	catch(const RuntimeError& e)
	{
		cerr << "Error: " << inspect(e) << endl;
		return {2};
	}
	catch(const exception& e)
	{
		log_error(e);
		return {3};
	}
}
}
