#ifndef FABULA_H_INCLUDED
#define FABULA_H_INCLUDED

#include "config.h"
#include "parser/parser.h"
#include "vm/vm.h"

namespace Fabula
{
extern const char* usage;

struct Result
{
	int code;
	Value value;
};

Result Evaluate(VM&, const string&);
void log_error(const exception&);
void show_result(const Result&);
}

#endif
