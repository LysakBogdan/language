#ifndef CONFIG_H_INCLUDED
#define CONFIG_H_INCLUDED

constexpr const char* Version = "0.1.1";

#define TRACE_EXECUTION 0
#define TRACE_ONLY_CURRENT_FRAME 1

#define GC_ENABLED 1
#define TRACE_GC 0

#endif
