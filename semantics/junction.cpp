#include "junction.h"

#include "error.h"
#include "atoms.h"

using namespace AST;

struct JContext
{
	struct Core
	{
		Expr expr;
		Symbol id;
		Value function;
	};

	vector<Core> cores;
};

Expr junction(Expr e, JContext& c);

Value atom(TJunction type)
{
	switch(type)
	{
		case TJunction::Every:
			return atom("every_in");
		case TJunction::Some:
			return atom("some_in");
		default:
			CAN_NOT_HAPPEN;
	}
}

Symbol junction(const Junction& o, JContext& c)
{
	auto id = Symbol::unique();
	c.cores.push_back({o.a, id, atom(o.type)});
	return id;
}

Infix junction(const Infix& e, JContext& c)
{
	return {e.op, junction(e.a, c), junction(e.b, c)};
}

Value junction(const Value& e, JContext& c)
{
	return e;
}

Symbol junction(const Symbol& e, JContext& c)
{
	return e;
}

string junction(const string& e, JContext& c)
{
	return e;
}

Wildcard junction(const Wildcard& e, JContext& c)
{
	return e;
}

Lambda junction(const Lambda& e, JContext& c)
{
	return e;
}

ArrayConstructor junction(const ArrayConstructor& e, JContext& c)
{
	return e;
}

Cortege junction(const Cortege& o, JContext& c)
{
	// TODO: optimize it
	Cortege r;
	for(auto e : o)
		r.push_back(junction(e, c));
	return r;
}

Apply junction(const Apply& o, JContext& c)
{
	return {junction(o.exp, c), junction(o.args, c)};
}

Prefix junction(const Prefix& o, JContext& c)
{
	return {o.op, junction(o.exp, c)};
}

Postfix junction(const Postfix& o, JContext& c)
{
	return {o.op, junction(o.exp, c)};
}

TableConstructor junction(const TableConstructor& o, JContext& c)
{
	return o;
}

template<class T>
Expr junction(const T&, JContext&)
{
	NOT_IMPLEMENTED;
}

Expr junction(Expr e, JContext& c)
{
	return visit([&c](auto e) -> Expr { return junction(e, c); }, *e);
}

Expr extract_junction(Expr e)
{
	JContext context;
	auto ast = junction(e, context);
	for(const auto& core : context.cores)
		ast = Apply{core.function, {core.expr, Lambda{{{core.id}}, ast}}};
	return ast;
}
