#include "ast.h"

#include "tools.h"
#include "error.h"

LocalVar VarSpace::operator[](int i) const
{
	assert(i >= 0 && i < var_count);
	return {index + i};
}

EmptyBlock Block::Empty;

LocalVar Block::define_var(Symbol name)
{
	return {index + add_unique(vars, name)};
}

LocalVar Block::new_var()
{
	auto id = index + size();
	vars.emplace_back("_");
	return {id};
}

optional<LocalVar> Block::var(Symbol name) const
{
	if(auto i = find_index(vars, name); i)
		return LocalVar{index + *i};
	return base.var(name);
}

optional<UpVar> Proc::find_upvalue(Symbol name)
{
	if(auto var = block.var(name); var)
		return UpVar{add_unique(captures, Capture(*var))};
	if(parent)
		if(auto up = parent->find_upvalue(name); up)
			return UpVar{add_unique(captures, Capture(*up))};
	return nullopt;
}
