#include "execution_inspection.h"

#include "inspection.h"
#include "ast_inspection.h"
#include "formatting.h"

ostream& operator<<(ostream& os, const Execution& a)
{
	return os << inspect(a);
}

string inspect(const Block& o)
{
	return format("Block({})", inspect(o.vars));
}

string inspect(Void)
{
	return "void";
}

string inspect(const Pass&)
{
	return "pass";
}

template<class T>
ostream& operator<<(ostream& os, const unique_ptr<T>& a)
{
	return os << a->str();
}

ostream& operator<<(ostream& os, const pair<Execution, Execution>& o)
{
	return os << inspect(o.first) << " => " << inspect(o.second);
}

string inspect(const Cortege& o)
{
	stringstream ss;
	ss << "(" << o << ")";
	return ss.str();
}

string inspect(const ArrayConstructor& o)
{
	return format("[{}]", inspect(o.items));
}

string inspect(const TableConstructor& o)
{
	stringstream ss;
	ss << "(table";
	for(const auto& [key, value] : o.pairs)
		ss << " " << key << " " << value;
	ss << ")";
	return ss.str();
}

string inspect(const Map& o)
{
	return format("{{{}}}", inspect(o.mappings));
}

string inspect(const Sequence& o)
{
	return format("seq {}\n{}", inspect(o.a), inspect(o.b));
}

string inspect(const NValues& o)
{
	return LISP("nvalues", o.count, o.is_variadic);
}

string inspect(const Arguments& o)
{
	return "args";
}

string inspect(const Self&)
{
	return "self";
}

string inspect(const Wildcard&)
{
	return "_";
}

string inspect(const UpVar& u)
{
	return format("up_var({})", u.index);
}

string inspect(const LocalVar& o)
{
	return format("%{}", o.index);
}

string inspect(const GlobalVar& o)
{
	return o.name.str();
}

string inspect(const Pin& o)
{
	return format("^{}", o.a);
}

string inspect(const Contextual& o)
{
	return format("%{}", o.name.str());
}

string inspect(const Assert& o)
{
	return format("assert {}", o.a);
}

string inspect(const Check& o)
{
	return format("check {}", o.a);
}

string inspect(const GetReference& o)
{
	return format("&{}", o.a);
}

string inspect(const FunctionScope& o)
{
	return format("sub{{{}}}", inspect(o.body));
}

string inspect(const Assign& o)
{
	return LISP("=", o.a, o.b);
}

string inspect(const MultiAssign& o)
{
	return LISP("...=", o.a, o.b);
}

string inspect(const Match& o)
{
	return LISP("~", o.a, o.b);
}

string inspect(const MatchList& o)
{
	return LISP("match_list", o.a, o.b);
}

string inspect(const OpAssign& o)
{
	return LISP("op_assign", o.a);
}

string inspect(const Swap& o)
{
	return LISP("<->", o.a, o.b);
}

string inspect(const And& o)
{
	return LISP("and", o.a, o.b);
}

string inspect(const Or& o)
{
	return LISP("or", o.a, o.b);
}

string inspect(const PostAssign& o)
{
	return LISP("post_op", o.a);
}

string inspect(const Apply& o)
{
	return LISP("apply", o.obj, o.args);
}

string inspect(const Action& o)
{
	return LISP("action", o.obj, o.args);
}

string inspect(const Decons& o)
{
	return LISP("decons", o.obj, o.args);
}

string inspect(const Deconstruction& o)
{
	return LISP("deconstruction", o.items, o.obj, o.args);
}

string inspect(const VariadicApply& o)
{
	return LISP("variadic_apply", o.obj, o.args);
}

string inspect(const Params& o)
{
	return LISP("params", o.args, o.is_variadic);
}

string inspect(const If& o)
{
	return LISP("if", o.condition, o.body, o.else_branch);
}

string inspect(const While& o)
{
	return LISP("while", o.condition, o.body);
}

string inspect(const Lambda& o)
{
	return inspect(o.sub);
}

string inspect(const Break& b)
{
	return LISP("break", b.exp);
}

string inspect(const Continue& o)
{
	return LISP("continue", o.level);
}

string inspect(const Return& r)
{
	return LISP("return", r.exp);
}

string inspect(const MatchReturn& r)
{
	return LISP("match_return", r.values);
}

string inspect(const Let& o)
{
	return LISP("let", o.pattern, o.value, o.body);
}

string inspect(const LetList& o)
{
	return LISP("let_list", o.pattern, o.value, o.body);
}

string inspect(const VarBlock& o)
{
	return LISP("var_block", o.count, o.body);
}

string inspect(const Execution& e)
{
	return visit([](auto a) -> string { return inspect(a); }, *e);
}
