#ifndef EXECUTION_INSPECTION_H_INCLUDED
#define EXECUTION_INSPECTION_H_INCLUDED

#include "ast.h"
#include <format>

string inspect(const Execution& e);
ostream& operator<<(ostream& os, const Execution& a);

template<>
struct std::formatter<Execution>: std::formatter<std::string>
{
	auto format(const Execution& e, format_context& c) const
	{
		return formatter<string>::format(inspect(e), c);
	}
};

#endif
