#include "semantics.h"

#include "atoms.h"
#include "junction.h"
#include "tools.h"
#include "lexer.h"
#include <algorithm>
#include <sstream>
#include <functional>

using enum KeyToken;

Execution execution(const Expr& e, const EContext& context);

auto execution(const AST::Definition& o, const EContext& context)
{
	return Assign{execution(o.name, context), execution(o.exp, context)};
}

VarBlock in_block(const IVarSpace& space, const Execution& a)
{
	return VarBlock{space.size(), a};
}

auto in_local_block(const Expr& o, const EContext& context)
{
	const IBlock& parent = context.block();
	auto block = Block(parent);
	auto c = context.withBlock(block).value_mode();

	return in_block(block, execution(o, c));
}

auto execution(const AST::Do& o, const EContext& context)
{
	return in_local_block(o.exp, context);
}

Execution execution(const AST::Generator& o, const EContext& context)
{
	// TODO: Fix hardcode
	auto a = AST::Apply{Symbol("Coroutine"), {AST::Lambda{{}, o.exp}}};
	return execution(a, context);
}

auto execution(const AST::Cortege& o, const EContext& context)
{
	auto s = Cortege{};
	transform(begin(o), end(o), back_inserter(s), [&](auto e)
	{
		return execution(e, context);
	});
	return s;
}

Execution execution(const AST::ArrayConstructor& o, const EContext& context)
{
	auto items = execution(o.exps, context);
	if(o.is_variadic)
		return VariadicApply{atom("array"), items};
	return ArrayConstructor{items};
}

LetList with_arguments(const Params& ps, const Execution& body)
{
	return {ps.args, NValues{Arguments{}, int(ps.args.size()), ps.is_variadic}, body};
}

Params execution(const AST::Parameters& ps, const EContext& context)
{
	return {execution(ps.args, context), ps.is_variadic};
}

FunctionScope function_body(const AST::Parameters& params, const Expr& exp, const EContext& context)
{
	auto proc = Proc{context.block(), context.proc()};
	auto block = Block();
	auto c = context.withBlock(block).withProc(&proc);

	auto pattern = execution(params, c.pattern_mode());

	auto body = execution(exp, c.value_mode());

	auto scoped = in_block(block, with_arguments(pattern, body));
	return {proc.captures, scoped};
}

auto execution(const AST::TableConstructor& o, const EContext& context)
{
	auto t = TableConstructor{};
	for(const auto& [pattern, expression] : o.pairs)
		t.pairs.push_back({execution(pattern, context), execution(expression, context)});
	return t;
}

Execution execution(const AST::MappingConstructor& o, const EContext& context)
{
	auto m = Map{};
	for(const auto& [pattern, expression] : o.pairs)
		m.mappings.push_back({function_body(AST::Parameters{{pattern}}, expression, context)});
	return m;
}

auto condition(Expr e, const EContext& context)
{
	return execution(extract_junction(e), context);
}

If if_execution(AST::If o, const EContext& c)
{
	return
	{
		condition(o.condition, c),
		in_local_block(o.yes, c),
		in_local_block(o.no, c)
	};
}

Execution execution(AST::If o, const EContext& context)
{
	if(context.is_pattern())
		return if_execution(o, context);

	auto block = Block(context.block());
	auto body = if_execution(o, context.withBlock(block).value_mode());
	return in_block(block, body);
}

auto execution(AST::While o, const EContext& context)
{
	auto block = Block(context.block());
	auto c = context.withBlock(block).value_mode();
	auto body = While
	{
		condition(o.condition, c),
		in_local_block(o.body, c)
	};
	return in_block(block, body);
}

auto IfLet(const Execution& pattern, const Execution& value, const Execution& body)
{
	return If{Match{pattern, value}, body, Pass{}};
}

auto for_all(const EContext& c, Execution collection, function<Execution(const Execution&)> body)
{
	auto it = c.block().new_var();
	auto e = c.block().new_var();
	auto loop = Sequence
	{
		Assign{it, collection},
		While
		{
			Deconstruction{Cortege{e, it}, atom("as_seq"), {it}},
			body(e)
		}
	};
	return in_block(c.block(), loop);
}

auto first_match(Expr pattern, Expr collection, const EContext& context)
{
	auto block = Block(context.block());
	auto local = context.withBlock(block);
	auto p = execution(pattern, local.pattern_mode());
	return for_all(local, execution(collection, context), [&](const Execution& e)
	{
		return IfLet(p, e, Break{e});
	});
}

auto execution(AST::For o, const EContext& context)
{
	auto block = Block(context.block());
	auto local = context.withBlock(block);
	auto pattern = execution(o.var, local.pattern_mode());
	return for_all(local, execution(o.collection, context), [&](const Execution& e)
	{
		return IfLet(pattern, e, execution(o.body, local));
	});
}

auto execution(AST::ForPairs o, const EContext& context)
{
	auto block = Block(context.block());
	auto local = context.withBlock(block);

	auto it = block.new_var();
	auto key = block.new_var();
	auto var = block.new_var();

	auto key_pattern = execution(o.key, local.pattern_mode());
	auto value_pattern = execution(o.value, local.pattern_mode());
	auto body = Sequence
	{
		Assign{it, execution(o.collection, local)},
		While
		{
			Deconstruction{{key, var, it}, atom("decons_pair"), {it}},
			IfLet(key_pattern, key,
				IfLet(value_pattern, var,
					execution(o.body, local)
				)
			)
		}
	};
	return in_block(block, body);
}

auto execution(AST::Let o, const EContext& context)
{
	auto block = Block(context.block());
	auto c = context.withBlock(block);

	auto pattern = execution(o.pattern, c.pattern_mode());
	auto value = execution(o.value, c);
	auto body = in_local_block(o.body, c);

	return in_block(block, Let{pattern, value, body});
}

Execution execution(Symbol name, const EContext& context)
{
	if(context.is_pattern())
		return context.block().define_var(name);

	if(auto var = context.block().var(name); var)
		return *var;

	if(auto up = context.proc()->find_upvalue(name); up)
		return *up;

	return GlobalVar{name};
}

Execution execution(const AST::Infix& o, const EContext& context)
{
	if(o.op == SWAP)
		return Swap
		{
			execution(o.a, context.reference_mode()),
			execution(o.b, context.reference_mode())
		};

	if(o.op == ASSIGN)
		return Assign{execution(o.a, context.reference_mode()), execution(o.b, context)};

	if(o.op == MATCH)
		return Match{execution(o.a, context.pattern_mode()), execution(o.b, context)};
	if(o.op == IN)
		return Match{execution(o.a, context), first_match(o.a, o.b, context)};

	if(o.op >= PLUSA && o.op <= POWA)
	{
		auto method = binary_op(KeyToken(int(o.op) - int(PLUSA)));
		auto a = execution(o.a, context.reference_mode());
		auto b = execution(o.b, context);
		return OpAssign{method, {a, b}};
	}

	if(o.op == AND)
		return And{execution(o.a, context), execution(o.b, context)};

	if(o.op == OR)
		return Or{execution(o.a, context), execution(o.b, context)};

	return Apply
	{
		binary_op(o.op),
		{execution(o.a, context), execution(o.b, context)}
	};
}

Lambda execution(const AST::Lambda& o, const EContext& context)
{
	return {function_body(o.params, o.exp, context)};
}

auto execution(const Value& o, const EContext&)
{
	return o;
}

auto execution(const string& s, const EContext&)
{
	return s;
}

auto assert_string(const Location& location)
{
	stringstream ss;
	ss << "assertion fail " << location;
	return ss.str();
}

Apply execution(const AST::Apply& o, const EContext& context)
{
	// TODO: fix hardcode
#ifdef DEBUG
	if(holds_alternative<Symbol>(*o.exp))
		if(get<Symbol>(*o.exp) == Symbol("show"))
		{
			if(o.args.size() != 1 || !holds_alternative<Symbol>(*o.args[0]))
				NOT_IMPLEMENTED;
			auto name = get<Symbol>(*o.args[0]);
			return {atom("print"), {name.str() + " = ", execution(name, context)}};
		}
#endif

	if(holds_alternative<Symbol>(*o.exp))
		if(get<Symbol>(*o.exp) == Symbol("assert"))
		{
			if(o.args.size() != 1)
				NOT_IMPLEMENTED;
			auto location = get_location(context.source(), o.view);
			auto message = assert_string(location);
			return {atom("assert"), {execution(o.args[0], context), make_value(message)}};
		}

	auto c = context.value_mode();
	return {execution(o.exp, c), execution(o.args, c)};
}

Action execution(const AST::Action& o, const EContext& context)
{
	auto c = context.value_mode();
	return {execution(o.exp, c), execution(o.args, c)};
}

VariadicApply execution(const AST::VariadicApply& o, const EContext& context)
{
	auto c = context.value_mode();
	return {execution(o.exp, c), execution(o.args, c)};
}

Void execution(AST::Void, const EContext&)
{
	return {};
}

Self execution(AST::Self, const EContext&)
{
	return {};
}

Wildcard execution(AST::Wildcard, const EContext&)
{
	return {};
}

Pass execution(AST::Pass, const EContext&)
{
	return {};
}

Execution execution(AST::Fail, const EContext&)
{
	return Assert{Value::Nil};
}

Contextual execution(const AST::Contextual& o, const EContext&)
{
	return {o.name};
}

Execution execution(const AST::Sequence& o, const EContext& c)
{
	return Sequence{execution(o.a, c), in_local_block(o.b, c)};
}

Execution execution(const AST::Junction& o, const EContext& context)
{
	CAN_NOT_HAPPEN;
}

Apply execution(const AST::Reduction& o, const EContext& context)
{
	return {atom("reduce"), {binary_op(o.op), execution(o.seq, context), execution(o.init, context)}};
}

Execution execution(AST::Prefix u, const EContext& context)
{
	if(u.op == PIN)
		return Pin{execution(u.exp, context.value_mode())};
	if(u.op == INC || u.op == DEC)
		return OpAssign{unary_op(u.op), {execution(u.exp, context.reference_mode())}};
	if(u.op == INTERSECTION)
		return GetReference{{execution(u.exp, context.reference_mode())}};
	return Apply{unary_op(u.op), {execution(u.exp, context.value_mode())}};
}

PostAssign execution(AST::Postfix o, const EContext& context)
{
	return {unary_op(o.op), {execution(o.exp, context.reference_mode())}};
}

Break execution(AST::Break o, const EContext& context)
{
	return {execution(o.exp, context), o.level};
}

Continue execution(AST::Continue o, const EContext& context)
{
	return {o.level};
}

Return execution(AST::Return o, const EContext& context)
{
	return {execution(o.exp, context)};
}

Execution execution(AST::Yield o, const EContext& context)
{
	// TODO: Fix hardcode
	return execution(AST::Apply{Symbol("yield"), {o.exp}}, context);
}

Cortege var_list(const VarSpace& space)
{
	Cortege vars;
	vars.reserve(space.size());
	for(int i = 0; i < space.size(); i++)
		vars.push_back(space[i]);
	return vars;
}

Execution sequence(const vector<Execution>& xs)
{
	if(xs.empty())
		return Pass{};

	Execution r = xs[0];
	for(int i = 1; i < int(xs.size()); i++)
		r = Sequence{r, xs[i]};
	return r;
}

map<KeyToken, const char*> unary_to_atom_name =
{
	{MINUS, "negate"},
	{PLUS, "identity"},
	{NOT, "not"},
	{MUL, "get"},
	{LENGTH, "size"},
	{INC, "next"},
	{DEC, "prev"}
};

Value unary_op(KeyToken op)
{
	return atom(sure_at(unary_to_atom_name, op));
}

map<KeyToken, const char*> binary_to_atom_name =
{
	{PLUS, "plus"},
	{MINUS, "minus"},
	{MUL, "mult"},
	{DIV, "div"},
	{IDIV, "idiv"},
	{MOD, "mod"},
	{POW, "pow"},

	{INTERSECTION, "intersection"},
	{UNION, "union"},
	{PIN, "symmetric_difference"},

	{EQUAL, "equal"},
	{NEQUAL, "not_equal"},
	{GREAT, "greater"},
	{LESS, "less"},
	{GEQUAL, "greater_equal"},
	{LEQUAL, "less_equal"},
	{LIKE, "equals"},

	{RANGE, "range"},
	{PUSH, "push"}
};

Value binary_op(KeyToken op)
{
	return atom(sure_at(binary_to_atom_name, op));
}

Execution execution(const Expr& e, const EContext& context)
{
	return visit([&](auto a) -> Execution { return execution(a, context); }, e.self->value);
}

FunctionScope execution(const Expr& e, const char* source)
{
	Block global;
	auto proc = Proc{Block::Empty, nullptr};
	auto c = EContext(source, global, &proc);

	auto body = in_local_block(e, c);
	return {proc.captures, in_block(global, body)};
}
