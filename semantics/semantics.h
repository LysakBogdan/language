#ifndef SEMANTICS_H_INCLUDED
#define SEMANTICS_H_INCLUDED

#include "error.h"
#include "ast.h"

class EContext
{
	enum class Mode {Value, Reference, Pattern};

	const char* _source;
	Block* _block;
	Proc* _proc;
	Mode mode;
public:
	EContext(const char* s, Block& b, Proc* p = nullptr, Mode m = Mode::Value):
		_source(s), _block(&b), _proc(p), mode(m)
	{}

	EContext withBlock(Block& b) const
	{
		EContext c(*this);
		c._block = &b;
		return c;
	}

	EContext withProc(Proc* p) const
	{
		EContext c(*this);
		c._proc = p;
		return c;
	}

	EContext withMode(Mode m) const
	{
		EContext c(*this);
		c.mode = m;
		return c;
	}

	EContext reference_mode() const
	{
		return withMode(Mode::Reference);
	}

	EContext value_mode() const
	{
		return withMode(Mode::Value);
	}

	EContext pattern_mode() const
	{
		return withMode(Mode::Pattern);
	}

	Block& block() const
	{
		return *_block;
	}

	Proc* proc() const
	{
		return _proc;
	}

	const char* source() const
	{
		return _source;
	}

	bool is_value() const
	{
		return mode == Mode::Value;
	}

	bool is_reference() const
	{
		return mode == Mode::Reference;
	}

	bool is_pattern() const
	{
		return mode == Mode::Pattern;
	}
};

Value unary_op(KeyToken);
Value binary_op(KeyToken);

FunctionScope execution(const Expr& e, const char* source);
VarBlock in_block(const IVarSpace& vars, const Execution& e);
Execution match(const Execution& pattern, const Execution& e, VarSpace space);
Execution precomputed_match(const Execution& pattern, const Execution& e, VarSpace space);
Execution match_list(const Cortege& pattern, const NValues& e, VarSpace space);
bool is_known(const Execution&);

#endif
