#ifndef JUNCTION_H_INCLUDED
#define JUNCTION_H_INCLUDED

#include "grammar.h"

Expr extract_junction(Expr);

#endif
