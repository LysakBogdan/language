#ifndef AST_H_INCLUDED
#define AST_H_INCLUDED

#include <optional>
#include "parser/grammar.h"

struct ExecutionVariant;

struct Execution
{
	unique_ptr<ExecutionVariant> self;
	template<class T>
	Execution(T x): self(make_unique<ExecutionVariant>(ExecutionVariant{std::move(x)})) {}
	Execution(const Execution& x): self(make_unique<ExecutionVariant>(*x.self)) {}
	Execution(Execution&&) noexcept = default;

	Execution& operator=(const Execution& x)
	{
		return *this = Execution(x);
	}
	Execution& operator=(Execution&&) noexcept = default;
	template<class T>
	optional<T> as() const;
};

struct Void {};
struct Pass {};

struct Arguments {};
struct NValues {Execution a; int count; bool is_variadic = false;};
struct Self {};
struct Wildcard {};
struct UpVar {int index;};
struct LocalVar {int index;};
struct GlobalVar {Symbol name;};

struct Sequence {Execution a, b;};
using Cortege = vector<Execution>;

struct Apply
{
	Execution obj;
	Cortege args;
};

struct Action
{
	Execution obj;
	Cortege args;
};

struct Decons
{
	Execution obj;
	Cortege args;
};

struct Deconstruction
{
	Cortege items;
	Execution obj;
	Cortege args;
};

struct VariadicApply
{
	Execution obj;
	Cortege args;
};

struct Params { Cortege args; bool is_variadic = false; };

struct Pin {Execution a;};
struct Contextual {Symbol name;};
struct Assert {Execution a;};
struct Check {Execution a;};

struct Assign {Execution a, b;};
struct MultiAssign {Cortege a; NValues b;};
struct Match {Execution a, b;};
struct MatchList {Cortege a; NValues b;};
struct OpAssign {Apply a;};
struct PostAssign {Apply a;};

struct GetReference {Execution a;};

struct Swap {Execution a, b;};
struct And {Execution a, b;};
struct Or {Execution a, b;};

class IVarSpace
{
public:
	virtual int size() const = 0;
};

struct VarSpace: IVarSpace
{
	int index, var_count;
	VarSpace(int shift, int n = 0): index(shift), var_count(n)
	{
	}

	int size() const override { return var_count; }

	VarSpace sub(int n) const
	{
		return {index + var_count, n};
	}

	LocalVar operator[](int i) const;
};

struct IBlock
{
	virtual optional<LocalVar> var(Symbol name) const = 0;
	virtual int child_index() const = 0;
};

struct EmptyBlock: IBlock
{
	optional<LocalVar> var(Symbol name) const override
	{
		return nullopt;
	}
	int child_index() const override
	{
		return 0;
	}
};

struct Block: IBlock, IVarSpace
{
	const IBlock& base;
	int index;
	vector<Symbol> vars;

	explicit Block(const IBlock& parent = Empty):
		base(parent),
		index(parent.child_index())
	{}

	LocalVar new_var();
	LocalVar define_var(Symbol name);
	optional<LocalVar> var(Symbol name) const override;
	int child_index() const override
	{
		return index + vars.size();
	}

	int begin() const { return index; }
	int size() const override { return vars.size(); }

	static EmptyBlock Empty;
};

struct Proc;
optional<UpVar> capture(Proc*, const IBlock& scope, Symbol);

// TODO: Optimize this, create execution variant
Cortege var_list(const VarSpace&);
Execution sequence(const vector<Execution>&);

struct Let
{
	Execution pattern;
	Execution value;
	Execution body;
};

struct LetList
{
	Cortege pattern;
	NValues value;
	Execution body;
};

struct VarBlock
{
	int count;
	Execution body;
};

struct If
{
	Execution condition;
	Execution body;
	Execution else_branch;
};

struct While
{
	Execution condition;
	Execution body;
};

struct Break {Execution exp; int level = 1;};
struct Return {Execution exp;};
struct MatchReturn {Cortege values;};
struct Continue {int level = 1;};

using UpRowProto = vector<int>;
using Capture = variant<LocalVar, UpVar>;

struct Proc
{
	IBlock& block;
	Proc* parent;
	vector<Capture> captures;

	optional<UpVar> find_upvalue(Symbol);
};

struct FunctionScope
{
	vector<Capture> captures;
	VarBlock body;
};

struct ArrayConstructor
{
	Cortege items;
};

struct Lambda
{
	FunctionScope sub;
};

struct TableConstructor
{
	vector<pair<Execution, Execution>> pairs;
};

struct Map
{
	vector<Lambda> mappings;
};

struct ExecutionVariant
{
	using Variant = variant
	<
		Void, Pass,
		Value, string,
		Arguments, Self, Wildcard, UpVar, LocalVar, GlobalVar,
		Sequence, Cortege, NValues, Apply, Action, Decons, Deconstruction, VariadicApply, Params, Pin, Contextual, Assert, Check,
		Assign, MultiAssign, Match, MatchList, OpAssign, PostAssign, GetReference, Swap, And, Or,
		Let, LetList, VarBlock, If, While, Break, Return, MatchReturn, Continue,
		FunctionScope, ArrayConstructor, TableConstructor, Map, Lambda
	>;
	Variant value;
};

inline ExecutionVariant::Variant& operator*(const Execution& o)
{
	return o.self->value;
}

template<class T>
optional<T> Execution::as() const
{
	if(!holds_alternative<T>(self->value))
		return nullopt;
	return get<T>(self->value);
}

inline bool operator==(LocalVar a, LocalVar b)
{
	return a.index == b.index;
}

inline bool operator==(UpVar a, UpVar b)
{
	return a.index == b.index;
}

#endif
