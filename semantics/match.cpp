#include "semantics.h"

#include "atoms.h"
#include "function.h"
#include "execution_inspection.h"
#include <algorithm>

template<class T>
Execution match_value(const T&, const LocalVar&)
{
	NOT_IMPLEMENTED;
}

template<class T>
Execution match(const T& pattern, const Execution& e, VarSpace space)
{
	return precomputed_match(pattern, e, space);
}

template<class A, class B>
Execution assert_eq(const A& a, const B& b)
{
	return Assert{Apply{atom("equal"), {a, b}}};
}

Execution match(const Value& value, const Execution& e, VarSpace space)
{
	return assert_eq(value, e);
}

Execution success(const Execution& e)
{
	return Sequence{Assert{e}, e};
}

Execution match_value(const LocalVar& var, const LocalVar& value)
{
	return Assign{var, success(value)};
}

Execution match(const GlobalVar& var, const Execution& e, VarSpace space)
{
	return Assign{var, e};
}

Execution match(const Pin& pin, const Execution& e, VarSpace space)
{
	return assert_eq(pin.a, e);
}

Execution match(const Contextual& o, const Execution& e, VarSpace space)
{
	return Assign{GlobalVar{o.name}, e};
}

Execution match(const Wildcard&, const Execution& e, VarSpace space)
{
	return success(e);
}

Execution match_value(const Cortege& c, const LocalVar& value)
{
	vector<Execution> matches = {};
	auto i = value.index;

	for(const auto& item : c)
		matches.push_back(Match{item, LocalVar{i++}});

	return sequence(matches);
}

Execution match(const Sequence& s, const Execution& e, VarSpace space)
{
	return Match{s.a, e};
}

Execution match(const Let& scope, const Execution& e, VarSpace space)
{
	return Match{scope.body, e};
}

Execution match(const LetList& scope, const Execution& e, VarSpace space)
{
	return Match{scope.body, e};
}

Execution match(const VarBlock& o, const Execution& e, VarSpace space)
{
	return Match{o.body, e};
}

Execution match(const If& o, const Execution& e, VarSpace space)
{
	return Sequence
	{
		Match{o.body, e},
		Assert{o.condition}
	};
}

Execution match_value(const ArrayConstructor& a, const LocalVar& arr)
{
	return Sequence
	{
		Assert{Apply{atom("greater_equal"), {Value::Int(a.items.size()), Apply{atom("size"), {arr}}}}},
		Deconstruction{a.items, atom("unpack"), {arr}}
	};
}

bool is_known(const Execution& e)
{
	if(holds_alternative<Value>(*e))
		return true;
	if(holds_alternative<LocalVar>(*e) || holds_alternative<Wildcard>(*e) || holds_alternative<Or>(*e) || holds_alternative<And>(*e))
		return false;
	if(holds_alternative<Sequence>(*e))
	{
		auto s = get<Sequence>(*e);
		return is_known(s.b);
	}
	if(holds_alternative<Cortege>(*e))
	{
		auto c = get<Cortege>(*e);
		return ranges::all_of(c, is_known);
	}
	if(holds_alternative<ArrayConstructor>(*e))
	{
		auto a = get<ArrayConstructor>(*e);
		return ranges::all_of(a.items, is_known);
	}
	if(holds_alternative<Apply>(*e))
	{
		auto a = get<Apply>(*e);
		return is_known(a.args);
	}
	if(holds_alternative<If>(*e))
	{
		auto a = get<If>(*e);
		// TODO: what if condition is known and is true, but else_branch is not?
		return is_known(a.condition) && is_known(a.body) && is_known(a.else_branch);
	}
	CAN_NOT_HAPPEN;
}

Execution sub_match(Value method, const Execution& target, const Execution& arg, const Execution& e)
{
	return Sequence{Match{target, Apply{method, {e, arg}}}, e};
}

Execution match_value(const Apply& a, const LocalVar& e)
{
	if(holds_alternative<Value>(*a.obj))
	{
		auto v = get<Value>(*a.obj);
		if(v == atom("plus"))
		{
			// TODO: calculate at compile time
			if(is_known(*a.args[0]) && is_known(*a.args[1]))
				return Match{Pin{a}, e};
			if(is_known(*a.args[1]))
				return sub_match(atom("minus_right"), a.args[0], a.args[1], e);
			if(is_known(*a.args[0]))
				return sub_match(atom("minus_left"), a.args[1], a.args[0], e);
			NOT_IMPLEMENTED;
		}
		else if(v == atom("less") || v == atom("greater") || v == atom("less_equal") || v == atom("greater_equal"))
		{
			if(is_known(*a.args[1]))
				return Match{a.args[0], Assert{Apply{v, {e, a.args[1]}}}};
			NOT_IMPLEMENTED;
		}
	}

	auto inverse = Apply{atom("inverse"), {a.obj}};
	return Deconstruction{a.args, inverse, {e}};
}

Execution match_value(const VariadicApply& a, const LocalVar& e)
{
	auto inverse = Apply{atom("inverse"), {a.obj}};
	return MatchList{a.args, NValues{Decons{inverse, {e}}, int(a.args.size()), true}};
}

Execution match_value(const And& a, const LocalVar& e)
{
	return Sequence{Match{a.a, e}, Match{a.b, e}};
}

Execution match_value(const Or& o, const LocalVar& e)
{
	return Assert{Or{Check{Match{o.a, e}}, Check{Match{o.b, e}}}};
}

template<class T>
Execution precomputed_match(const T& pattern, const Execution& e, VarSpace space)
{
	auto local = space.sub(1);
	auto precomputed = local[0];
	return in_block(local, Sequence{Assign{precomputed, e}, match_value(pattern, precomputed)});
}

Execution match_list(const Cortege& pattern, const NValues& e, VarSpace space)
{
	if(pattern.empty())
		return Pass{};

	auto local = space.sub(pattern.size());
	auto values = var_list(local);
	return in_block(local, Sequence{MultiAssign{values, e}, match_value(pattern, local[0])});
}

Execution match(const Match& o, const Execution& e, VarSpace space)
{
	return Match{o.a, Or{e, o.b}};
}

Execution match(const Execution& pattern, const Execution& e, VarSpace space)
{
	return visit([=](auto a) -> Execution { return match(a, e, space); }, *pattern);
}
