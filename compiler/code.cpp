#include "code.h"

void Code::add(TCommand cmd, short a, short b, short c)
{
	proto.code.push_back({cmd, a, b, c});
	if(Command::info[int(cmd)].push)
		push();
}

int Code::index() const
{
	return _index;
}

void closeBreaks(Code& code)
{
	for(auto index : code.loop->breaks)
		code[index].a = code.pos();
}

void closeCheckBlock(Code& code, int r)
{
	for(auto index : code.check->fails)
	{
		code[index].a = code.pos();
		code[index].b = r;
	}
}
