#ifndef CODE_H_INCLUDED
#define CODE_H_INCLUDED

#include "function.h"
#include "module.h"
#include "memory.h"

struct Block;

struct Loop
{
	int start;
	int index;
	vector<int> breaks;
};

struct CheckBlock
{
	vector<int> fails;
};

class Code
{
	int _index = 0;
public:
	int locals_count = 0;
	FuncProto& proto;
	GC &gc;
	Loop* loop = nullptr;
	CheckBlock* check = nullptr;

	using enum TCommand;

	Code(FuncProto& aProto, GC &aGC): proto(aProto), gc(aGC)
	{}

	int addConstant(Value c)
	{
		return proto.addConstant(c);
	}

	void add(TCommand, short a = 0, short b = 0, short c = 0);

	void operator()(TCommand cmd, short a = 0, short b = 0, short c = 0)
	{
		add(cmd, a, b, c);
	}

	int append(TCommand cmd, short a = 0, short b = 0, short c = 0)
	{
		auto p = pos();
		add(cmd, a, b, c);
		return p;
	}
	Command& operator[](int i){ return proto.code[i]; }

	int pos() const
	{
		return proto.code.size();
	}

	int index() const;

	int alloc(int count = 1)
	{
		int res = locals_count;
		locals_count += count;
		add(LocalsCount, locals_count);
		return res;
	}

	void push()
	{
		++_index;
	}

	void pop()
	{
		assert(_index != 0);
		--_index;
		add(Pop);
	}

	void pop_n(int n)
	{
		while(n--)
			pop();
	}

	void dup()
	{
		add(Push, _index - 1);
	}

	void set_count(int n)
	{
		add(SetCount, n);
		_index += n;
	}

	void set()
	{
		add(Set);
		_index -= 2;
	}

	void add_pair()
	{
		add(AddPair);
		_index -= 2;
	}

	void set_local(int var)
	{
		add(SetLocal, var);
		--_index;
	}

	void set_global(Symbol name)
	{
		add(SetGlobal, addConstant(value(name)));
		--_index;
	}

	void dip(int r)
	{
		add(Dip, r);
		_index = r + 1;
	}

	void binary(TCommand cmd)
	{
		add(cmd);
		_index--;
	}

	void apply(int argn)
	{
		_index -= argn;
		add(CmdApply, argn + 1);
	}

	void decons(int argn)
	{
		_index -= argn + 1;
		add(CmdDecons, argn + 1);
	}

	void unpack()
	{
		add(Unpack);
		--_index;
	}

	void new_array(int count)
	{
		add(NewArray, count);
		++_index -= count;
	}

	void take_index(Code& code)
	{
		_index = code._index;
		locals_count = code.locals_count;
	}

	void fixed_values(int n)
	{
		add(FixedValues, n - 1);
		_index += n;
	}

	VarSpace var_space(int n = 0) const
	{
		return {locals_count, n};
	}

	void assert_true()
	{
		auto r = _index - 1;

		if(check)
		{
			auto fail = append(CheckCmd);
			check->fails.push_back(fail);
		}
		else
			add(AssertCmd, r);
	}
};

void closeBreaks(Code& code);
void closeCheckBlock(Code& code, int r);

void compile_primitive(Code&, TCommand, const Cortege&);

#endif
