#include "reference.h"

int ref_info_size(const Execution& e)
{
	return visit([](auto a) -> int { return ref_info_size(a); }, *e);
}

int image(Code& code, const Execution& e)
{
	return visit([&code](auto a) -> int { return image(code, a); }, *e);
}

void write(Code& code, const Execution& e, int ref)
{
	visit([&code, ref](auto a)
	{
		write(code, a, ref);
	}, *e);
}

void read(Code& code, const Execution& e, int ref)
{
	visit([&code, ref](auto a)
	{
		read(code, a, ref);
	}, *e);
}

int empty_image(Code& code)
{
	return code.locals_count;
}

Reference reference(Code& code, const Execution& e)
{
	auto r = image(code, e);
	return {code, e, r};
}

void Reference::write()
{
	::write(code, o, ref);
}

void Reference::read()
{
	::read(code, o, ref);
}
