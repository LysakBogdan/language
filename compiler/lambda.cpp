#include "compiler.h"

void compile(const Lambda& o, Code& code)
{
	auto proto = code.gc.create<FuncProto>(o.sub);
	auto id = code.proto.addConstant(proto.value());
	code(TCommand::NewFunction, id);

	Code lambda_code(*proto, code.gc);
	compile(o.sub, lambda_code);
}
