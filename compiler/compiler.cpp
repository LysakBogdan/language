#include "compiler.h"

void compile(Void, Code&);
void compile(const Pass&, Code&);

void compile(const Value&, Code&);
void compile(const string&, Code&);

void compile(const NValues&, Code&);
void compile(const Arguments&, Code&);
void compile(const Self&, Code&);
void compile(const Wildcard&, Code&);
void compile(const UpVar&, Code&);
void compile(const LocalVar&, Code&);
void compile(const GlobalVar&, Code&);
void compile(const Pin&, Code&);
void compile(const Contextual&, Code&);
void compile(const Assert&, Code&);
void compile(const Check&, Code&);
void compile(const GetReference&, Code&);

void compile(const Apply&, Code&);
void compile(const Action&, Code&);
void compile(const Decons&, Code&);
void compile(const Deconstruction&, Code&);

void compile(const Assign&, Code&);
void compile(const MultiAssign&, Code&);
void compile(const Match&, Code&);
void compile(const MatchList&, Code&);
void compile(const OpAssign&, Code&);
void compile(const PostAssign&, Code&);
void compile(const Swap&, Code&);
void compile(const And&, Code&);
void compile(const Or&, Code&);

void compile(const If&, Code&);
void compile(const While&, Code&);

void compile(const FunctionScope&, Code&);
void compile(const Lambda&, Code&);

void compile(const Sequence&, Code&);
void compile(const Cortege&, Code&);

void compile(const ArrayConstructor&, Code&);
void compile(const TableConstructor&, Code&);
void compile(const Map&, Code&);

void compile(const Break&, Code&);
void compile(const Return&, Code&);
void compile(const MatchReturn&, Code&);
void compile(const Continue&, Code&);

void compile(const Let&, Code&);
void compile(const LetList&, Code&);
void compile(const VarBlock&, Code&);

void compile(const Execution& e, Code& code)
{
	visit([&code](auto a) -> void { compile(a, code); }, *e);
}
