#include "compiler.h"

using enum TCommand;

void compile(const If& o, Code& code)
{
	auto index = code.index();
	compile(Check{o.condition}, code);

	auto else_jump = code.append(JumpFalse, index, 0);

	code.pop();
	compile(o.body, code);
	auto end_jump = code.append(Jump, 0);

	code[else_jump].b = code.pos();

	code.pop();
	compile(o.else_branch, code);
	code[end_jump].a = code.pos();
}

void compile(const While& o, Code& code)
{
	Code local(code);
	auto start = local.pos();
	auto index = code.index();
	compile(Check{o.condition}, local);

	auto exit_jump = local.append(JumpFalse, index, 0);

	Loop loop{start, index};
	local.loop = &loop;

	local.pop();
	compile(o.body, local);
	local.pop();

	local(Jump, start);

	local[exit_jump].b = local.pos();

	closeBreaks(local);

	local.push();

	code.take_index(local);
}
