#include "compiler.h"

#include "reference.h"

void compile_primitive(Code& code, TCommand command, const Execution& a)
{
	compile(a, code);
	code(command);
}

void compile_primitive(Code& code, TCommand command, const Execution& a, const Execution& b)
{
	compile(a, code);
	compile(b, code);
	code.binary(command);
}

void compile_primitive(Code& code, TCommand command, const Cortege& args)
{
	if(args.size() == 1)
		compile_primitive(code, command, args[0]);
	else if(args.size() == 2)
		compile_primitive(code, command, args[0], args[1]);
	else
		CAN_NOT_HAPPEN;
}

void compile(const Assign& o, Code& code)
{
	compile(o.b, code);
	code.dup();
	reference(code, o.a).write();
}

void compile_n(Code& code, const NValues& args, int n)
{
	compile(args, code);
	auto count = args.count;
	if(count < n)
		for(int i = count; i < n; i++)
			code(TCommand::Nil);
	else if(count > n)
		code.pop_n(count - n);
}

void compile(const MultiAssign& o, Code& code)
{
	auto value = code.index();
	auto size = o.a.size();

	compile_n(code, o.b, size);
	auto ref = image_list(code, o.a);
	write_list(code, o.a, ref, value);

	if(size > 1)
		code.pop_n(size - 1);
	else if(size == 0)
		code(TCommand::Nil);
}

void compile(const Match& o, Code& code)
{
	compile(match(o.a, o.b, code.locals_count), code);
}

void compile(const MatchList& o, Code& code)
{
	compile(match_list(o.a, o.b, code.var_space()), code);
}

void compile(const Swap& o, Code& code)
{
	auto left = reference(code, o.a);
	auto right = reference(code, o.b);

	right.read();
	code.dup();
	left.read();

	right.write();
	left.write();
}

void compile(const OpAssign& o, Code& code)
{
	auto arg = o.a.args[0];
	compile(o.a, code);
	code.dup();
	reference(code, arg).write();
}

void compile(const PostAssign& o, Code& code)
{
	auto arg = o.a.args[0];
	compile(arg, code);
	auto ref = reference(code, arg);
	compile(o.a, code);
	ref.write();
}

void logical_operator(Code& code, TCommand command, const Execution& a, const Execution& b)
{
	auto r = code.index();
	compile(a, code);
	auto jump = code.append(command, r, 0);
	code.pop();
	compile(b, code);
	code[jump].b = code.pos();
}

void compile(const And& o, Code& code)
{
	logical_operator(code, TCommand::JumpFalse, o.a, o.b);
}

void compile(const Or& o, Code& code)
{
	logical_operator(code, TCommand::JumpTrue, o.a, o.b);
}
