#include "compiler.h"

#include "reference.h"

void compile(const Cortege& o, Code& code)
{
	for(const auto& item : o)
		compile(item, code);
}

int image_list(Code& code, const Cortege& o)
{
	auto start = code.locals_count;
	for(const auto& item : o)
		image(code, item);
	return start;
}

void write_list(Code& code, const Cortege& o, int r, int value)
{
	for(const auto& e : o)
	{
		code(TCommand::Push, value++);
		write(code, e, r);
		r += ref_info_size(e);
	}
}

void read_list(Code& code, const Cortege& o, int r)
{
	for(const auto& e : o)
	{
		read(code, e, r);
		r += ref_info_size(e);
	}
}

void compile(const NValues& o, Code& code)
{
	compile(o.a, code);
	if(o.is_variadic)
		code.fixed_values(o.count);
	else
		code.set_count(o.count);
}

void compile(const Sequence& o, Code& code)
{
	compile(o.a, code);
	code.pop();
	compile(o.b, code);
}
