#include "compiler.h"

using enum TCommand;

Loop* find_loop(Code& code, int level)
{
	if(level == 1)
		return code.loop;

	NOT_IMPLEMENTED;
}

void compile(const Break& o, Code& code)
{
	auto loop = find_loop(code, o.level);
	if(!loop)
		throw CompileError{WrongBreak{}};

	compile(o.exp, code);
	code(Dip, loop->index);
	loop->breaks.push_back(code.append(Jump));
}

void compile(const Continue& o, Code& code)
{
	auto loop = find_loop(code, o.level);
	if(!loop)
		throw CompileError{WrongBreak{}};

	code(Jump, loop->start);
	code.push();
}

void compile(const MatchReturn& o, Code& code)
{
	compile(o.values, code);
	code(MatchRet, o.values.size());
}

void compile(const Return& o, Code& code)
{
	compile(o.exp, code);
	code(Ret);
}
