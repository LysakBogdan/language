#include "compiler.h"

#include "atoms.h"

using enum TCommand;

void write_at(Code& code, int ref)
{
	int value = code.index() - 1;
	code(GetLocal, ref);
	code(GetLocal, ref + 1);
	code(Push, value);
	code.set();
}

optional<TCommand> primitive_command(const Execution& a)
{
	if(auto value = a.as<Value>(); value)
		return PrimitiveCommand(*value);
	return nullopt;
}

void compile(const Apply& o, Code& code)
{
	if(auto cmd = primitive_command(o.obj); cmd)
	{
		compile_primitive(code, *cmd, o.args);
		return;
	}

	compile(o.obj, code);
	compile(o.args, code);
	code.apply(o.args.size());
}

void compile(const Action& o, Code& code)
{
	compile(Deconstruction{o.args, o.obj, o.args}, code);
}

void compile(const Decons& o, Code& code)
{
	compile(o.obj, code);
	compile(o.args, code);
	code.decons(o.args.size());
}

void compile(const Deconstruction& o, Code& code)
{
	compile(MatchList{o.items, NValues{Decons{o.obj, o.args}, int(o.items.size())}}, code);
}

int ref_info_size(const Apply& o)
{
	return 1 + o.args.size();
}

int image(Code& code, const Apply& o)
{
	auto var = code.alloc(ref_info_size(o));

	compile(o.obj, code);
	code.set_local(var);

	auto res = code.index();
	compile(o.args, code);
	for(int i = 0; i < int(o.args.size()); i++)
	{
		code(Push, res + i);
		code.set_local(var + i + 1);
	}
	code.pop_n(o.args.size());

	return var;
}

auto get_const(const Execution& a)
{
	auto value = a.as<Value>();
	if(!value)
		NOT_IMPLEMENTED;
	return *value;
}

Value setter_for(Value a)
{
	if(a == atom("get"))
		return atom("send");
	if(a == atom("size"))
		return atom("set_size");
	if(a.type == TValue::METHOD)
		return Value::Method(Symbol(setter(Symbol(a.id).str())));

	NOT_IMPLEMENTED;
}

void write(Code& code, const Apply& o, int ref)
{
	if(o.obj.as<Value>() == atom("at"))
	{
		write_at(code, ref + 1);
		return;
	}

	auto setter = code.addConstant(setter_for(get_const(o.obj)));

	auto value = code.index() - 1;
	code(GetConst, setter);
	code(GetLocal, ref + 1);
	code(Push, value);
	for(int i = 1; i < int(o.args.size()); i++)
		code(GetLocal, ref + 1 + i);
	code.apply(o.args.size() + 1);
	code.pop();
	code.pop();
}

void read(Code& code, const Apply& o, int ref)
{
	if(o.obj.as<Value>() == atom("at"))
	{
		code(Get);
		return;
	}

	auto getter = code.addConstant(get_const(o.obj));
	code(GetConst, getter);
	for(size_t i = 0; i < o.args.size(); i++)
		code(Push, ref + i);
	code.apply(o.args.size());
}
