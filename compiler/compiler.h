#ifndef COMPILER_H_INCLUDED
#define COMPILER_H_INCLUDED

#include "semantics.h"
#include "code.h"

void compile(const Execution&, Code&);

#endif
