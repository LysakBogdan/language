#include "compiler.h"

#include "reference.h"

using enum TCommand;

void push_nil(Code& code)
{
	return code(Nil);
}

int image(Code& code, const Void&)
{
	return empty_image(code);
}

void read(Code& code, Void, int)
{
	push_nil(code);
}

void write(Code& code, Void, int ref)
{
	code.pop();
}

int image(Code& code, const Wildcard&)
{
	return empty_image(code);
}

void write(Code& code, Wildcard, int ref)
{
	code.pop();
}

int image(Code& code, const Pin& o)
{
	auto var = code.alloc(1);
	compile(o.a, code);
	code.set_local(var);
	return var;
}

void write(Code& code, const Pin&, int ref)
{
	code(GetLocal, ref);
	code.binary(Eq);
	code.assert_true();
	code.pop();
}

void read(Code& code, const Pin&, int ref)
{
	code(GetLocal, ref);
}

void compile(Void, Code& code)
{
	push_nil(code);
}

void compile(const Pass&, Code& code)
{
	push_nil(code);
}

void compile(const Value& value, Code& code)
{
	code(GetConst, code.addConstant(value));
}

void compile(const string& s, Code& code)
{
	code(NewString, code.addConstant(make_value(s)));
}

void compile(const Pin& o, Code& code)
{
	compile(o.a, code);
}

void compile(const Contextual& o, Code& code)
{
	compile(GlobalVar{o.name}, code);
}

void compile(const Assert& o, Code& code)
{
	compile(o.a, code);
	code.assert_true();
}

void compile(const Check& o, Code& code)
{
	Code local(code);

	CheckBlock check {};
	local.check = &check;

	auto r = code.index();
	compile(o.a, local);

	closeCheckBlock(local, r + 1);

	code.take_index(local);
}

void compile(const GetReference& o, Code& code)
{
	if(!holds_alternative<LocalVar>(*o.a))
		NOT_IMPLEMENTED;
	compile(Value::Int(get<LocalVar>(*o.a).index), code);
}
