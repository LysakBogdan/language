#include "compiler.h"

#include "atoms.h"
#include "reference.h"
#include "tools.h"

void compile(const ArrayConstructor& o, Code& code)
{
	compile(o.items, code);
	code.new_array(o.items.size());
}

int image(Code& code, const ArrayConstructor& o)
{
	return image_list(code, o.items);
}

void write(Code& code, const ArrayConstructor& o, int ref)
{
	auto e = code.index() - 1;
	code.unpack();
	code.set_count(o.items.size());

	for(const auto& item : o.items)
	{
		code(TCommand::Push, e++);
		write(code, item, ref);
		ref += ref_info_size(item);
	}
	code.pop_n(o.items.size());
}

void read(Code& code, const ArrayConstructor& o, int ref)
{
	read_list(code, o.items, ref);
	code.new_array(o.items.size());
}

int ref_info_size(const ArrayConstructor& o)
{
	int s = 0;
	for(const auto& item : o.items)
		s += ref_info_size(item);
	return s;
}

Execution make_mappings(const vector<Lambda>& ms)
{
	if(ms.empty())
		return atom("fail");
	if(ms.size() == 1)
		return ms[0];

	return Apply{atom("fallback"), {ms[0], make_mappings(tail(ms))}};
}

void compile(const TableConstructor& o, Code& code)
{
	compile(Apply{atom("table"), {}}, code);
	for(auto [key, value] : o.pairs)
	{
		compile(key, code);
		compile(value, code);
		code.add_pair();
	}
}

void compile(const Map& o, Code& code)
{
	compile(make_mappings(o.mappings), code);
}

int image(Code& code, const TableConstructor& o)
{
	auto r = empty_image(code);
	for(const auto& [key, value] : o.pairs)
		image(code, value);
	return r;
}

int ref_info_size(const TableConstructor& o)
{
	int s = 0;
	for(const auto& [key, value] : o.pairs)
		s += ref_info_size(value);
	return s;
}

void write(Code& code, const TableConstructor& o, int ref)
{
	for(const auto& [key, value] : o.pairs)
	{
		code.dup();
		compile(key, code);
		code.binary(TCommand::Get);
		write(code, value, ref);
		ref += ref_info_size(value);
	}
	code.pop();
}
