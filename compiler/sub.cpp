#include "compiler.h"

void compile(const FunctionScope& o, Code& code)
{
	compile(o.body, code);
	code(TCommand::Ret);
	code.proto.captures = o.captures;
}

optional<Symbol> contextual(Execution a)
{
	if(holds_alternative<Contextual>(*a))
		return get<Contextual>(*a).name;
	return nullopt;
}

void compile(const Let& o, Code& code)
{
	if(auto name = contextual(o.pattern); name)
	{
		auto var = GlobalVar{*name};

		auto space = code.var_space(1);
		auto saved = space[0];

		auto wrapped = in_block(space, sequence
		({
			Assign{saved, var},
			Let{var, o.value, o.body},
			Assign{var, saved}
		}));
		compile(wrapped, code);
		return;
	}

	compile(Match{o.pattern, o.value}, code);
	code.pop();
	compile(o.body, code);
}

void compile(const LetList& o, Code& code)
{
	compile(MatchList{o.pattern, o.value}, code);
	code.pop();
	compile(o.body, code);
}

void compile(const VarBlock& o, Code& code)
{
	if(o.count == 0)
		return compile(o.body, code);

	auto begin = code.alloc(o.count);
	compile(o.body, code);
	code(TCommand::LocalsCount, begin);
}
