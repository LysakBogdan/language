#ifndef REFERENCE_H_INCLUDED
#define REFERENCE_H_INCLUDED

#include "code.h"

template<class T>
int ref_info_size(T)
{
	return 0;
}

template<class T>
int image(Code& code, const T& o)
{
	throw CompileError{AssignToNotReference{}};
}

int empty_image(Code&);

template<class T>
void read(Code&, T, int ref) { NOT_IMPLEMENTED; }

template<class T>
void write(Code&, T, int ref) { NOT_IMPLEMENTED; }

void write(Code&, Void, int ref);
void read(Code&, Void, int ref);

int image(Code&, const Wildcard&);
void write(Code&, Wildcard, int ref);

int image(Code&, Value);
void write(Code&, Value, int);
void read(Code&, Value, int);
void write(Code&, const LocalVar&, int ref);
void read(Code&, const LocalVar&, int ref);
int image(Code&, const LocalVar&);
void write(Code&, const GlobalVar&, int);
void read(Code&, const GlobalVar&, int);
int image(Code&, const GlobalVar&);

void write(Code&, const Pin&, int);
void read(Code&, const Pin&, int);
int image(Code&, const Pin&);

int ref_info_size(const Apply&);
int image(Code&, const Apply&);
void write(Code&, const Apply&, int ref);
void read(Code&, const Apply&, int ref);

int image_list(Code&, const Cortege&);
void read_list(Code&, const Cortege&, int ref);
void write_list(Code& code, const Cortege& o, int r, int value);

int image(Code&, const ArrayConstructor&);
void write(Code&, const ArrayConstructor&, int ref);
void read(Code&, const ArrayConstructor&, int ref);
int ref_info_size(const ArrayConstructor&);

int ref_info_size(const TableConstructor&);
int image(Code&, const TableConstructor&);
void write(Code&, const TableConstructor&, int ref);

int ref_info_size(const Execution&);
int image(Code&, const Execution&);

void write(Code&, const Execution&, int ref);
void read(Code&, const Execution&, int ref);

struct Reference
{
	Code& code;
	const Execution& o;
	int ref;

	void read();
	void write();
};

Reference reference(Code&, const Execution&);

#endif
