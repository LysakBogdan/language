#include "compiler.h"

#include "reference.h"

using enum TCommand;

void compile(const Arguments& o, Code& code)
{
}

void compile(const Self&, Code& code)
{
	code(GetSelf);
}

void compile(const Wildcard&, Code& code)
{
	compile(Value::Unit, code);
}

void compile(const UpVar& o, Code& code)
{
	code(GetUpvalue, o.index);
}

void compile(const LocalVar& o, Code& code)
{
	code(GetLocal, o.index);
}

void compile(const GlobalVar& o, Code& code)
{
	code(GetGlobal, code.addConstant(value(o.name)));
}

int image(Code& code, Value x)
{
	return empty_image(code);
}

void write(Code& code, Value a, int ref)
{
	compile(a, code);
	code.binary(Eq);
	code.assert_true();
	code.pop();
}

void read(Code& code, Value x, int)
{
	compile(x, code);
}

int image(Code& code, const LocalVar&)
{
	return empty_image(code);
}

int image(Code& code, const GlobalVar&)
{
	return empty_image(code);
}

void write(Code& code, const GlobalVar& o, int ref)
{
	code.set_global(o.name);
}

void read(Code& code, const GlobalVar& o, int)
{
	code(GetGlobal, code.addConstant(value(o.name)));
}

void write(Code&code, const LocalVar& o, int ref)
{
	code.set_local(o.index);
}

void read(Code& code, const LocalVar& o, int)
{
	code(GetLocal, o.index);
}
