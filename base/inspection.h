#ifndef INSPECTION_H_INCLUDED
#define INSPECTION_H_INCLUDED

#include <sstream>
#include <iterator>
#include <vector>

template<class T, class S>
ostream& write_separated_by(ostream& os, const vector<T> &a, const S& separator)
{
	if(a.empty())
		return os;
	copy(a.cbegin(), a.cend() - 1, ostream_iterator<T>(os, separator));
	return os << a.back();
}

template<class T>
ostream& operator<<(ostream& os, const vector<T> &a)
{
	return write_separated_by(os, a, ", ");
}

template<class T>
string inspect(const T& x)
{
	stringstream ss;
	ss << x;
	return ss.str();
}

inline string inspect(const string& o)
{
	return "\"" + o + "\"";
}

template<class T>
string LISP(const char* name, const T& a)
{
	return format("({} {})", name, inspect(a));
}

template<class T, class U>
string LISP(const char* name, const T& a, const U& b)
{
	return format("({} {} {})", name, inspect(a), inspect(b));
}

template<class T, class U, class V>
string LISP(const char* name, const T& a, const U& b, const V& c)
{
	return format("({} {} {} {})", name, inspect(a), inspect(b), inspect(c));
}

#endif
