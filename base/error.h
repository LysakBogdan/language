#ifndef ERROR_H_INCLUDED
#define ERROR_H_INCLUDED

#include <variant>
#include <assert.h>

#include "base.h"
#include "runtime/value.h"

struct CanNotBeCalled { Value value; };
struct UnknownMethod { std::string method; Value object; };
struct OutOfRange {int index, size;};
struct MatchFail {};
struct TypeError { std::string expected_type; Value value; };
struct ExpectNArguments { int expected, got; };
struct MissingArgument { int i; };

struct RuntimeError
{
    std::variant<CanNotBeCalled, UnknownMethod, OutOfRange,
    MatchFail, TypeError, ExpectNArguments, MissingArgument> reason;
};

struct AssignToNotReference {};
struct WrongBreak {};

struct CompileError
{
	std::variant<AssignToNotReference, WrongBreak> reason;
};

std::runtime_error DevError(const char* message, const char* file, int line);

#define NOT_IMPLEMENTED throw DevError("Not implemented yet ", __FILE__, __LINE__)
#define CAN_NOT_HAPPEN throw DevError("Can not happen ", __FILE__, __LINE__)

template<class Map, class Key>
auto sure_at(const Map& map, const Key& key)
{
	auto it = map.find(key);
	assert(it != map.end());
	return it->second;
}

#endif
