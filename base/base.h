#ifndef BASE_H_INCLUDED
#define BASE_H_INCLUDED

#include <fstream>
#include <string>
#include <optional>

#ifdef DEBUG
	extern std::optional<std::ofstream> Log;
	#define LOG_LINE(x) if(Log) *Log << x << endl
	#define LOG(x) LOG_LINE(#x << " = " << x)
	#define LOG_FILE(path) Log.emplace(path);
#else
	#define LOG_LINE(x)
	#define LOG(x)
	#define LOG_FILE(path)
#endif

void log_indentation(int n);

std::string load_file(const char*);

#endif
