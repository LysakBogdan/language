#ifndef TOOLS_H_INCLUDED
#define TOOLS_H_INCLUDED

#include <algorithm>
#include <optional>
#include <functional>

template<class T, class U>
std::optional<int> find_index(const T& xs, const U& x)
{
	auto i = std::ranges::find(xs, x);
	if(i == end(xs))
		return std::nullopt;
	return i - begin(xs);
}

template<class T, class U>
int add_unique(T& xs, const U& x)
{
	auto i = find_index(xs, x);
	if(i)
		return *i;
	int index = xs.size();
	xs.push_back(x);
	return index;
}

template<class T>
auto cons(T first, std::vector<T> rest)
{
	auto a = std::vector<T>{first};
	a.insert(a.end(), rest.begin(), rest.end());
	return a;
}

template<class T>
auto tail(const std::vector<T>& a)
{
	return vector<T>(a.begin() + 1, a.end());
}

template<class T>
class storage
{
    std::function<T*()> get;

    template<class X>
    struct small
    {
        X x;
        T* operator()() { return &x; }
    };

public:
	struct pointer
	{
		T* ptr;
		T* operator()() { return ptr; }
	};

	storage(pointer p): get(p) {}
    template<class X>
    storage(X x): get(small<X>{x}) {}

    T* operator()() { return get(); }
    const T* operator()() const { return get(); }
};

#endif
