#include "base.h"

#include <stdexcept>
#include <sstream>

using namespace std;

#ifdef DEBUG
optional<ofstream> Log;

void log_indentation(int n)
{
	if(Log)
		*Log << string(n, '\t');
}
#endif

string load_file(const char* path)
{
	ifstream file(path);
	if(!file.is_open())
		throw runtime_error(string("Unable to open file: ") + path);
	stringstream s;
	s << file.rdbuf();
	return s.str();
}
