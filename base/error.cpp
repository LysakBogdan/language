#include "error.h"

#include <sstream>

using namespace std;

runtime_error DevError(const char* message, const char* file, int line)
{
	stringstream ss;
	ss << message << file << ":" << line;
	return runtime_error(ss.str());
}
