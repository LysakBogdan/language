#include "table.h"

#include "memory.h"
#include "error.h"
#include "formatting.h"

#include <sstream>

Table::Table(int n, Value* pairs)
{
	for(int i = 0; i < n;)
	{
		auto key = pairs[i++];
		auto value = pairs[i++];
		table.insert({key, value});
	}
}

void Table::mark(GC &gc) const
{
	for(const auto& [key, value] : table)
	{
		gc.mark(key);
		gc.mark(value);
	}
}

void Table::inspect(Printer& print) const
{
	print << "{";
	print_pairs(print, table);
	print << "}";
}

void Table::add_pair(const Value& key, const Value& value)
{
	table.insert({key, value});
}

void MutableTable::set(const Value& key, const Value& value)
{
	if(value.type == TValue::NIL)
		table.erase(key);
	else
		table[key] = value;
}
