#ifndef TABLE_H
#define TABLE_H

#include "object.h"
#include <map>

class Table: public Object
{
public:
	using Dictionary = std::map<Value, Value>;
	Dictionary table;
	Table() = default;
	Table(const Dictionary& values): table(values){}
	Table(Dictionary::const_iterator first, Dictionary::const_iterator last): table(first, last){}
	Table(int n, Value* pairs);

	static constexpr auto type_name = "table";
	void mark(GC &gc) const override;
	void inspect(Printer&) const override;
	Type& type() const override;

	void add_pair(const Value& key, const Value& value);
};

class MutableTable: public Table
{
public:
	MutableTable() = default;
	MutableTable(const Dictionary& values): Table(values){}
	MutableTable(Dictionary::const_iterator first, Dictionary::const_iterator last): Table(first, last){}
	MutableTable(int n, Value* pairs): Table(n, pairs){};

	static constexpr auto type_name = "dictionary";
	Type& type() const override;

	void set(const Value& key, const Value& value);
};

#endif
