#include "array.h"

#include "memory.h"
#include "error.h"
#include "sequence.h"
#include "inspection.h"
#include "formatting.h"

Array::Array(const RAFS::rafs& fs)
{
	values.reserve(fs.size());
	for(int i = 0; i < fs.size(); i++)
		values.push_back(fs[i]);
}

Array::Array(Context& c, Value seq)
{
	auto [e, rest] = Atoms::decons_h(c, seq);
	while(*e != Value::Nil)
	{
		values.push_back(e);
		tie(e, rest) = Atoms::decons(c, rest);
	}
}

void Array::mark(GC &gc) const
{
	for(auto e : values)
		gc.mark(e);
}

void Array::inspect(Printer& print) const
{
	print << "[";
	int i = 0;
	for(auto value : values)
		print << (i++ ? ", " : "") << value;
	print << "]";
}

vector<Value>& as_array(Value a)
{
	return as_object<Array>(a)->values;
}
