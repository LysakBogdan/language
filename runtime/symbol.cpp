#include "symbol.h"

using namespace std;

unsigned Symbol::next_id = 1;
map<string, unsigned> Symbol::name_to_id;
map<unsigned, string> Symbol::id_to_name;
string Symbol::empty_string = "";

void Symbol::register_id(unsigned id, const string& name)
{
	name_to_id.insert(make_pair(name, id));
	id_to_name.insert(make_pair(id, name));
}

Symbol::Symbol(const string& name)
{
	auto i = name_to_id.find(name);
	if(i == name_to_id.end())
		register_id((_id = next_id++), name);
	else
		_id = i->second;
}

Symbol Symbol::unique()
{
	auto id = next_id++;
	auto name = "#" + to_string(next_id);
	register_id(id, name);

	return Symbol(id);
}

ostream& operator<<(ostream &os, Symbol symbol)
{
	return os << '$' << symbol.str();
}

string setter(const string& s)
{
	return "set_" + s;
}
