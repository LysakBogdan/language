#ifndef TYPE_H_INCLUDED
#define TYPE_H_INCLUDED

#include "value.h"
#include "object.h"
#include "context.h"
#include "module.h"
#include <set>
#include <vector>

class Type: public Object
{
	typedef map<Symbol, Value> Methods;
	Methods methods;
	set<Symbol> method_names;
public:
	Value constructor;
	template<class ...Ts>
	Type(Value aConstructor, const Ts& ...traits): constructor(aConstructor)
	{
		(include(traits), ...);
	}

	template<class ...Ts>
	Type(const Ts& ...traits): Type(Value::Nil, traits...){}

	Value getMethod(Symbol) const;
	void setMethod(Symbol, Value);

	static constexpr auto type_name = "type";
	void mark(GC &gc) const override;
	void inspect(Printer&) const override;
	Type& type() const override;

	void include(const Library&);

	static Type& get_type();
};

class LangType: public Type
{
	void mark(GC &gc) const override;
};

template<class T>
T* as_object(Value a)
{
	if(a.is_object())
		if(T* o = a.as<T>(); o)
			return o;
	throw RuntimeError{TypeError{T::type_name, a}};
}

#endif
