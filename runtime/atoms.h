#ifndef ATOMS_H_INCLUDED
#define ATOMS_H_INCLUDED

#include <optional>
#include "type.h"
#include "module.h"

namespace Atoms
{
	void Init(Module&, VM&);
	optional<string> name(Value);
}

Value atom(const char* name);

#endif
