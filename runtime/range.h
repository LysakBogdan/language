#ifndef RANGE_H
#define RANGE_H

#include "type.h"
#include "rafs.h"

struct Range: Object, IRAFS
{
	int a, b;

	Range(int aA, int aB): a(aA), b(aB) {}

	static constexpr auto type_name = "range";
	void inspect(Printer&) const override;
	Type& type() const override;

	int size() const override
	{
		return b - a + 1;
	}
	Value element(int i) const override
	{
		return Value::Int(a + i);
	}

	bool empty() const;
};

struct Iota: Object
{
	int a;

	Iota(int aA = 0): a(aA) {}

	static constexpr auto type_name = "iota";
	void inspect(Printer&) const override;
	Type& type() const override;

	bool empty() const
	{
		return false;
	}

	static Type Interface;
};

#endif
