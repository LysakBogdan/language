#ifndef SYMBOL_H
#define SYMBOL_H

#include "base/base.h"
#include <map>

class Symbol
{
	unsigned _id = 0;
	static std::map<std::string, unsigned> name_to_id;
	static std::map<unsigned, std::string> id_to_name;
	static std::string empty_string;
	static unsigned next_id;

	static void register_id(unsigned, const std::string&);
public:
	Symbol() = default;
	explicit Symbol(unsigned aId): _id(aId) {};
	explicit Symbol(const std::string& name);

	const std::string& str() const {return _id ? id_to_name[_id] : empty_string;}
	std::string inspect() const {return str();}
	unsigned id() const {return _id;}

	bool operator==(Symbol s) const {return _id == s._id;}
	bool operator<(Symbol s) const {return _id < s._id;}
	friend std::ostream& operator<<(std::ostream &os, Symbol symbol);

	static Symbol unique();
};

std::string setter(const std::string&);

#endif
