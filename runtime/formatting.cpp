#include "formatting.h"

#include "inspection.h"
#include "object.h"
#include "lexer.h"
#include "atoms.h"

ostream& operator<<(ostream& os, const Value& a)
{
	return os << inspect(a);
}

string inspect(const Value& x)
{
	stringstream ss;
	switch(x.type)
	{
		using enum TValue;
		case NIL:
			ss << "nil";
			break;
		case UNIT:
			ss << "_";
			break;
		case BOOL:
			ss << (x._bool ? "yes" : "no");
			break;
		case NUMBER:
			ss << x.number;
			break;
		case SYMBOL:
			ss << "'" << Symbol(x.id).str() << "'";
			break;
		case FUNCTION:
		case OBJECT:
			ss << (Printer{} << x.object).str();
			break;
		case CFUNCTION:
		case CPATTERN:
			ss << "@" << Atoms::name(x).value_or("???");
			break;
		case METHOD:
			ss << "_." << Symbol(x.id).str();
			break;
		default:
			NOT_IMPLEMENTED;
	}
	return ss.str();
}

string inspect(TValue type)
{
	// TODO: list?
	switch(type)
	{
		using enum TValue;
		case NIL:
			return "nil";
		case BOOL:
			return "bool";
		case NUMBER:
			return "number";
		case SYMBOL:
			return "symbol";
		case OBJECT:
			return "object";
		case FUNCTION:
			return "function";
		case METHOD:
			return "method";
		default:
			CAN_NOT_HAPPEN;
	}
}

string inspect(const CanNotBeCalled& e)
{
	return format("Can not be called: {}", e.value);
}

string inspect(const UnknownMethod& e)
{
	return format("Unknown method '{}' for object {}", e.method, inspect(e.object));
}

string inspect(const OutOfRange& e)
{
	return format("Out of range, index = {}, size = {}", e.index, e.size);
}

string inspect(const MatchFail&)
{
	return "Fail";
}

string inspect(const TypeError& e)
{
	return format("Expected type: {}, got: {}", e.expected_type, e.value);
}

string inspect(const ExpectNArguments& e)
{
	return format("Function expects {} arguments, got {}", e.expected, e.got);
}

string inspect(const MissingArgument& e)
{
	return format("Missing argument #{}", e.i + 1);
}

string inspect(const RuntimeError& e)
{
	return visit([](auto a) -> string { return inspect(a); }, e.reason);
}

string inspect(const AssignToNotReference&)
{
	return "Assign to not reference";
}

string inspect(const WrongBreak&)
{
	return "Wrong break";
}

string inspect(const CompileError& e)
{
	return visit([](auto a) -> string { return inspect(a); }, e.reason);
}

string inspect(const IdIsExpected&)
{
	return "id is expected";
}

string inspect(const WrongBlock&)
{
	return "wrong block";
}

template<class T>
string inspect(const Unexpected<T>& e)
{
	return format("expected: {}, got: {}", inspect(Lexeme(e.expected)), inspect(e.lx));
}

string inspect(const UnexpectedLexeme& e)
{
	return format("unexpected {}", inspect(e.lx));
}

string inspect(const UnexpectedIndent&)
{
	return "unexpected indent";
}

string inspect(const string& source, const ParsingError& e)
{
	auto location = get_location(source, e.view);
	auto reason = visit([](auto a) -> string { return inspect(a); }, e.reason);
	return format("{}: syntax error, {}", location.line, reason);
}

string InspectPointer(const char* type_name, const void* pointer)
{
	return format("[{}]", pointer);
}

Printer& Printer::operator<<(Value a)
{
	if(a.type == TValue::OBJECT)
		*this << a.object;
	else
		ss << inspect(a);
	return *this;
}

Printer& Printer::operator<<(Object* o)
{
	if(seen.contains(o))
		return *this << "(...)";

	seen.insert(o);
	o->inspect(*this);
	seen.erase(o);
	return *this;
}
