#include "module.h"

Value Module::operator[](Symbol name) const
{
	auto i = items.find(name);
	if(i != items.end())
		return i->second;

	return Value::Nil;
}

void Module::put(Symbol name, Value value)
{
	items[name] = value;
}

void Module::include(const Library& lib)
{
	for(auto [name, method] : lib)
		put(name, method);
}

void Module::mark(GC& gc) const
{
	for(auto [name, value] : items)
		gc.mark(value);
}
