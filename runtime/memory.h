#ifndef MEMORY_H_INCLUDED
#define MEMORY_H_INCLUDED

#include "standalone/config.h"
#include "value.h"
#include <vector>
#include <set>
#include <cstddef>
#include <atomic>

class GC
{
public:
	struct Object
	{
		mutable bool marked = false;
		virtual void mark(GC&) const {};
		virtual ~Object() = default;
	};

	void add(const Object*);
	void mark(const Object* obj);
	void mark(const Value& x);
	void collect();
	int manual_collect();
	bool need_collect() const;
	~GC();

	class Lock
	{
		GC& gc;
	public:
		// TODO: make sure there is no way a memory leak can happen
		explicit Lock(GC& aGC): gc(aGC) { ++gc.locks; };
		~Lock(){ --gc.locks; };
	};

	class Handle
	{
		GC& gc;
		Value o;
	public:
		Value get() const { return o; }
		Value operator*() const { return o; }
		const Value* operator->() const { return &o; }
		operator Value() const { return o; }
		void operator=(Value a)
		{
			gc.unlock(o);
			o = a;
			gc.lock(o);
		}
		Handle(Handle&& a): gc(a.gc), o(a.o)
		{
			a.o = Value::Nil;
		}
		Handle(const Handle&) = delete;
		Handle(GC& aGC, Value a): gc(aGC), o(a)
		{
			gc.lock(o);
		}
		~Handle()
		{
			gc.unlock(o);
		}
	};

	template<class T>
	class Ptr
	{
		Handle _value;
	public:
		T* get() const { return static_cast<T*>(_value->object); }
		T& operator*() const { return *get(); }
		T* operator->() const { return get(); }
		Value value() const { return _value; }
		operator Value() const { return _value; }
		Ptr(Ptr&& p): _value(std::move(p._value)){}
		Ptr(GC& aGC, T* a): _value(aGC, make_value(a)){}
	};
	template<class T, class ...Args>
	Ptr<T> create(Args&&... args)
	{
		auto o = new T(args...);
		add(o);
#if TRACE_GC
		LOG_LINE("	" << o->inspect());
#endif
		return Ptr(*this, o);
	}
private:
	std::vector<const Object*> objects;
	std::vector<const Object*> roots;
	std::multiset<const Object*> locked;

	int allocations = 0;
	int collected = 0;

	std::atomic<int> locks = 0;

	void lock(Value a);
	void unlock(Value a);

	void sweep();
	void mark_roots();
};

#endif
