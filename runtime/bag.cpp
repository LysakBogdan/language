#include "bag.h"

#include "vm.h"
#include "inspection.h"
#include "sequence.h"
#include <algorithm>

bool Bag::pick(Value a)
{
	auto i = ranges::find(values, a);
	if(i == values.end())
		return false;

	swap(*i, values.back());
	values.pop_back();
	return true;
}

Value Bag::pick(Context& c, Value a) const
{
	auto copy = create<Bag>(c, values);
	if(copy->pick(a))
		return copy.value();
	return Value::Nil;
}

int Bag::decons(Context& c) const
{
	if(values.empty())
		return c.match_fail();

	auto rest = values;
	auto a = rest.back();
	rest.pop_back();

	return c.results(a, create<Bag>(c, rest));
}

bool bag_equals(Context& c, Value x, Value y)
{
	auto b = c.keep(y);

	for(Iterator e(c, x); e; e.next())
	{
		b = as_object<Pickable>(b)->pick(c, *e);
		if(*b == Value::Nil)
			return false;
	}
	return Atoms::empty_sequence(c, b);
}

void Bag::mark(GC& gc) const
{
	for(auto value : values)
		gc.mark(value);
}

void Bag::inspect(Printer& p) const
{
	stringstream ss;
	ss << "Bag(" << values << ")";
	p << ss.str();
}

Value bag_equals(Context& c)
{
	return Value::Bool(bag_equals(c, c[0], c[1]));
}

namespace IBag
{
Value pick(Context& c)
{
	auto bag = as_object<Bag>(c[0]);
	return bag->pick(c, c[1]);
}

int decons(Context& c, Value a)
{
	auto bag = as_object<Bag>(a);
	return bag->decons(c);
}

Library methods =
{
	{"pick", value(pick)},
	{"decons", value(decons)},
	{"equals", value(bag_equals)}
};
}

Type& Bag::type() const
{
	static Type interface(IBag::methods);
	return interface;
}
