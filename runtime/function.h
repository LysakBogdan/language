#ifndef FUNCTION_H
#define FUNCTION_H

#include "vm/command.h"
#include "blob.h"
#include "semantics/ast.h"

struct FuncProto: public Object
{
	FunctionScope exe;
	vector<Value> constants;
	vector<string> args, locals;
	vector<Capture> captures;
	vector<Command> code;

	explicit FuncProto(const FunctionScope& execution): exe(execution){}

	friend ostream& operator<<(ostream&, const FuncProto&);

	void mark(GC &gc) const override
	{
		for(const auto& c : constants)
			gc.mark(c);
	}

	int addConstant(Value value);
	void inspect(Printer& p) const override
	{
		p << "FuncProto";
	}
};

class Function: public Object
{
	FuncProto &proto;
	vector<Value> captures;
public:
	Function(FuncProto& aProto, Function* parent = nullptr, Value* locals = nullptr);
	~Function();

	FuncProto& getProto()
	{
		return proto;
	}

	Value get_capture(int var) const
	{
		return captures[var];
	}

	static constexpr auto type_name = "function";
	void mark(GC &gc) const override
	{
		gc.mark(&proto);
		for(const auto& c : captures)
			gc.mark(c);
	}

	void inspect(Printer& p) const override;
};

optional<pair<Value, Value>> single_pair(Context& c, Value a);

#endif
