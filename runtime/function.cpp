#include "function.h"

#include "thread.h"
#include "execution_inspection.h"
#include "formatting.h"
#include "inspection.h"
#include <sstream>
#include <algorithm>

int FuncProto::addConstant(Value value)
{
	auto i = ranges::find(constants, value);
	if(i != constants.end())
		return i - constants.begin();
	else
	{
		auto res = constants.size();
		constants.push_back(value);
		return res;
	}
}

Value capture_value(Function* parent, Value* locals, Capture c)
{
	if(holds_alternative<LocalVar>(c))
		return locals[get<LocalVar>(c).index];
	else
		return parent->get_capture(get<UpVar>(c).index);
}

Function::Function(FuncProto& aProto, Function* parent, Value* locals): proto(aProto)
{
	transform(begin(proto.captures), end(proto.captures), back_inserter(captures), [=](auto c)
	{
		return capture_value(parent, locals, c);
	});
}

Function::~Function()
{
	LOG_LINE("Function was deleted: " << this);
}

ostream& operator<<(ostream& os, const FuncProto& o)
{
	auto index = 0;
	for(const auto& c : o.code)
		print_command(os << index++ << "\t", c, o.constants) << "\n";
	return os;
}

void Function::inspect(Printer& p) const
{
	p << inspect_pointer(this);
}

optional<Value> single_key(Execution func_body)
{
	if(!holds_alternative<LetList>(*func_body))
		return nullopt;

	auto let = get<LetList>(*func_body);
	if(let.pattern.size() != 1)
		return nullopt;
	auto key = let.pattern[0];
	if(!holds_alternative<Value>(*key))
		return nullopt;
	return get<Value>(*key);
}

optional<pair<Value, Value>> single_pair(Context& c, Value a)
{
	if(a.type != TValue::FUNCTION)
		return nullopt;

	auto func = a.as<Function>();
	if(auto key = single_key(func->getProto().exe.body.body); key)
	{
		auto value = c.thread.apply({a, *key});
		return pair<Value, Value>(*key, value);
	}
	return nullopt;
}
