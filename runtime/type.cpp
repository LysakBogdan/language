#include "type.h"

#include "memory.h"
#include "formatting.h"
#include "itype.h"
#include <sstream>

void Type::include(const Library& lib)
{
	for(auto [name, func] : lib)
		if(!methods.contains(Symbol(name)))
		{
			methods[Symbol(name)] = func;
			method_names.insert(Symbol(name));
		}
}

void Type::inspect(Printer& p) const
{
	p << "Type(";
	print_pairs(p, methods);
	p << ")";
}

Value Type::getMethod(Symbol name) const
{
	auto i = methods.find(name);
	if(i != methods.end())
		return i->second;
	return Value::Nil;
}

void Type::setMethod(Symbol name, Value val)
{
	if(method_names.contains(name))
		return;

	if(val.type != TValue::NIL)
		methods[name] = val;
	else
		methods.erase(name);
};

void Type::mark(GC &gc) const
{
	gc.mark(constructor);
	for(const auto& [name, method] : methods)
		gc.mark(method);
}

void LangType::mark(GC &gc) const
{
	Type::mark(gc);
}

Type& Type::type() const
{
	return get_type();
}

Type& Type::get_type()
{
	static Type interface(value(IType::constructor), IType::methods);
	return interface;
}
