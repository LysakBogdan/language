#ifndef VALUE_H_INCLUDED
#define VALUE_H_INCLUDED

#include "lexer/lexeme.h"
#include "symbol.h"

class Context;
class VM;
struct Value;

using CFunction = Value (*)(Context&);
using CPattern = int (*)(Context&, Value);

enum class TValue {NIL, UNIT, BOOL, NUMBER, SYMBOL, OBJECT, FUNCTION, CFUNCTION, CPATTERN, METHOD};

struct Object;
class Function;
class Type;

struct Value
{
	TValue type;
	union
	{
		Object* object;
		bool _bool;
		unsigned id;
		double number;
		CFunction cfunction;
		CPattern cpattern;
	};

	using enum TValue;

	template<class T>
	T* as() const
	{
		return dynamic_cast<T*>(object);
	}

	explicit operator bool() const
	{
		return type != NIL;
	};
	bool operator==(const Value& x) const;
	bool operator!=(const Value& x) const
	{
		return !operator==(x);
	}
	bool operator<(const Value& x) const;
	bool is_object() const;
	template<class T>
	bool is_object() const
	{
		return is_object() && as<T>();
	}
	Type& getType(VM&) const;

	static Value Int(int64_t);
	static Value Double(double);
	static Value Bool(bool);
	static Value YesNo(bool);

	static Value Method(Symbol);

	static const Value Nil;
	static const Value Unit;
};

Value value(const Symbol& sym);
Value value(CFunction);
Value value(CPattern);

Value make_value(Value);
Value make_value(bool);
Value make_value(int);
Value make_value(double);
Value make_value(const std::string&);
Value make_value(Function*);
Value make_value(Object*);

double as_number(Value);
int as_int(Value);
const std::string& as_string(Value);
bool is_string(Value);
std::string to_string(Value);
Symbol as_symbol(Value);
Function* as_function(Value);
Value parse_number(const std::string&);

template<class T>
auto cast(Value a)
{
	return a.as<T>();
}

template<>
inline auto cast<int>(Value a)
{
	return as_int(a);
}

template<>
inline auto cast<Value>(Value a)
{
	return a;
}

template<>
inline auto cast<std::string>(Value a)
{
	return as_string(a);
}

inline auto operator "" _m(const char* name, size_t size){ return Value::Method(Symbol(name)); }

#endif
