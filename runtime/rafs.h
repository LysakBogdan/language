#ifndef RAFS_H_INCLUDED
#define RAFS_H_INCLUDED

#include "type.h"
#include "base/tools.h"
#include "module.h"

// Random Access Finite Sequence
class IRAFS
{
public:
	virtual int size() const = 0;
	virtual Value element(int i) const = 0;

	bool empty() const { return size() == 0; }
};

namespace RAFS
{
	struct rafs
	{
		storage<const IRAFS> box;
		const IRAFS* object;

		rafs(const IRAFS* r): box(storage<const IRAFS>::pointer{r}), object(r) {};
		template<class T>
		rafs(T x): box(x), object(box()) {};

		rafs(const rafs& a): box(a.box), object(box()) {};
		void operator=(const rafs&) = delete;

		int size() const { return object->size(); };
		Value operator[](int i) const { return object->element(i); };

		bool empty() const { return object->empty(); }
	};

	rafs RAFS(Value a);
	bool is_RAFS(Value a);
	inline rafs RAFS(const IRAFS& r) { return rafs(&r); };
	bool equals(const rafs&, const rafs&);

	extern Library methods;
}

int clamp(int a, int min, int max);

struct Slice: Object, IRAFS
{
	Value x;
	int start, stop;

	Slice(Value aX, int aStart, int aStop): x(aX)
	{
		auto n = RAFS::RAFS(x).size();
		start = clamp(aStart, 0, n);
		stop = clamp(aStop, 0, n);
	}

	Slice(Value aX, int aStart): x(aX)
	{
		auto n = RAFS::RAFS(x).size();
		start = clamp(aStart, 0, n);
		stop = n;
	}

	static constexpr auto type_name = "slice";
	void mark(GC &gc) const override;
	void inspect(Printer&) const override;
	Type& type() const override;

	int size() const override
	{
		return stop - start;
	}

	Value element(int i) const override
	{
		return RAFS::RAFS(x)[start + i];
	}
};

struct SliceView: Slice
{
	SliceView(Value aX, int aStart, int aStop): Slice(aX, aStart, aStop) {}

	static constexpr auto type_name = "slice iterator";
	void inspect(Printer&) const override;
	Type& type() const override;
};

int decons_pair(Context&, const SliceView&);
int decons(Context&, const SliceView&);
Value slice(Context&);

template<>
inline auto cast<RAFS::rafs>(Value a)
{
	return RAFS::RAFS(a);
}

#endif
