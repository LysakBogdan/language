#ifndef ATOM_H_INCLUDED
#define ATOM_H_INCLUDED

#include "vm.h"

bool init_apply(Context&);
bool callValue(Value, Context&);

template<class T, class... Ps, size_t... ns>
CFunction contructor(index_sequence<ns...>)
{
	return [](Context& c) -> Value
	{
		if((... || (c[ns].type == TValue::NIL)))
			return c.fail();
		return create<T>(c, cast<Ps>(c[ns])...);
	};
}

template<class T, class... Ps>
CFunction contructor()
{
	return contructor<T, Ps...>(make_index_sequence<sizeof...(Ps)>());
}

#endif
