#ifndef BLOB_H
#define BLOB_H

#include "object.h"
#include "rafs.h"
#include <string>

class Blob: public Object, public IRAFS
{
	string data;
public:
	Blob() {};
	Blob(const string& s): data(s) {};

	char& operator[](int index) {return data[index];}
	bool empty() const {return data.empty();}
	void push_back(char c) {data.push_back(c);}
	void pop_back() {data.pop_back();}
	char& back() {return data.back();}
	void resize(int new_size) {data.resize(new_size);}
	string::iterator begin() {return data.begin();}
	string::iterator end() {return data.end();}
	void clear() {return data.clear();}
	const string& str() const {return data;}
	void extend(const string& s){data += s;}

	static constexpr auto type_name = "string";
	int size() const override { return int(data.size()); };
	Value element(int i) const override { return Value::Int(data[i]); };

	void inspect(Printer&) const override;
	Type& type() const override;
};

#endif
