#include "rafs.h"

#include "vm.h"
#include "atoms.h"
#include "range.h"

namespace RAFS
{
struct BoxedSymbol: IRAFS
{
	Symbol symbol;

	BoxedSymbol(Symbol s): symbol(s){};

	virtual int size() const { return int(symbol.str().size()); };
	virtual Value element(int i) const { return Value::Int(symbol.str()[i]); };
};

struct BoxedInt: IRAFS
{
	int i;

	BoxedInt(int n): i(n){};

	virtual int size() const { return i; };
	virtual Value element(int) const { return Value::Unit; };
};

rafs RAFS(Value a)
{
	if(a.type == TValue::SYMBOL)
		return rafs(BoxedSymbol(as_symbol(a)));
	if(a.type == TValue::NUMBER)
		return rafs(BoxedInt(as_int(a)));

	if(a.is_object())
		if(auto r = dynamic_cast<const IRAFS*>(a.object); r)
			return rafs(r);

	throw RuntimeError{TypeError{"RAFS", a}};
}

bool is_RAFS(Value a)
{
	return a.type == TValue::SYMBOL || a.type == TValue::NUMBER || a.is_object<IRAFS>();
}

Value at(Context& c)
{
	auto fs = RAFS(c[0]);
	auto i = as_int(c[1]);

	if(i < 0 || i >= fs.size())
		return c.fail();
	return fs[i];
}

Value first(Context& c)
{
	auto fs = RAFS(c[0]);
	if(fs.empty())
		return c.fail();
	return fs[0];
}

Value last(Context& c)
{
	auto fs = RAFS(c[0]);
	if(fs.empty())
		return c.fail();
	return fs[(fs.size() - 1)];
}

Value last_num(Context& c)
{
	auto fs = RAFS(c[0]);
	if(fs.empty())
		return c.fail();
	return Value::Int(fs.size() - 1);
}

Value size(Context& c)
{
	return Value::Int(RAFS(c[0]).size());
}

int unpack(Context& c, Value a)
{
	if(a == Value::Nil)
		return c.match_fail();

	auto fs = RAFS(a);

	c.set_results_number(fs.size());
	for(int i = 0; i < fs.size(); i++) // TODO: stack overflow control
		c[i] = fs[i];
	return fs.size();
}

Value is_empty(Context& c)
{
	return Value::Bool(RAFS(c[0]).empty());
}


bool equals(const rafs& a, const rafs& b)
{
	if(a.size() != b.size())
		return false;

	// TODO: use equal(begin(a), end(a), begin(b)));
	for(int i = 0; i < a.size(); i++)
		if(a[i] != b[i])
			return false;
	return true;
}

Value equals(Context& c)
{
	auto a = RAFS(c[0]);
	auto b = RAFS(c[1]);

	return Value::Bool(equals(a, b));
}

Value minus_left(Context& c)
{
	auto full = RAFS(c[0]);
	auto left = RAFS(c[1]);

	if(left.size() > full.size())
		return c.fail();

	auto slice = Slice(c[0], 0, left.size());
	if(RAFS::equals(left, RAFS(slice)))
		return create<Slice>(c, c[0], left.size(), full.size());
	return c.fail();
}

Value minus_right(Context& c)
{
	auto full = RAFS(c[0]);
	auto right = RAFS(c[1]);

	if(right.size() > full.size())
		return c.fail();

	auto slice = Slice(c[0], full.size() - right.size(), full.size());
	if(RAFS::equals(right, RAFS(slice)))
		return create<Slice>(c, c[0], 0, slice.start);
	return c.fail();
}

Value random(Context& c)
{
	auto fs = RAFS(c[0]);
	if(fs.empty())
		return c.fail();
	return fs[rand() % fs.size()];
}

Value each(Context& c)
{
	auto fs = RAFS(c[0]);

	for(int i = 0; i < fs.size(); i++)
		c.thread.apply({c[1], fs[i]});
	return Value::Unit;
}

Value each_pair(Context& c)
{
	auto fs = RAFS(c[0]);

	for(int i = 0; i < fs.size(); i++)
		c.thread.apply({c[1], Value::Int(i), fs[i]});
	return Value::Unit;
}

int decons(Context& c, Value a)
{
	auto fs = RAFS(a);
	return decons(c, SliceView(a, 0, fs.size()));
}

int decons_pair(Context& c, Value a)
{
	auto fs = RAFS(a);
	return decons_pair(c, SliceView(c[0], 0, fs.size()));
}

int as_finite_list(Context& c, Value a)
{
	const auto& fs = RAFS(a);
	return c.results(fs.size(), a);
}

Library methods =
{
	{"at", value(at)},
	{"call", value(at)},
	{"size", value(size)},
	{"first", value(first)},
	{"last", value(last)},
	{"last#", value(last_num)},
	{"unpack", value(unpack)},
	{"empty?", value(is_empty)},
	{"equals", value(equals)},
	{"slice", value(slice)},
	{"minus_left", value(minus_left)},
	{"minus_right", value(minus_right)},
	{"random", value(random)},
	{"each", value(each)},
	{"each_pair", value(each_pair)},
	{"decons", value(decons)},
	{"decons_pair", value(decons_pair)},
	{"as_finite_list", value(as_finite_list)}
};
}

void Slice::mark(GC &gc) const
{
	gc.mark(x);
}

void Slice::inspect(Printer& p) const
{
	p << format("Slice({}, {}:{})", x, start, stop);
}

void SliceView::inspect(Printer& p) const
{
	p << format("SliceView({}, {}:{})", x, start, stop);
}

int decons_pair(Context& c, const SliceView& slice)
{
	if(slice.empty())
		return c.match_fail();

	auto first = slice.element(0);
	auto rest = create<SliceView>(c, slice.x, slice.start + 1, slice.stop);
	return c.results(slice.start, first, rest.value());
}

int decons(Context& c, const SliceView& slice)
{
	if(slice.empty())
		return c.match_fail();

	auto first = slice.element(0);
	auto rest = create<SliceView>(c, slice.x, slice.start + 1, slice.stop);
	return c.results(first, rest.value());
}

namespace ISliceView
{
int decons(Context& c, Value a)
{
	const auto& slice = *as_object<SliceView>(a);
	return decons(c, slice);
}

int decons_pair(Context& c, Value a)
{
	const auto& slice = *as_object<SliceView>(a);
	return decons_pair(c, slice);
}

Library methods =
{
	{"decons", value(decons)},
	{"decons_pair", value(decons_pair)}
};
};

namespace ISlice
{
int decons(Context& c, Value a)
{
	const auto& slice = *as_object<Slice>(a);
	if(slice.empty())
		return c.match_fail();

	auto first = slice.element(0);
	auto rest = create<Slice>(c, slice.x, slice.start + 1, slice.stop);
	return c.results(first, rest.value());
}

Library methods =
{
	{"decons", value(decons)}
};
};

Type& SliceView::type() const
{
	static Type interface(ISliceView::methods, RAFS::methods);
	return interface;
}

Type& Slice::type() const
{
	static Type interface(ISlice::methods, RAFS::methods);
	return interface;
}

Value slice(Context& c)
{
	auto start = c.is_arg(1) ? as_int(c[1]) : 0;

	if(!c.is_arg(2))
		return create<Slice>(c, c[0], start);

	auto stop = as_int(c[2]);
	if(stop < start)
		return c.fail();

	return create<Slice>(c, c[0], start, stop);
}

int clamp(int a, int min, int max)
{
	if(a < min)
		return min;
	if(a > max)
		return max;
	return a;
}
