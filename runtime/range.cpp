#include "range.h"

#include "vm.h"
#include "formatting.h"
#include "rafs.h"
#include "sequence.h"
#include "atoms.h"

void Range::inspect(Printer& p) const
{
	p << format("Range({}..{})", a, b);
}

bool Range::empty() const
{
	return a > b;
}

void Iota::inspect(Printer& p) const
{
	p << format("Iota({})", a);
}

namespace IRange
{

int decons(Context& c, Value a)
{
	const auto& rg = *as_object<Range>(a);
	if(rg.empty())
		return c.match_fail();

	auto first = rg.a;
	auto rest = create<Range>(c, rg.a + 1, rg.b);
	return c.results(first, rest.get());
}

Library methods =
{
	{"decons", value(decons)}
};
};

namespace IIota
{

int decons(Context& c, Value a)
{
	const auto& iota = *as_object<Iota>(a);

	auto first = iota.a;
	auto rest = create<Iota>(c, iota.a + 1);
	return c.results(first, rest.get());
}

Library methods =
{
	{"decons", value(decons)}
};
};

Type& Range::type() const
{
	static Type interface(IRange::methods, RAFS::methods);
	return interface;
}

Type& Iota::type() const
{
	static Type interface(IIota::methods);
	return interface;
}
