#ifndef CONTEXT_H_INCLUDED
#define CONTEXT_H_INCLUDED

#include "value.h"
#include "error.h"
#include "memory.h"
#include "formatting.h"
#include <vector>

class VM;
class Thread;
struct FunctionScope;

class Context
{
	int shift;
	int size;
	Value* r() const;
public:
	VM& vm;
	Thread& thread;
	Context(int aShift, int aSize, VM& aVM, Thread &aThread): shift(aShift), size(aSize), vm(aVM), thread(aThread)
	{}
	Value& operator[](int i);

	void set_results_number(int n);

	template<class... T>
	int results(T... x)
	{
		int i = 0;
		set_results_number(sizeof...(x));
		((void)(r()[i++] = make_value(x)), ...);
		return sizeof...(x);
	}

	Value fail()
	{
		return Value::Nil;
	}

	int match_fail()
	{
		return results(fail());
	}

	template<class ...T, std::size_t... Is>
	tuple<T...> arguments_by_indexes(std::index_sequence<Is...>)
	{
		if(size != sizeof...(T))
			throw RuntimeError{ExpectNArguments{sizeof...(T), size}};

		return {cast<T>(r()[Is])...};
	}

	template<class ...T>
	tuple<T...> arguments()
	{
		return arguments_by_indexes<T...>(make_index_sequence<sizeof...(T)>{});
	}

	int arg_num() const
	{
		return size;
	}

	bool is_arg(int i)
	{
		return i < arg_num() && r()[i] != Value::Nil;
	}

	Value* begin() const
	{
		return r();
	}

	Value* end() const
	{
		return r() + size;
	}

	void pop_front();

	int operator()(Value);
	template<class X>
	int operator()(X x)
	{
		return operator()(value(x));
	}
	Value delegate(Value method)
	{
		operator()(method);
		return *r();
	}

	Value createBlob(string);
	Value createFunction(const FunctionScope&);
	Value createChannel(Value, Value, Value);
	Value createSource(Value, Value);
	Value createSink(Value, Value);

	GC::Handle keep(Value a);

	friend Thread;
};

template<class T, TValue type>
T* type_checked(Value a)
{
	if(a.type != type)
		throw RuntimeError{TypeError{inspect(type), a}};
	return a.as<T>();
}

#endif
