#include "channel.h"

#include "atoms.h"
#include "formatting.h"
#include "vm.h"
#include "sequence.h"
#include <iostream>

Value Source::get(Context& c)
{
	return c.delegate(getter);
}

void Source::mark(GC& gc) const
{
	gc.mark(seq);
	gc.mark(getter);
}

void Source::inspect(Printer& p) const
{
	p << inspect_pointer(this);
}

Value Sink::send(Context& c)
{
	return c.delegate(sender);
}

void Sink::mark(GC& gc) const
{
	gc.mark(seq);
	gc.mark(sender);
}

void Sink::inspect(Printer& p) const
{
	p << inspect_pointer(this);
}

void Channel::inspect(Printer& p) const
{
	p << inspect_pointer(this);
}

Value Channel::get(Context& c)
{
	return c.delegate(getter);
}

Value Channel::send(Context& c)
{
	return c.delegate(sender);
}

namespace Atoms
{
Value get(Context& c)
{
	if(c[0].type == TValue::BOOL)
		return Value::Bool(c[0]._bool);
	return as_object<ISource>(c[0])->get(c);
}

Value send(Context& c)
{
	return as_object<ISink>(c[0])->send(c);
}

Value make_discard(VM& vm)
{
	return vm.create<Sink>(Value::Nil, value([](Context& c)
	{
		return c.fail();
	}));
}

Value make_io(VM& vm)
{
	return vm.create<Channel>(Value::Nil, value([](Context& c)
	{
		string line;
		getline(cin, line);
		return c.createBlob(line);
	}), value([](Context& c)
	{
		string line = to_string(c[1]);
		cout << line << endl;
		return Value::Unit;
	}));
}

Value make_stdout(VM& vm)
{
	return vm.create<Sink>(Value::Nil, value([](Context& c)
	{
		cout << to_string(c[1]);
		return c[1];
	}));
}

Value seq_channel(Context& c)
{
	return c.createSource(c[0], value([](Context& c)
	{
		auto ch = as_object<Source>(c[0]);
		auto [first, rest] = decons_h(c, ch->sequence());
		ch->sequence() = rest;
		return *first;
	}));
}

Value accumulator(Context& c)
{
	return c.createChannel(c[0], value([](Context& c)
	{
		return as_object<Channel>(c[0])->sequence();
	}), value([](Context& c)
	{
		auto ch = as_object<Channel>(c[0]);
		auto sum = c.thread.apply({atom("plus"), ch->sequence(), c[1]});
		ch->sequence() = sum;
		return sum;
	}));
}

Library ChannelLib()
{
	return
	{
		{"get", value(get)},
		{"send", value(send)},
		{"seq_channel", value(seq_channel)},
		{"accumulator", value(accumulator)}
	};
}
}
