#ifndef OBJECT_H_INCLUDED
#define OBJECT_H_INCLUDED

#include "runtime/memory.h"
#include "base/error.h"

class Printer;

struct Object: GC::Object
{
	virtual void inspect(Printer&) const = 0;
	virtual Type& type() const { NOT_IMPLEMENTED; };
};

#endif
