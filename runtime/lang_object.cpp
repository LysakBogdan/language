#include "lang_object.h"

#include "memory.h"
#include "formatting.h"

void LangObject::mark(GC &gc) const
{
	obj.mark(gc);
}

void LangObject::inspect(Printer& p) const
{
	p << "Object(" << &obj << ")";
}
