#include "memory.h"

#include "function.h"
#include "config.h"

#include <algorithm>

void GC::add(const Object* o)
{
#if TRACE_GC
	LOG_LINE("GC: new " << o);
#endif
	allocations++;
	objects.push_back(o);
}

void GC::mark(const Value& x)
{
	switch(x.type)
	{
		using enum TValue;
		case NIL:
		case UNIT:
		case BOOL:
		case NUMBER:
		case SYMBOL:
		case CFUNCTION:
		case CPATTERN:
		case METHOD:
			return;
		case FUNCTION:
		case OBJECT:
			mark(x.object);
			break;
		default:
			CAN_NOT_HAPPEN;
	}
}

void GC::mark(const Object* obj)
{
	if(!obj)
		return;

	if(!obj->marked)
	{
#if TRACE_GC
		LOG_LINE("GC: marked " << obj);
#endif
		obj->marked = true;
		roots.push_back(obj);
	}
}

void GC::mark_roots()
{
#if TRACE_GC
	for(auto hold : locked)
		LOG(hold);
#endif
	for(auto e : locked)
		mark(e);

	while(!roots.empty())
	{
		auto o = roots.back();
		roots.pop_back();
		o->mark(*this);
	}
}

void GC::sweep()
{
	erase_if(objects, [this](const Object* object)
	{
		if(object->marked)
		{
			object->marked = false;
			return false;
		}

#if TRACE_GC
		LOG_LINE("GC: deleted " << object);
#endif
		delete object;
		collected++;
		return true;
	});

	allocations = 0;
}

void GC::collect()
{
#if GC_ENABLED
	mark_roots();
	sweep();
#endif
}

int GC::manual_collect()
{
	collect();
	auto res = collected;
	collected = 0;
	return res;
}

bool GC::need_collect() const
{
	// TODO: change
	return !locks && allocations > 512;
}

void GC::lock(Value a)
{
	if(a.is_object())
		locked.insert(a.object);
}

void GC::unlock(Value a)
{
	if(a.is_object())
	{
		auto it = locked.find(a.object);
		locked.erase(it);
	}
}

GC::~GC()
{
	for(auto o : objects)
		delete o;
}
