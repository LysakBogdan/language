#ifndef LANG_OBJECT_H_INCLUDED
#define LANG_OBJECT_H_INCLUDED

#include "table.h"

class LangObject: public Object
{
public:
	Type* _type;
	MutableTable obj;
	explicit LangObject(Type* aType): _type(aType) {}

	void mark(GC &gc) const override;
	void inspect(Printer&) const override;

	Type& type() const override { return *_type; };
};

#endif
