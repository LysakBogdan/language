#ifndef CHANNEL_H_INCLUDED
#define CHANNEL_H_INCLUDED

#include "value.h"
#include "object.h"
#include "module.h"

struct ISource
{
	virtual Value get(Context&) = 0;
	static constexpr auto type_name = "source";
};

struct ISink
{
	virtual Value send(Context&) = 0;
	static constexpr auto type_name = "sink";
};

class Channel: public Object, public ISource, public ISink
{
	Value seq, getter, sender;
public:
	Channel(Value aSeq, Value aGetter, Value aSender): seq(aSeq), getter(aGetter), sender(aSender){}
	Value get(Context& c) override;
	Value send(Context& c) override;
	Value& sequence() { return seq; }

	static constexpr auto type_name = "channel";
	void mark(GC &gc) const override
	{
		gc.mark(seq);
		gc.mark(getter);
		gc.mark(sender);
	}
	void inspect(Printer&) const override;
};

class Source: public Object, public ISource
{
	Value seq, getter;
public:
	Source(Value aSeq, Value aGetter): seq(aSeq), getter(aGetter)
	{
	}

	Value get(Context& c) override;
	Value& sequence() { return seq; }

	void mark(GC &gc) const override;
	void inspect(Printer&) const override;
};

class Sink: public Object, public ISink
{
	Value seq, sender;
public:
	Sink(Value aSeq, Value aSender): seq(aSeq), sender(aSender)
	{
	}

	Value send(Context& c) override;
	Value& sequence() { return seq; }

	void mark(GC &gc) const override;
	void inspect(Printer&) const override;
};

class VM;

namespace Atoms
{
	Value make_discard(VM& vm);
	Value make_io(VM& vm);
	Value make_stdout(VM& vm);
	Library ChannelLib();
}

#endif
