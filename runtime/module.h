#ifndef MODULE_H_INCLUDED
#define MODULE_H_INCLUDED

#include "value.h"
#include "memory.h"
#include <algorithm>

using Library = std::map<std::string, Value>;

class Module
{
	std::map<Symbol, Value> items;
public:
	Value operator[](Symbol) const;
	Value operator[](const std::string& s) const
	{
		return operator[](Symbol(s));
	};
	void put(Symbol, Value);
	void put(std::string name, Value value)
	{
		put(Symbol(name), value);
	}

	void include(const Library&);

	void mark(GC& gc) const;
};

#endif
