#ifndef FORMATTING_H_INCLUDED
#define FORMATTING_H_INCLUDED

#include "error.h"
#include "parser/parsing_error.h"
#include <set>
#include <sstream>
#include <format>

using namespace std;

string inspect(const Value&);
string inspect(TValue);
ostream& operator<<(ostream& os, const Value& a);
ostream& operator<<(ostream&, const Location&);

string inspect(const RuntimeError&);
string inspect(const CompileError&);
string inspect(const string&, const ParsingError&);

string InspectPointer(const char* type_name, const void* pointer);

template<class X>
string inspect_pointer(X* x)
{
	return InspectPointer(x->type_name, x);
}

template<>
struct std::formatter<Value>: std::formatter<std::string>
{
	auto format(Value a, format_context& c) const
	{
		return formatter<string>::format(inspect(a), c);
	}
};

class Printer
{
	stringstream ss;
	set<const Object*> seen;
public:

	template<class T>
	Printer& operator<<(const T& x)
	{
		ss << x;
		return *this;
	}

	Printer& operator<<(Value a);
	Printer& operator<<(Object* o);

	string str() const { return ss.str(); };
};

template<class T>
void print_pairs(Printer& print, const T& table)
{
	bool comma = false;
	for(auto [key, value] : table)
	{
		print << (comma ? ", " : "") << key << " => " << value;
		comma = true;
	}
}

#endif
