#ifndef ARRAY_H
#define ARRAY_H

#include "object.h"
#include "rafs.h"
#include <vector>

class Array: public Object, public IRAFS
{
public:
	vector<Value> values;

	Array() {};
	explicit Array(unsigned size): values(size) {};
	Array(unsigned size, const Value* data): values(data, data + size) {};
	Array(const RAFS::rafs& fs);
	Array(Context& c, Value seq);

	static constexpr auto type_name = "array";
	static Type& get_type();
	void mark(GC &gc) const override;
	void inspect(Printer&) const override;
	Type& type() const override;

	int size() const override { return values.size(); }
	Value element(int i) const override { return values[i]; }
};

class MutableArray: public Array
{
public:
	MutableArray() {};
	MutableArray(const RAFS::rafs& fs): Array(fs) {};
	MutableArray(Context& c, Value seq): Array(c, seq) {};

	static constexpr auto type_name = "list";
	Type& type() const override;

	int size() const override { return values.size(); }
	Value element(int i) const override { return values[i]; }
};

vector<Value>& as_array(Value a);

#endif
