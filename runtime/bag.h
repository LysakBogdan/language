#ifndef BAG_H_INCLUDED
#define BAG_H_INCLUDED

#include "type.h"

struct Pickable
{
	static constexpr auto type_name = "bag-like";
	virtual Value pick(Context& c, Value a) const = 0;
};

class Bag: public Object, public Pickable
{
	vector<Value> values;
public:
	Bag() {}
	Bag(const vector<Value>& a): values(a) {}

	static constexpr auto type_name = "bag";
	void mark(GC& gc) const override;
	void inspect(Printer&) const override;
	Type& type() const override;

	void add(Value a)
	{
		values.push_back(a);
	}

	Value pick(Context& c, Value a) const override;

	bool pick(Value a);
	int decons(Context& c) const;
};

Value bag_equals(Context& c);

#endif
