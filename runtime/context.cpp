#include "context.h"

#include "vm.h"
#include "channel.h"
#include "atom.h"

Value& Context::operator[](int i)
{
	assert(i >= 0);
	if(i >= size)
		throw RuntimeError{MissingArgument{i}};
	return r()[i];
}

Value* Context::r() const
{
	return thread.stack.data() + shift;
}

Value Context::createBlob(string s)
{
	return make_value(vm.create<Blob>(s));
}

Value Context::createFunction(const FunctionScope& exe)
{
	auto proto = compile(vm, exe);
	auto function = vm.create<Function>(*proto);
	return function.value();
}

Value Context::createChannel(Value a, Value get, Value send)
{
	return make_value(vm.create<Channel>(a, get, send));
}

Value Context::createSource(Value a, Value get)
{
	return make_value(vm.create<Source>(a, get));
}

Value Context::createSink(Value a, Value send)
{
	return make_value(vm.create<Sink>(a, send));
}

int Context::operator()(Value method)
{
	if(!callValue(method, *this))
		thread.run();
	return thread.result_number();
}

GC::Handle Context::keep(Value a)
{
	return GC::Handle(vm.gc, a);
}

void Context::pop_front()
{
	copy(r() + 1, r() + size--, r());
	thread.pop();
}

void Context::set_results_number(int n)
{
	size = n;
	thread.stack.resize(shift + n);
}
