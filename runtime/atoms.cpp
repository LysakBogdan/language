#include "atoms.h"

#include "atom.h"
#include "semantics.h"
#include "formatting.h"
#include "range.h"
#include "channel.h"
#include "sequence.h"
#include "bag.h"

#include "inumber.h"
#include "isymbol.h"
#include "iarray.h"
#include "itable.h"

#include "general.h"
#include "arithmetic.h"
#include "comparisons.h"
#include "os.h"
#include "internal.h"

#include <math.h>
#include <iostream>
#include <charconv>

using enum TValue;

Value try_method(Context& c, Symbol name)
{
	if(!supports(c.vm, c[0], name))
		return c.fail();
	return c.delegate(Value::Method(name));
}

Value not_implemented(Context&)
{
	NOT_IMPLEMENTED;
}

Value fail(Context& c)
{
	return c.fail();
}

Value identity(Value x)
{
	return x;
}

Value not_(Value a)
{
	return Value::Bool(!a);
}

template<auto (*f)(Value)>
CFunction to_atom()
{
	return [](Context& c)
	{
		return make_value(f(c[0]));
	};
}

namespace Atoms
{
Value yes_no(Context& c)
{
	return Value::YesNo(bool(c[0]));
}

Value array(Context& c)
{
	return create<Array>(c, c.arg_num(), c.begin());
}

Value table(Context& c)
{
	if(c.arg_num() % 2 != 0)
		return c.fail();
	return create<Table>(c, c.arg_num(), c.begin());
}

Value bag(Context& c)
{
	auto a = create<Bag>(c);
	for(int i = 0; i < c.arg_num(); i++)
		a->add(c[i]);
	return a;
}

struct AssertFail: runtime_error
{
	AssertFail(const string& msg): runtime_error(msg) {};
};

Value assert_true(Context& c)
{
	if(!c[0])
		throw AssertFail(as_symbol(c[1]).str());
	return Value::Unit;
}

int as_range(Context& c, Value range)
{
	Iterator e(c, range);
	if(!e)
		return c.match_fail();

	auto a = as_number(*e);
	auto b = a;

	for(e.next(); e; e.next())
		if(*e != Value::Int(++b))
			return c.match_fail();
	return c.results(a, b);
}

Value parse_int(Context& c)
{
	const auto s = as_string(c[0]);
	auto base = c.arg_num() > 1 ? as_int(c[1]) : 10;
	auto first = s.data();
	auto last = s.data() + s.size();

	int n;
	auto [ptr, ec] = std::from_chars(first, last, n, base);
	if(ec == std::errc() && ptr == last)
		return Value::Int(n);
	return Value::Nil;
}

Value finite_list(Context&)
{
	NOT_IMPLEMENTED;
}

Value inverse_atom(Value a)
{
	static map<Value, Value> inv =
	{
		{atom("identity"), atom("identity")},
		{atom("array"), atom("unpack")},
		{atom("negate"), atom("negate")},
		{atom("not"), atom("not")},
		{atom("seq"), atom("as_seq")},
		{atom("range"), atom("as_range")},
		{atom("finite_list"), "as_finite_list"_m},
		{atom("pair"), atom("as_pair")}
	};

	if(auto it = inv.find(a); it != inv.end())
		return it->second;
	NOT_IMPLEMENTED;
}

Value inverse(Context& c)
{
	auto [f] = c.arguments<Value>();
	if(f.type != FUNCTION)
		return inverse_atom(f);

	const auto& exe = f.as<Function>()->getProto().exe;

	auto n = exe.body.count;
	auto results = VarSpace(0, n);
	auto body = VarBlock{n, Let{exe.body, NValues{Arguments{}, 1}, MatchReturn{var_list(results)}}};

	return c.createFunction({exe.captures, body});
}

Value print(Context& c)
{
	for(int i = 0; i < c.arg_num(); i++)
		cout << to_string(c[i]);
	cout << endl;
	return Value::Unit;
}

Value convert_to_string(Context& c)
{
	auto [x] = c.arguments<Value>();
	return value(Symbol(to_string(x)));
}

Value inspect_value(Context& c)
{
	return value(Symbol(inspect(c[0])));
}

Value type(Context& c)
{
	return make_value(&c[0].getType(c.vm));
}

Value random_array(Context& c)
{
	auto [n] = c.arguments<int>();
	auto numbers = create<Array>(c, n);
	for(auto& x : numbers->values)
		x = Value::Int(rand() % 1000000);
	return numbers;
}

Value yield(Context& c)
{
	c.thread.yield_key = Value::Unit;
	c.thread.yield = c[0];
	return c[0];
}

Value yield_pair(Context& c)
{
	auto [key, value] = c.arguments<Value, Value>();
	c.thread.yield_key = key;
	c.thread.yield = value;

	return value;
}

Value run_gc(Context& c)
{
	return Value::Int(manual_garbage_collection(c.vm));
}

Value to_array(Context& c)
{
	if(c.arg_num() > 0 && !RAFS::is_RAFS(c[0]))
		return create<Array>(c, c, c[0]);
	auto [fs] = c.arguments<RAFS::rafs>();
	return create<Array>(c, fs);
}

Value mutable_list(Context& c)
{
	if(c.arg_num() == 0)
		return create<MutableArray>(c);

	if(RAFS::is_RAFS(c[0]))
	{
		auto [fs] = c.arguments<RAFS::rafs>();
		return create<MutableArray>(c, fs);
	}

	return create<MutableArray>(c, c, c[0]);
}

Value capitalize(Context& c)
{
	auto [s] = c.arguments<string>();
	s[0] = toupper(s[0]);
	return value(Symbol(s));
}

Value dict(Context& c)
{
	MutableTable::Dictionary dictionary{};

	c(atom("decons_pair"));
	while(c[0] != Value::Nil)
	{
		dictionary.insert(pair<Value, Value>{c[0], c[1]});
		c[0] = c[2];
		c(atom("decons_pair"));
	}

	return create<MutableTable>(c, dictionary);
};

Value key_value_pair(Context& c)
{
	auto [key, value] = c.arguments<Value, Value>();

	auto exe = FunctionScope{{}, in_block(Block(), LetList{Cortege{key}, NValues{Arguments{}, 1}, value})};
	return c.createFunction(exe);
}

int as_pair(Context& c, Value a)
{
	if(auto pr = single_pair(c, a); pr)
		return c.results(pr->first, pr->second);
	return c.match_fail();
}

Value intersection(Context& c)
{
	if(c[0].type == NUMBER && c[1].type == NUMBER)
	{
		auto [a, b] = c.arguments<int, int>();
		return Value::Int(min(a, b));
	}

	return create<Intersection>(c, c[0], c[1]);
}

Value bag_union(Context& c)
{
	if(c[0].type == NUMBER && c[1].type == NUMBER)
	{
		auto [a, b] = c.arguments<int, int>();
		return Value::Int(max(a, b));
	}

	return create<Union>(c, c[0], c[1]);
}

Value bag_difference(Context& c)
{
	if(c[0].type == NUMBER && c[1].type == NUMBER)
	{
		auto [a, b] = c.arguments<int, int>();
		if(a < b)
			return Value::Int(0);
		return Value::Int(a - b);
	}

	return create<BagDifference>(c, c[0], c[1]);
}

Value symmetric_difference(Context& c)
{
	if(c[0].type == NUMBER && c[1].type == NUMBER)
	{
		auto [a, b] = c.arguments<int, int>();
		return Value::Int(a > b ? a - b : b - a);
	}

	return create<SymmetricDifference>(c, c[0], c[1]);
}

Value get_global(Context& c)
{
    return c.vm.module[as_symbol(c[0])];
}

Value set_value(Context& c)
{
	auto [index, value] = c.arguments<int, Value>();
	return c.thread.var(index) = value;
}

int write_value(Context& c, Value x)
{
	return c.results(c[1], c[1]);
}

int swap_values(Context& c, Value x)
{
	return c.results(c[1], c[0]);
}

int inc(Context& c, Value x)
{
	auto [i] = c.arguments<int>();
	return c.results(Value::Int(i + 1));
}

Library atoms;

Library InitAtoms()
{
	return
	{
		{"identity", value(to_atom<::identity>())},
		{"not", value(to_atom<::not_>())},
		{"fail", value(fail)},
		{"bool", value(yes_no)},

		{"not_implemented", value(not_implemented)},

		{"array", value(array)},
		{"table", value(table)},
		{"bag", value(bag)},

		{"assert", value(assert_true)},
		{"inverse", value(inverse)},
		{"print", value(print)},
		{"to_string", value(convert_to_string)},
		{"inspect", value(inspect_value)},
		{"type", value(type)},

		{"range", value(contructor<Range, int, int>())},
		{"iota", value(contructor<Iota>())},
		{"as_range", value(as_range)},

		{"__random_array", value(random_array)},

		{"int", value(parse_int)},

		{"yield", value(yield)},
		{"yield_pair", value(yield_pair)},
		{"run_gc", value(run_gc)},

		{"fallback", value(contructor<Fallback, Value, Value>())},
		{"finite_list", value(finite_list)},

		{"to_array", value(to_array)},
		{"list", value(mutable_list)},
		{"push", "push"_m},

		{"slice", value(slice)},

		{"capitalize", value(capitalize)},
		{"pair", value(key_value_pair)},
		{"as_pair", value(as_pair)},
		{"dict", value(dict)},

		{"pick", "pick"_m},
		{"intersection", value(intersection)},
		{"union", value(bag_union)},
		{"bag_difference", value(bag_difference)},
		{"symmetric_difference", value(symmetric_difference)},
		{"equals", "equals"_m},

		{"get_global", value(get_global)},

		{"__set_value", value(set_value)},
		{"__write_value", value(write_value)},
		{"__swap", value(swap_values)},
		{"__inc", value(inc)}
	};
}

void insert(Library& a, const Library& b)
{
	a.insert(b.begin(), b.end());
}

void Init(Module& module, VM& vm)
{
	atoms = InitAtoms();
	insert(atoms, Sequence());
	insert(atoms, Comparisons());
	insert(atoms, Arithmetic());
	insert(atoms, ChannelLib());
	insert(atoms, OS(vm));
	insert(atoms, RAFS::methods);
	insert(atoms, Internal());

	insert(atoms, INumber::methods);
	insert(atoms, IArray::mutable_methods);
	insert(atoms, IDictionary::methods);
	insert(atoms, ISymbol::methods);

	module.include(Atoms::atoms);

	module.put("discard", Atoms::make_discard(vm));
	module.put("io", Atoms::make_io(vm));
	module.put("stdout", Atoms::make_stdout(vm));
	module.put("infinity", vm.create<Infinity>());
}

optional<string> name(Value f)
{
	for(auto [name, atom] : atoms)
		if(atom == f)
			return name;
	return nullopt;
}
};

bool callMethod(Symbol name, Context& context)
{
	if(context[0].type == NIL)
	{
		context.thread.callAtom(context, fail);
		return true;
	}

	auto method = context[0].getType(context.vm).getMethod(name);
	if(method == Value::Nil)
		throw RuntimeError{UnknownMethod{name.str(), context[0]}};

	return callValue(method, context);
}

bool callValue(Value callee, Context& context)
{
	switch(callee.type)
	{
		case FUNCTION:
			context.thread.callFunction(context, *callee.as<Function>());
			return false;
		case CFUNCTION:
			context.thread.callAtom(context, callee.cfunction);
			return true;
		case CPATTERN:
			context.thread.callPattern(context, callee.cpattern);
			return true;
		case METHOD:
			return callMethod(Symbol(callee.id), context);
		default:
			throw RuntimeError{CanNotBeCalled{callee}};
	}
}

bool init_apply(Context& context)
{
	auto callee = context[0];
	if(callee.type == OBJECT || callee.type == SYMBOL)
		callee = "call"_m;
	else
		context.pop_front();
	return callValue(callee, context);
}

Value atom(const char* name)
{
	auto it = Atoms::atoms.find(name);
	if(it != Atoms::atoms.end())
		return it->second;
	return Value::Nil;
}
