#include "value.h"

#include "type.h"

#include "lang_object.h"
#include "channel.h"
#include "inspection.h"
#include "error.h"
#include "chars.h"
#include "vm.h"

#include <sstream>

const Value Value::Nil = {NIL, {0}};
const Value Value::Unit = {UNIT, {0}};

using enum TValue;

Type& Value::getType(VM& vm) const
{
	if(type == OBJECT)
		return as<Object>()->type();
	return vm.base_types[int(type)];
}

Value parse_number(const string& number)
{
	auto s = number.c_str();

	if(s[0] == '0')
	switch(s[1])
	{
		case 'b':
		case 'B':
			s += 2;
			return Value::Int(parse_binary(s));
		case 'o':
		case 'O':
			s += 2;
			return Value::Int(parse_octal(s));
		case 'x':
		case 'X':
			s += 2;
			return Value::Int(parse_hex(s));
		default:
			break;
	}

	return Value::Double(parse_real(s));
}

bool Value::operator==(const Value& a) const
{
	if(type != a.type)
		return false;

	switch(type)
	{
		case NIL:
		case UNIT:
			return true;
		case BOOL:
			return _bool == a._bool;
		case NUMBER:
			return number == a.number;
		case SYMBOL:
		case METHOD:
			return id == a.id;
		case FUNCTION:
		case OBJECT:
			return object == a.object;
		case CFUNCTION:
			return cfunction == a.cfunction;
		case CPATTERN:
			return cpattern == a.cpattern;
		default:
			CAN_NOT_HAPPEN;
	}
}

bool Value::operator<(const Value& a) const
{
	if(type != a.type)
		return type < a.type;

	switch(type)
	{
		case NIL:
		case UNIT:
			return false;
		case BOOL:
			return _bool < a._bool;
		case NUMBER:
			return number < a.number;
		case SYMBOL:
		case METHOD:
			return id < a.id;
		case OBJECT:
		case FUNCTION:
			return object < a.object;
		case CFUNCTION:
			return reinterpret_cast<void*>(cfunction) < reinterpret_cast<void*>(a.cfunction);
		case CPATTERN:
			return reinterpret_cast<void*>(cfunction) < reinterpret_cast<void*>(a.cfunction);
		default:
			NOT_IMPLEMENTED;
	}
}

bool Value::is_object() const
{
	return type == OBJECT || type == FUNCTION;
}

Value Value::Double(double number)
{
	Value a;
	a.type = NUMBER;
	a.number = number;
	return a;
}

Value value(const Symbol& symbol)
{
	Value a;
	a.type = SYMBOL;
	a.id = symbol.id();
	return a;
}

Value value(CFunction f)
{
	Value a;
	a.type = CFUNCTION;
	a.cfunction = f;
	return a;
}

Value value(CPattern f)
{
	Value a;
	a.type = CPATTERN;
	a.cpattern = f;
	return a;
}

Value Value::Int(int64_t number)
{
	Value a;
	a.type = NUMBER;
	a.number = number;
	return a;
}

Value Value::Bool(bool b)
{
	return b ? Unit : Nil;
}

Value Value::YesNo(bool b)
{
	Value a;
	a.type = BOOL;
	a._bool = b;
	return a;
}

Value Value::Method(Symbol name)
{
	Value a;
	a.type = METHOD;
	a.id = name.id();
	return a;
}

Value make_value(Value a)
{
	return a;
}

Value make_value(bool b)
{
	return Value::Bool(b);
}

Value make_value(int i)
{
	return Value::Int(i);
}

Value make_value(double d)
{
	return Value::Double(d);
}

Value make_value(const string& s)
{
	return value(Symbol(s));
}

Value make_value(Function* f)
{
	return {FUNCTION, {f}};
}

Value make_value(Object* o)
{
	return {OBJECT, {o}};
}

double as_number(Value a)
{
	if(a.type != NUMBER)
		throw RuntimeError{TypeError{inspect(NUMBER), a}};
	return a.number;
}

int as_int(Value a)
{
	if(a.type == NUMBER)
		if(auto n = int(a.number); a.number == double(n))
			return n;
	throw RuntimeError{TypeError{"int", a}};
}

Symbol as_symbol(Value a)
{
	if(a.type != SYMBOL)
		throw RuntimeError{TypeError{inspect(SYMBOL), a}};
	return Symbol(a.id);
}

Function* as_function(Value a)
{
	if(a.type != FUNCTION)
		throw RuntimeError{TypeError{inspect(FUNCTION), a}};
	return a.as<Function>();
}
